<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionableTemplateDocumentData extends Model
{
    protected $table = 'actionable_template_document_data';

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
}
