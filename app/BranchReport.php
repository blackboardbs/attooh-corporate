<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchReport extends Model
{
    protected $table = 'branch_report';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function branch_report_columns(){
        return $this->hasMany('App\BranchReportColumns','branch_report_id','id');
    }
}
