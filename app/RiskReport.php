<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiskReport extends Model
{
    protected $table = 'risk_report';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    // public function fc_report_columns(){
    //     return $this->hasMany('App\FcReportColumns','branch_report_id','id');
    // }
}
