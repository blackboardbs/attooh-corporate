<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Document;

class ClientMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $message;
    public $document;

    public function __construct($data)
    {
        $this->subject = $data['subject'];
        $this->message = $data['message'];
        $this->document = $data['document'];
    }

    public function build()
    {
        //  dd($this->message);
        // dd($this->document->id);

        if(isset($this->document->id)){
            $doc = Document::where('id', $this->document->id)->first();
            return $this->subject($this->subject)->view('emails.client-mail')->with(['mail_message' => $this->message])->attach(storage_path('app/documents/'.$doc->file));
        } else{
            return $this->subject($this->subject)->view('emails.client-mail')->with(['mail_message' => $this->message]);
        }

    }
}
