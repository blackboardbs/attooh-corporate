<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultantReport extends Model
{
    protected $table = 'consultant_report';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function consultant_report_columns(){
        return $this->hasMany('App\ConsultantReportColumns','consultant_report_id','id');
    }
}
