<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DynamicReport extends Model
{
    protected $table = 'dynamic_report';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function dynamic_report_columns(){
        return $this->hasMany('App\DynamicReportColumns','dynamic_report_id','id');
    }
}
