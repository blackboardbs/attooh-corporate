<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSectionInputMirrorValue extends Model
{
    protected $table = 'form_section_input_mirror_value';
}
