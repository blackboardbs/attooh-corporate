<?php

namespace App\Http\Controllers;

use App\FormSection;
use App\FormSectionInputs;
use Illuminate\Http\Request;
use App\FormSectionInputVisibilityRule;
use App\FormInputDropdownItem;

class FormSectionInputController extends Controller
{
    public function getInputType($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return 'App\FormInputText';
                break;
            case 'heading':
                return 'App\FormInputHeading';
                break;
            case 'subheading':
                return 'App\FormInputSubheading';
                break;
            case 'amount':
                return 'App\FormInputAmount';
                break;
            case 'percentage':
                return 'App\FormInputPercentage';
                break;
            case 'integer':
                return 'App\FormInputInteger';
                break;
            case 'video':
                return 'App\FormInputVideo';
                break;
            case 'textarea':
                return 'App\FormInputTextarea';
                break;
            case 'dropdown':
                return 'App\FormInputDropdown';
                break;
            case 'radio':
                return 'App\FormInputRadio';
                break;
            case 'checkbox':
                return 'App\FormInputCheckbox';
                break;
            case 'date':
                return 'App\FormInputDate';
                break;
            case 'boolean':
                return 'App\FormInputBoolean';
                break;
            case 'id':
                return 'App\FormInputId';
                break;
            case 'passport':
                return 'App\FormInputPassport';
                break;
            case 'document':
                return 'App\FormInputDocument';
                break;
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getInputs(Request $request){

        $section = FormSection::find($request->input('form_section_id'));

        $inputs = FormSectionInputs::where('form_section_id',$section->id)->where('input_type',$this->getInputType($request->input('aatype')))->orderBy('order')->get();

        $input = [];
        foreach ($inputs as $p){
            $input[$p->id] = $p->name;
        }
        return $input;
    }

    public function getInputs2(Request $request){
        //$form = FormSection::where('id',$request->input('step_id'))->get();
        $activities = FormSectionInputs::select('order')->where('id',$request->input('activity_id'))->first();
        $prec_activities = FormSectionInputs::where('form_section_id',$request->input('step_id'))->where('id','!=',$request->input('activity_id'))->where('order','<',$activities->order)->orderBy('order')->pluck('name','id');

        return response()->json(['result'=>'success','activities' => $prec_activities]);
    }

    public function getInputType2(Request $request){
        $activity = FormSectionInputs::find($request->input('activity_id'));
        $type = $activity->getFormTypeName();

        return response()->json(['result'=>'success','activity_type' => $type]);
    }

    public function getDropdownItems(Request $request){
        $activity = FormSectionInputs::find($request->input('activity_id'));

        return response()->json(['result'=>'success','dropdownitems' => $activity->input->items->pluck('name')->toArray()]);
    }

    public function getDependantInputs(Request $request){

        $activity = FormSectionInputs::find($request->input('input_id'));

        if(strpos($request->input('input_value'),',') !== false && strlen($request->input('input_value') > 1)){
            if(isset($activity) && $activity->getFormTypeName() == 'dropdown') {
                $vars = explode(',', $request->input('input_value'));

                $dropdownitems = FormInputDropdownItem::select('name')->whereIn('id', $vars)->get();

                $activities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->whereIn('input_value', collect($dropdownitems)->toArray())->get();
                if (count($activities) > 0) {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->whereNotIn('input_value', collect($dropdownitems)->toArray())->whereNotIn('input_id', collect($activities)->toArray())->get();
                } else {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->whereNotIn('input_value', collect($dropdownitems)->toArray())->get();
                }
            }
        } else {
            if(isset($activity) && $activity->getFormTypeName() == 'dropdown') {
                $dropdownitems = FormInputDropdownItem::where('id', $request->input('input_value'))->first();
            }

            if(isset($dropdownitems)) {
                $activities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->where('input_value', $dropdownitems->name)->get();
                if(count($activities) > 0) {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->where('input_value','!=', $dropdownitems->name)->whereNotIn('input_id', collect($activities)->toArray())->get();
                } else {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->where('input_value','!=',$dropdownitems->name)->get();
                }
            } else {
                $activities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input',$request->input('input_id'))->where('input_value',$request->input('input_value'))->get();
                if(count($activities) > 0) {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->where('input_value','!=', $request->input('input_value'))->whereNotIn('input_id', collect($activities)->toArray())->get();
                } else {
                    $nonactivities = FormSectionInputVisibilityRule::select('input_id')->where('preceding_input', $request->input('input_id'))->where('input_value','!=', $request->input('input_value'))->get();
                }
            }
        }


        $prec_activities = [];
        if($activities) {
            foreach ($activities as $activity) {
                if(!in_array($activity->input_id,$prec_activities,true)) {
                    array_push($prec_activities, $activity->input_id);
                }
                /*ActivityVisibilityRule::where('id',$activity->id)->update(['visible'=>1]);*/
            }
        }

        $prec_nonactivities = [];
        if($nonactivities) {
            foreach ($nonactivities as $nonactivity) {
                if(!in_array($nonactivity->input_id,$prec_nonactivities,true)) {
                    array_push($prec_nonactivities, $nonactivity->input_id);
                }
                /*ActivityVisibilityRule::where('id',$nonactivity->id)->update(['visible'=>0]);*/
            }
        }

        return response()->json(['result'=>'success','activities' => $prec_activities,'nonactivities' => $prec_nonactivities]);
    }
}
