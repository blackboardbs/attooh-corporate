<?php

namespace App\Http\Controllers;

use App\EventType;
use App\OutlookIntegration\OutlookIntegrationTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    use OutlookIntegrationTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    // function index()
    // {
    //     $event_types = EventType::all();

    //     return view('master_data.event_types.index')->with('event_types', $event_types);
    // }

    // function create()
    // {

    //     return view('master_data.event_types.create');
    // }

    public function store(Request $request)
    {
        try {
            // Validate required fields
            $request->validate([
                                   'event_type' => 'nullable|integer',
                                   'eventAttendees' => 'nullable|string',
                                   'date_scheduled' => 'required|date',
                                   'end_at' => 'required|date|after_or_equal:date_scheduled',
                                   'event_notes' => 'nullable|string'
                               ]);

            $event = new Event();
            $event->client_id = $request->input('event_client_id');
            $event->user_id = Auth::user()->id;
            $event->event_type_id = $request->input('event_type');
            $event->date_scheduled = Carbon::parse($request->input('date_scheduled'))->toDateTimeString();
            $event->end_at = Carbon::parse($request->input('end_at'))->toDateTimeString();
            $event->event_notes = $request->input('event_notes');
            $event->save();

            $viewData = $this->loadViewData();
            $viewData['userTimeZone'] = "South Africa Standard Time"; // Todo: Set this dynamically

            $graph = $this->getGraph();

            // Attendees from form are a semi-colon delimited list of
            // email addresses
            $attendeeAddresses = explode(';', $request->eventAttendees);

            // The Attendee object in Graph is complex, so build the structure
            $attendees = [];
            foreach($attendeeAddresses as $attendeeAddress)
            {
                array_push($attendees, [
                    // Add the email address in the emailAddress property
                    'emailAddress' => [
                        'address' => $attendeeAddress
                    ],
                    // Set the attendee type to required
                    'type' => 'required'
                ]);
            }

            // Build the event
            $newEvent = [
                'subject' => EventType::find($request->event_type)->name??null,
                'attendees' => $attendees,
                'start' => [
                    'dateTime' => $request->date_scheduled,
                    'timeZone' => $viewData['userTimeZone']
                ],
                'end' => [
                    'dateTime' => $request->end_at,
                    'timeZone' => $viewData['userTimeZone']
                ],
                'body' => [
                    'content' => $request->event_notes,
                    'contentType' => 'text'
                ]
            ];

            // POST /me/events
            $response = $graph->createRequest('POST', '/me/events')
                ->attachBody($newEvent)
                ->setReturnType(\Microsoft\Graph\Model\Event::class)
                ->execute();

            // $event = new Event();
            // $event->client_id = $request->input('event_client_id');
            // $event->user_id = Auth::user()->id;
            // $event->event_type_id = $request->input('event_type');
            // $event->date_scheduled = Carbon::parse($request->input('date_scheduled'))->toDateTimeString();
            // $event->end_at = Carbon::parse($request->input('end_at'))->toDateTimeString();
            // $event->event_notes = $request->input('event_notes');
            // $event->save();

            return redirect()->back()->with('flash_success','Event captured successfully');
        } catch (\Exception $exception){
            return redirect()->back()->with('flash_danger', $exception->getMessage());
        }

    }

    public function show(Event $event)
    {
        $res = Event::find($event->id);
        $res->delete();

        return redirect()->back()->with('flash_success','Event deleted successfully');
    }

    public function destroy(Event $event)
    {
        $res = Event::find($event->id);
        $res->delete();

        return redirect()->back()->with('flash_success','Event deleted successfully');
    }
}
