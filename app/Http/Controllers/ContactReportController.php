<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Process;
use App\Forms;
use App\ContactReport;
use App\RiskReportColumns;
use App\ActionableAmountData;
use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\ActionableDropdownData;
use App\Activity;
use App\ActivityRelatedPartyLink;
use App\Committee;
use App\Config;
use App\Exports\CustomReportExport;
use App\FormInputBooleanData;
use App\FormInputDateData;
use App\FormInputDropdownData;
use App\FormInputTextareaData;
use App\FormInputTextData;
use App\FormSection;
use App\FormSectionInputs;
use App\RelatedPartiesTree;
use App\RelatedParty;
use App\RelatedPartyBooleanData;
use App\RelatedPartyDateData;
use App\RelatedPartyDocumentData;
use App\RelatedPartyDropdownData;
use App\RelatedPartyMultipleAttachment;
use App\RelatedPartyMultipleAttachmentData;
use App\RelatedPartyNotificationData;
use App\RelatedPartyTemplateEmailData;
use App\RelatedPartyTextareaData;
use App\RelatedPartyTextData;
use App\Template;
use App\TriggerType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Step;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\ActionableTextareaData;
// use Maatwebsite\Excel\Excel;
use App\User;
use App\BranchReport;
use App\BranchReportColumns;
use App\ClientHelper;
use App\EmailTemplate;
use App\Actions;
use App\ActionsAssigned;
use App\ActivityInClientBasket;
use App\ActivityLog;
use App\ActivityStepVisibilityRule;
use App\ActivityVisibilityRule;
use App\BillboardMessage;
use App\ClientActivity;
use App\ClientComment;
use App\ClientCRFForm;
use App\ClientProcess;
use App\ClientUser;
use App\ClientVisibleActivity;
use App\ClientVisibleStep;
use App\Document;
use App\Events\NotificationEvent;
use App\FormInputAmountData;
use App\FormInputCheckboxData;
use App\FormInputIntegerData;
use App\FormInputPercentageData;
use App\FormInputRadioData;
use App\FormLog;
use App\HelperFunction;
use App\Http\Requests\StoreClientFormRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreFollowRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Log;
use App\Mail\AssignedConsultantNotify;
use App\Mail\NotificationMail;
use App\Notification;
use App\OfficeUser;
use App\ProcessArea;
use App\Project;
use App\Referrer;
use App\TokenStore\MicrosoftTokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\UserNotification;
use App\UserTask;
use App\WhatsappTemplate;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Mail;
use App\Mail\TemplateMail;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\ClientForm;
use App\BusinessUnits;
use App\Presentation;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;
use App\Mail\ClientMail;
// use PDF;
use Excel;

class ContactReportController extends Controller
{
    private $helper;
    public function __construct()
    {
        $this->middleware('auth')->except(['sendtemplate', 'sendnotification']);
        $this->middleware('auth:api')->only(['sendtemplate', 'sendnotification']);
        $this->middleware('permission:maintain_client')->except(['create', 'store']);

        $this->progress_colours = [
            'not_started' => 'background-color: rgba(64,159,255, 0.15)',
            'started' => 'background-color: rgba(255,255,70, 0.15)',
            'progress_completed' => 'background-color: rgba(60,255,70, 0.15)',
        ];

        $this->helper = new ClientHelper();
    }

    public function index()
    {
        $reports = ContactReport::orderBy('name')->with('user')->get();
        // $branches = Client::whereNotNull('client_id')->orderBy('company')->select('company', 'id')->get();
        $clients = Client::orderBy('company')->get();
        // $fund_consultants = User::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')->get();

        // dd($reports);
        $parameters = [
            'reports' => $reports,
            'clients' => $clients,
            // 'fund_consultants' => $fund_consultants
        ];
        return view('contactreports.index')->with($parameters);
    }

    public function create(){

        // $branches = Client::whereNotNull('client_id')->orderBy('company')->pluck('company', 'id')->prepend('All branches', 'all');
        $clients = Client::orderBy('company')->pluck('company', 'id')->prepend('Select client', 'all');
        // $fund_consultants = User::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')->pluck('name', 'id')->prepend('All consultants', 'all');
        $form_inputs = FormSectionInputs::where('form_section_id', 39)->get();
        
            // dd($form_inputs);

        $parameters = [
            'process' => Process::where('process_type_id',1)->pluck('name','id'),
            'crm' => Forms::orderBy('name')->pluck('name','id'),
            'clients' => $clients,
            // 'fund_consultants' => $fund_consultants
        ];
        return view('contactreports.create')->with($parameters);
    }

    

    public function store(Request $request){
        // dd($request->input('crm'));

        $request->validate([
            'name'=>'required',
            'client_id'=>'required|integer',
        ]);

        $breport = new ContactReport();
        $breport->name = $request->input('name');
        $breport->client_id = $request->input('client_id');
        $breport->user_id = Auth::id();
        $breport->save();

        return redirect(route('contact_report.index'))->with('flash_success', 'Contact Details report created successfully');
    }

    public function edit($contact_report_id){

        $report = ContactReport::where('id',$contact_report_id)->first();

        $clients = Client::orderBy('company')->pluck('company', 'id')->prepend('Select client', 'all');
        // $fund_consultants = User::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')->pluck('name', 'id')->prepend('All consultants', 'all');
        $form_inputs = FormSectionInputs::where('form_section_id', 39)->get();
        
            // dd($form_inputs);

        $parameters = [
            'process' => Process::where('process_type_id',1)->pluck('name','id'),
            'crm' => Forms::orderBy('name')->pluck('name','id'),
            'clients' => $clients,
            'report' => $report
            // 'fund_consultants' => $fund_consultants
        ];

        return view('contactreports.edit')->with($parameters);
    }

    public function update($contact_report_id, Request $request){

        $request->validate([
            'name'=>'required',
            'client_id'=>'required|integer',
        ]);

        $breport = ContactReport::find($contact_report_id);
        $breport->name = $request->input('name');
        $breport->client_id = $request->input('client_id');
        // $breport->date_from = $request->input('date_from');
        // $breport->date_to = $request->input('date_to');
        $breport->user_id = Auth::id();
        $breport->save();

        return redirect(route('contact_report.index'))->with('flash_success', 'Contact Details report updated successfully');
    }

    public function show(Request $request,$contact_report_id){

        $report = ContactReport::find($contact_report_id);
        $client = Client::where('id', $report->client_id)->orderBy('company')->first();

        $client_details = $this->helper->clientDetails($client, 1)['forms'];
        $inputs = $client_details[3]['Profile']['data'][38]['inputs'];
        $activities = collect($inputs)->sortBy('id')->toArray();

        $acts1 = [];
        $acts2 = [];
        $acts3 = [];
        $acts4 = [];
        $acts5 = [];
        $acts6 = [];

        foreach($activities as $activity){
            // dd($activity['id']);
            if($activity['id'] == 728 || $activity['id'] == 729 || $activity['id'] == 730 || $activity['id'] == 732 || $activity['id'] == 733){
                array_push($acts1, $activity);
            }
            if($activity['id'] == 739 || $activity['id'] == 740 || $activity['id'] == 741 || $activity['id'] == 743 || $activity['id'] == 744){
                array_push($acts2, $activity);
            }
            if($activity['id'] == 748 || $activity['id'] == 749 || $activity['id'] == 750 || $activity['id'] == 752 || $activity['id'] == 753){
                array_push($acts3, $activity);
            }
            if($activity['id'] == 757 || $activity['id'] == 758 || $activity['id'] == 759 || $activity['id'] == 761 || $activity['id'] == 762){
                array_push($acts4, $activity);
            }
            if($activity['id'] == 766 || $activity['id'] == 767 || $activity['id'] == 768 || $activity['id'] == 770 || $activity['id'] == 771){
                array_push($acts5, $activity);
            }
            if($activity['id'] == 775 || $activity['id'] == 776 || $activity['id'] == 777 || $activity['id'] == 779 || $activity['id'] == 780){
                array_push($acts6, $activity);
            }
        }

        // dd($acts6);

        $parameters = [
            'report' => $report,
            'client' => $client,
            'acts1' => $acts1,
            'acts2' => $acts2,
            'acts3' => $acts3,
            'acts4' => $acts4,
            'acts5' => $acts5,
            'acts6' => $acts6,
            // 'progress' => $progress,
            // 'consultants' => $consultants
        ];

        return view('contactreports.show')->with($parameters);
    }

    public function destroy($contact_report_id){
        ContactReport::destroy($contact_report_id);
        return redirect()->route("contact_report.index")->with('flash_success','Contact Details report deleted successfully');
    }

    public function export(Excel $excel,Request $request,$contact_report_id,$report_type){

        $np = 0;
        $qa = 0;
        $total = 0;
        $rows = 0;

        $report_name = '';
        $report_columns = array();
        $activity_id = array();
        $rp_activity_id = array();
        $data = array();


        if($report_type == "process") {
            $report = ContactReport::with('risk_report_columns.activity_name')->where('id', $contact_report_id)->withTrashed()->first();

            $clients = Client::with(['referrer', 'process.steps2.activities.actionable.data', 'introducer', 'consultant', 'committee', 'trigger'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
                DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
                DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
                DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
                DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
            );

            if ($request->has('q') && $request->input('q') != '') {

                $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
            }

            $clients = $clients->get();

            if ($request->has('user') && $request->input('user') != null) {
                $clients = $clients->filter(function ($client) use ($request) {
                    return $client->consultant_id == $request->input('user');
                });
            }

            if ($request->has('f') && $request->input('f') != '') {
                $p = $request->input('f');
                $clients = $clients->filter(function ($client) use ($p) {
                    return $client->instruction_date >= $p;
                });
            }

            if ($request->has('t') && $request->input('t') != '') {
                $p = $request->input('t');
                $clients = $clients->filter(function ($client) use ($p) {
                    return Carbon::parse($client->instruction_date)->format("Y-m-d") <= $p;
                });
            }

            if (isset($report) && $report->group_report == '0') {

                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {
                    array_push($report_columns, $report_activity->activity_name["name"]);
                    array_push($activity_id, $report_activity->activity_name["id"]);
                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = Activity::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\ActionableBoolean':
                                    $yn_value = '';

                                    $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableDate':
                                    $data_value = '';

                                    $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableText':
                                    $data_value = '';

                                    $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableTextarea':
                                    $data_value = '';

                                    $data2 = ActionableTextareaData::where('client_id', $client->id)->where('actionable_textarea_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDropdown':
                                    $data_value = '';

                                    $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDocument':
                                    $yn_value = '';

                                    $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $yn_value = '';

                                    $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableNotification':
                                    $yn_value = '';

                                    $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'type' => 'P',
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            } else {
                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {

                    $rows = Activity::select(DB::raw("DISTINCT grouping"))->where('step_id', $report_activity->activity_name["step_id"])->where('grouping', '>', 0)->get()->count();

                    array_push($report_columns, $report_activity->activity_name["name"]);

                    if ($report_activity->activity_name["grouping"] > 0) {
                        array_push($activity_id, $report_activity->activity_name["id"]);
                    } else {
                        array_push($activity_id, $report_activity->activity_name["id"]);
                    }
                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = Activity::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\ActionableBoolean':
                                    $yn_value = '';

                                    $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableDate':
                                    $data_value = '';

                                    $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableText':
                                    $data_value = '';

                                    $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    $data[$activity["id"]] = $data_value;
                                    //array_push($data, $data_value);
                                    break;
                                case 'App\ActionableTextarea':
                                    $data_value = '';

                                    $data2 = ActionableTextareaData::where('client_id', $client->id)->where('actionable_textarea_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDropdown':
                                    $data_value = '';

                                    $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDocument':
                                    $yn_value = '';

                                    $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $yn_value = '';

                                    $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableNotification':
                                    $yn_value = '';

                                    $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'type' => 'P',
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }

            }
        }

        if($report_type == "crm") {
            $report = ContactReport::with('risk_report_columns.crm_name')->where('id',$contact_report_id)->withTrashed()->first();

            $clients = Client::with(['crm.sections.form_section_inputs.input.data'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
                DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
                DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
                DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
                DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
            );

            if ($request->has('q') && $request->input('q') != '') {

                $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
            }

            $clients = $clients->get();

            if ($request->has('user') && $request->input('user') != null) {
                $clients = $clients->filter(function ($client) use ($request) {
                    return $client->consultant_id == $request->input('user');
                });
            }

            if ($request->has('f') && $request->input('f') != '') {
                $p = $request->input('f');
                $clients = $clients->filter(function ($client) use ($p) {
                    return $client->instruction_date >= $p;
                });
            }

            if ($request->has('t') && $request->input('t') != '') {
                $p = $request->input('t');
                $clients = $clients->filter(function ($client) use ($p) {
                    return Carbon::parse($client->instruction_date)->format("Y-m-d") <= $p;
                });
            }

            if(isset($report) && $report->group_report == '0') {

                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {
                    if($report_activity->crm_name["input_type"] != 'App\FormInputSubheading' && $report_activity->crm_name["input_type"] != 'App\FormInputHeading') {
                        array_push($report_columns, $report_activity->crm_name["name"]);
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    }
                }
//dd($report_columns);
                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = FormSectionInputs::where('id', $value)->first();

                            switch ($activity["input_type"]) {
                                case 'App\FormInputBoolean':
                                    $yn_value = '';

                                    $data2 = FormInputBooleanData::where('client_id', $client->id)->where('form_input_boolean_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputDate':
                                    $data_value = '';

                                    $data2 = FormInputDateData::where('client_id', $client->id)->where('form_input_date_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputText':
                                    $data_value = '';

                                    $data2 = FormInputTextData::where('client_id', $client->id)->where('form_input_text_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputTextarea':
                                    $data_value = '';

                                    $data2 = FormInputTextareaData::where('client_id', $client->id)->where('form_input_textarea_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDropdown':
                                    $data_value = '';

                                    $data2 = FormInputDropdownData::with('item')->where('client_id', $client->id)->where('form_input_dropdown_id', $activity->input_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDocument':
                                    $yn_value = '';

                                    $data2 = FormInputDocumentData::where('client_id', $client->id)->where('form_input_document_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputTemplateEmail':
                                    $yn_value = '';

                                    $data2 = FormInputTemplateEmailData::where('client_id', $client->id)->where('form_input_template_email_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputNotification':
                                    $yn_value = '';

                                    $data2 = FormInputNotificationData::where('client_id', $client->id)->where('form_input_notification_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = FormInputMultipleAttachmentData::where('client_id', $client->id)->where('form_input_ma_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            } else {
                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {

                    $rows = FormSectionInputs::select(DB::raw("DISTINCT grouping"))->where('form_section_id',$report_activity->crm_name["form_section_id"])->where('grouping','>',0)->get()->count();

                    array_push($report_columns, $report_activity->crm_name["name"]);

                    if($report_activity->activity_name["grouping"] > 0) {
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    } else {
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    }
                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = FormSectionInputs::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\FormInputBoolean':
                                    $yn_value = '';

                                    $data2 = FormInputBooleanData::where('client_id', $client->id)->where('form_input_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputDate':
                                    $data_value = '';

                                    $data2 = FormInputDateData::where('client_id', $client->id)->where('form_input_date_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputText':
                                    $data_value = '';

                                    $data2 = FormInputTextData::where('client_id', $client->id)->where('form_input_text_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    $data[$activity["id"]] = $data_value;
                                    //array_push($data, $data_value);
                                    break;
                                case 'App\FormInputTextarea':
                                    $data_value = '';

                                    $data2 = FormInputTextareaData::where('client_id', $client->id)->where('form_input_textarea_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDropdown':
                                    $data_value = '';

                                    $data2 = FormInputDropdownData::with('item')->where('client_id', $client->id)->where('form_input_dropdown_id', $activity->input_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDocument':
                                    $yn_value = '';

                                    $data2 = FormInputDocumentData::where('client_id', $client->id)->where('form_input_document_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputTemplateEmail':
                                    $yn_value = '';

                                    $data2 = FormInputTemplateEmailData::where('client_id', $client->id)->where('form_input_template_email_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputNotification':
                                    $yn_value = '';

                                    $data2 = FormInputNotificationData::where('client_id', $client->id)->where('form_input_notification_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = FormInputMultipleAttachmentData::where('client_id', $client->id)->where('input_ma_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            }

        }

            $parameters = [
                'np' => $np,
                'qa' => $qa,
                'report_id' => $contact_report_id,
                'report_name' => $report_name,
                'fields' => $report_columns,
                'clients' => (isset($client_data) ? $client_data : []),
                'activity' => '',
                'total' => $total
            ];


        return $excel->download(new RiskReportExport($client_data,$report_columns), 'clients_'.date('Y_m_d_H_i_s').'.xlsx');
    }

    public function pdfexport($contact_report_id,$report_type,Request $request)
    {

        $request->session()->forget('path_route');

        $np = 0;
        $qa = 0;
        $total = 0;
        $rows = 0;

        $report_name = '';
        $report_columns = array();
        $activity_id = array();
        $rp_activity_id = array();
        $data = array();


        if($report_type == "process") {
            $report = ContactReport::with('risk_report_columns.activity_name')->where('id', $contact_report_id)->withTrashed()->first();

            $clients = Client::with(['referrer', 'process.steps2.activities.actionable.data', 'introducer', 'consultant', 'committee', 'trigger'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
                DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
                DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
                DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
                DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
            );

            if ($request->has('q') && $request->input('q') != '') {

                $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
            }

            $clients = $clients->get();

            if ($request->has('user') && $request->input('user') != null) {
                $clients = $clients->filter(function ($client) use ($request) {
                    return $client->consultant_id == $request->input('user');
                });
            }

            if ($request->has('f') && $request->input('f') != '') {
                $p = $request->input('f');
                $clients = $clients->filter(function ($client) use ($p) {
                    return $client->instruction_date >= $p;
                });
            }

            if ($request->has('t') && $request->input('t') != '') {
                $p = $request->input('t');
                $clients = $clients->filter(function ($client) use ($p) {
                    return Carbon::parse($client->instruction_date)->format("Y-m-d") <= $p;
                });
            }

            if (isset($report) && $report->group_report == '0') {

                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {
                    array_push($report_columns, $report_activity->activity_name["name"]);
                    array_push($activity_id, $report_activity->activity_name["id"]);

                    $rp_activity = ActivityRelatedPartyLink::select('related_activity')->where('primary_activity', $report_activity->activity_name["id"])->first();

                    if ($rp_activity) {
                        array_push($rp_activity_id, $rp_activity->related_activity);
                    }
                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = Activity::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\ActionableBoolean':
                                    $yn_value = '';

                                    $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableDate':
                                    $data_value = '';

                                    $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableText':
                                    $data_value = '';

                                    $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableTextarea':
                                    $data_value = '';

                                    $data2 = ActionableTextareaData::where('client_id', $client->id)->where('actionable_textarea_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDropdown':
                                    $data_value = '';

                                    $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDocument':
                                    $yn_value = '';

                                    $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $yn_value = '';

                                    $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableNotification':
                                    $yn_value = '';

                                    $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'type' => 'P',
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            } else {
                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {

                    $rows = Activity::select(DB::raw("DISTINCT grouping"))->where('step_id', $report_activity->activity_name["step_id"])->where('grouping', '>', 0)->get()->count();

                    array_push($report_columns, $report_activity->activity_name["name"]);

                    if ($report_activity->activity_name["grouping"] > 0) {
                        array_push($activity_id, $report_activity->activity_name["id"]);
                    } else {
                        array_push($activity_id, $report_activity->activity_name["id"]);
                    }

                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = Activity::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\ActionableBoolean':
                                    $yn_value = '';

                                    $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableDate':
                                    $data_value = '';

                                    $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableText':
                                    $data_value = '';

                                    $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    $data[$activity["id"]] = $data_value;
                                    //array_push($data, $data_value);
                                    break;
                                case 'App\ActionableTextarea':
                                    $data_value = '';

                                    $data2 = ActionableTextareaData::where('client_id', $client->id)->where('actionable_textarea_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDropdown':
                                    $data_value = '';

                                    $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\ActionableDocument':
                                    $yn_value = '';

                                    $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $yn_value = '';

                                    $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableNotification':
                                    $yn_value = '';

                                    $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'type' => 'P',
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'client_id' => '',
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => '',
                            'data' => $data
                        ];

                        $total++;
                    }
                }

            }
        }

        if($report_type == "crm") {
            $report = ContactReport::with('risk_report_columns.crm_name')->where('id',$contact_report_id)->withTrashed()->first();

            $clients = Client::with(['crm.sections.form_section_inputs.input.data'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
                DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
                DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
                DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
                DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
                DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
            );

            if ($request->has('q') && $request->input('q') != '') {

                $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                    ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
            }

            $clients = $clients->get();

            if ($request->has('user') && $request->input('user') != null) {
                $clients = $clients->filter(function ($client) use ($request) {
                    return $client->consultant_id == $request->input('user');
                });
            }

            if ($request->has('f') && $request->input('f') != '') {
                $p = $request->input('f');
                $clients = $clients->filter(function ($client) use ($p) {
                    return $client->instruction_date >= $p;
                });
            }

            if ($request->has('t') && $request->input('t') != '') {
                $p = $request->input('t');
                $clients = $clients->filter(function ($client) use ($p) {
                    return Carbon::parse($client->instruction_date)->format("Y-m-d") <= $p;
                });
            }

            if(isset($report) && $report->group_report == '0') {

                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {
                    if($report_activity->crm_name["input_type"] != 'App\FormInputSubheading' && $report_activity->crm_name["input_type"] != 'App\FormInputHeading') {
                        array_push($report_columns, $report_activity->crm_name["name"]);
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    }
                }
//dd($report_columns);
                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = FormSectionInputs::where('id', $value)->first();

                            switch ($activity["input_type"]) {
                                case 'App\FormInputBoolean':
                                    $yn_value = '';

                                    $data2 = FormInputBooleanData::where('client_id', $client->id)->where('form_input_boolean_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputDate':
                                    $data_value = '';

                                    $data2 = FormInputDateData::where('client_id', $client->id)->where('form_input_date_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputText':
                                    $data_value = '';

                                    $data2 = FormInputTextData::where('client_id', $client->id)->where('form_input_text_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputTextarea':
                                    $data_value = '';

                                    $data2 = FormInputTextareaData::where('client_id', $client->id)->where('form_input_textarea_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDropdown':
                                    $data_value = '';

                                    $data2 = FormInputDropdownData::with('item')->where('client_id', $client->id)->where('form_input_dropdown_id', $activity->input_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDocument':
                                    $yn_value = '';

                                    $data2 = FormInputDocumentData::where('client_id', $client->id)->where('form_input_document_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputTemplateEmail':
                                    $yn_value = '';

                                    $data2 = FormInputTemplateEmailData::where('client_id', $client->id)->where('form_input_template_email_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputNotification':
                                    $yn_value = '';

                                    $data2 = FormInputNotificationData::where('client_id', $client->id)->where('form_input_notification_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = FormInputMultipleAttachmentData::where('client_id', $client->id)->where('form_input_ma_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            } else {
                $report_name = $report->name;
                foreach ($report->risk_report_columns as $report_activity) {

                    $rows = FormSectionInputs::select(DB::raw("DISTINCT grouping"))->where('form_section_id',$report_activity->crm_name["form_section_id"])->where('grouping','>',0)->get()->count();

                    array_push($report_columns, $report_activity->crm_name["name"]);

                    if($report_activity->activity_name["grouping"] > 0) {
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    } else {
                        array_push($activity_id, $report_activity->crm_name["id"]);
                    }
                }

                foreach ($clients as $client) {
                    if ($client) {
                        $data = [];

                        foreach ($activity_id as $key => $value) {
                            $activity = FormSectionInputs::where('id', $value)->first();

                            switch ($activity["actionable_type"]) {
                                case 'App\FormInputBoolean':
                                    $yn_value = '';

                                    $data2 = FormInputBooleanData::where('client_id', $client->id)->where('form_input_boolean_id', $activity->actionable_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputDate':
                                    $data_value = '';

                                    $data2 = FormInputDateData::where('client_id', $client->id)->where('form_input_date_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputText':
                                    $data_value = '';

                                    $data2 = FormInputTextData::where('client_id', $client->id)->where('form_input_text_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    $data[$activity["id"]] = $data_value;
                                    //array_push($data, $data_value);
                                    break;
                                case 'App\FormInputTextarea':
                                    $data_value = '';

                                    $data2 = FormInputTextareaData::where('client_id', $client->id)->where('form_input_textarea_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    $data_value = $data2["data"];

                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDropdown':
                                    $data_value = '';

                                    $data2 = FormInputDropdownData::with('item')->where('client_id', $client->id)->where('form_input_dropdown_id', $activity->input_id)->get();

                                    foreach ($data2 as $key => $value):
                                        if (count($data2) > 1) {
                                            $data_value .= $value["item"]["name"] . ', ';
                                        } else {
                                            $data_value .= $value["item"]["name"];
                                        }
                                    endforeach;
                                    array_push($data, $data_value);
                                    break;
                                case 'App\FormInputDocument':
                                    $yn_value = '';

                                    $data2 = FormInputDocumentData::where('client_id', $client->id)->where('form_input_document_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["document_id"])) {
                                        $yn_value = "Yes";
                                    } else {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputTemplateEmail':
                                    $yn_value = '';

                                    $data2 = FormInputTemplateEmailData::where('client_id', $client->id)->where('form_input_template_email_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputNotification':
                                    $yn_value = '';

                                    $data2 = FormInputNotificationData::where('client_id', $client->id)->where('form_input_notification_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                case 'App\FormInputMultipleAttachment':
                                    $yn_value = '';

                                    $data2 = FormInputMultipleAttachmentData::where('client_id', $client->id)->where('input_ma_id', $activity->input_id)->orderBy('created_at', 'desc')->take(1)->first();

                                    if (isset($data2["data"]) && $data2["data"] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($data2["data"]) && $data2["data"] == '0') {
                                        $yn_value = "No";
                                    }
                                    array_push($data, $yn_value);
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                        }
                        $client_data[$client->id] = [
                            'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                            'id' => $client->id,
                            'case_nr' => $client->case_number,
                            'cif_code' => $client->cif_code,
                            'committee' => isset($client->committee) ? $client->committee->name : null,
                            'trigger' => ($client->trigger_type_id > 0 ? $client->trigger->name : ''),
                            'instruction_date' => $client->instruction_date,
                            'completed_at' => ($client->completed_at != null ? $client->completed_at : ''),
                            'date_submitted_qa' => $client->qa_start_date,
                            'assigned' => ($client->consultant_id != null ? 1 : 0),
                            'consultant' => isset($client->consultant_id) ? $client->consultant->first_name . ' ' . $client->consultant->last_name : null,
                            'data' => $data
                        ];

                        $total++;
                    }
                }
            }

        }
//dd($report_columns);
        /*$parameters = [
            'np' => $np,
            'qa' => $qa,
            'report_type' => $report_type,
            'report_id' => $contact_report_id,
            'report_name' => $report_name,
            'fields' => $report_columns,
            'clients' => (isset($client_data) ? $client_data : []),
            'committee' => Committee::orderBy('name')->pluck('name', 'id')->prepend('All committees', 'all'),
            'trigger' => TriggerType::orderBy('name')->pluck('name', 'id')->prepend('All trigger types', 'all'),
            'assigned_user' => Client::all()->keyBy('consultant_id')->map(function ($consultant){
                return isset($consultant->consultant)?$consultant->consultant->first_name.' '.$consultant->consultant->last_name:null;
            })->sort(),
            'activity' => '',
            'total' => $total
        ];*/

        $parameters = [
            'report_id' => $contact_report_id,
            'report_name' => $report_name,
            'fields' => $report_columns,
            'clients' => $client_data,
            'steps' => FormSection::orderBy('form_id')->orderBy('order')->pluck('name', 'id')->prepend('All crm', ''),
            'activity' => ''
        ];

        $pdf = PDF::loadView('pdf.riskreport2', $parameters)->setPaper('a4')->setOrientation('landscape');
        return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');
    }

    public function getActivities(Request $request,$processid){

        $step_arr = array();
        $activities_arr = array();

        if($request->input('type') == "process") {
            $steps = Step::where('process_id', $processid)->orderBy('order', 'asc')->get();

            foreach($steps as $step){
                $step_arr2 = array();

                $step_arr2['id'] = $step->id;
                $step_arr2['name'] = $step->name;

                //array_push($step_arr, $step_arr2);

                $activities = Activity::select('activities.id','activities.name','activities.actionable_type','activities.grouping')->leftJoin('steps','steps.id','activities.step_id')->where('steps.process_id',$processid)->where('steps.deleted_at',null)->where('activities.deleted_at',null)->where('activities.step_id',$step->id)->orderBy('activities.step_id','asc')->orderBy('activities.order','asc')->get();
                //dd($activities);
                $step_arr2['activity'] = array();
                foreach($activities as $activity){

                    if($activity->grouping != null && $activity->grouping > 0) {
                        if($activity->grouping == 1) {
                            array_push($step_arr2['activity'], [
                                'id' => $activity->id,
                                'name' => $activity->name,
                                'type' => $activity->actionable_type,
                                'step' => $activity->step_id,
                                'grouping' => '1'
                            ]);
                        }
                    } else {
                        array_push($step_arr2['activity'], [
                            'id' => $activity->id,
                            'name' => $activity->name,
                            'type' => $activity->actionable_type,
                            'step' => $activity->step_id,
                            'grouping' => '0'
                        ]);
                    }


                }

                array_push($step_arr,$step_arr2);
            }
        }

        if($request->input('type') == "crm") {
            $steps = FormSection::where('form_id', $processid)->orderBy('order', 'asc')->get();

            foreach($steps as $step){
                $step_arr2 = array();

                $step_arr2['id'] = $step->id;
                $step_arr2['name'] = $step->name;

                //array_push($step_arr, $step_arr2);

                $activities = FormSectionInputs::select('form_section_inputs.id','form_section_inputs.name','form_section_inputs.input_type','form_section_inputs.grouping')->leftJoin('form_sections','form_sections.id','form_section_inputs.form_section_id')->where('form_sections.form_id',$processid)->where('form_sections.deleted_at',null)->where('form_section_inputs.deleted_at',null)->where('form_section_inputs.form_section_id',$step->id)->orderBy('form_section_inputs.form_section_id','asc')->orderBy('form_section_inputs.order','asc')->get();
                //dd($activities);
                $step_arr2['activity'] = array();
                foreach($activities as $activity){
                    if($activity->input_type != "App\Subheading" && $activity->input_type != "App\Heading") {
                        if ($activity->grouping != null && $activity->grouping > 0) {
                            if ($activity->grouping == 1) {
                                array_push($step_arr2['activity'], [
                                    'id' => $activity->id,
                                    'name' => $activity->name,
                                    'type' => $activity->input_type,
                                    'step' => $activity->form_section_id,
                                    'grouping' => '1'
                                ]);
                            }
                        } else {
                            array_push($step_arr2['activity'], [
                                'id' => $activity->id,
                                'name' => $activity->name,
                                'type' => $activity->input_type,
                                'step' => $activity->form_section_id,
                                'grouping' => '0'
                            ]);
                        }
                    }


                }

                array_push($step_arr,$step_arr2);
            }
        }





        return response()->json($step_arr);
    }

    public function getSelectedActivities($contact_report_id){

        $sa = array();

        $process = ContactReport::where('id',$contact_report_id)->first();
//return $process;
        $selected_activities = RiskReportColumns::select('activity_id')->where('contact_report_id',$contact_report_id)->get();
//return $selected_activities;
        foreach($selected_activities as $result){
            array_push($sa,$result->activity_id);
        }

        $step_arr = array();
        $activities_arr = array();

        if($process->type == "process") {
            $steps = Step::where('process_id', $process->process_id)->orderBy('order', 'asc')->get();

            foreach ($steps as $step) {
                $step_arr2 = array();

                $step_arr2['id'] = $step->id;
                $step_arr2['name'] = $step->name;

                $activities = Activity::select('activities.id', 'activities.name', 'activities.actionable_type', 'activities.grouping')->leftJoin('steps', 'steps.id', 'activities.step_id')->where('activities.step_id', $step->id)->where('steps.process_id', $process->process_id)->where('steps.deleted_at', null)->where('activities.deleted_at', null)->where('activities.step_id', $step->id)->orderBy('activities.step_id', 'asc')->orderBy('activities.order', 'asc')->get();

                $step_arr2['activity'] = array();
                foreach ($activities as $activity) {

                    if (($key = array_search($activity->id, $sa)) === false) {
                        if ($activity->grouping != null && $activity->grouping > 0) {
                            if ($activity->grouping == 1) {
                                array_push($step_arr2['activity'], [
                                    'id' => $activity->id,
                                    'name' => $activity->name,
                                    'type' => $activity->actionable_type,
                                    'selected' => '1',
                                    'grouping' => '1'
                                ]);
                            }
                        } else {
                            array_push($step_arr2['activity'], [
                                'id' => $activity->id,
                                'name' => $activity->name,
                                'type' => $activity->actionable_type,
                                'selected' => '1',
                                'grouping' => '0'
                            ]);
                        }
                    } else {
                        if ($activity->grouping != null && $activity->grouping > 0) {
                            if ($activity->grouping == 1) {
                                array_push($step_arr2['activity'], [
                                    'id' => $activity->id,
                                    'name' => $activity->name,
                                    'type' => $activity->actionable_type,
                                    'selected' => '0',
                                    'grouping' => '1'
                                ]);
                            }
                        } else {
                            array_push($step_arr2['activity'], [
                                'id' => $activity->id,
                                'name' => $activity->name,
                                'type' => $activity->actionable_type,
                                'selected' => '0',
                                'grouping' => '0'
                            ]);
                        }
                    }

                }

                array_push($step_arr, $step_arr2);
            }
        }

        if($process->type == "crm") {

            $steps = FormSection::where('form_id', $process->process_id)->orderBy('order', 'asc')->get();

            foreach ($steps as $step) {
                $step_arr2 = array();

                $step_arr2['id'] = $step->id;
                $step_arr2['name'] = $step->name;

                $activities = FormSectionInputs::select('form_section_inputs.id', 'form_section_inputs.name', 'form_section_inputs.input_type', 'form_section_inputs.grouping')->leftJoin('form_sections', 'form_sections.id', 'form_section_inputs.form_section_id')->where('form_section_inputs.form_section_id', $step->id)->where('form_sections.form_id', $process->process_id)->where('form_sections.deleted_at', null)->where('form_section_inputs.deleted_at', null)->where('form_section_inputs.form_section_id', $step->id)->orderBy('form_section_inputs.form_section_id', 'asc')->orderBy('form_section_inputs.order', 'asc')->get();

                $step_arr2['activity'] = array();
                foreach ($activities as $activity) {

                    if($activity->input_type != "App\Subheading" && $activity->input_type != "App\Heading") {
                        if (($key = array_search($activity->id, $sa)) === false) {
                            if ($activity->grouping != null && $activity->grouping > 0) {
                                if ($activity->grouping == 1) {
                                    array_push($step_arr2['activity'], [
                                        'id' => $activity->id,
                                        'name' => $activity->name,
                                        'type' => $activity->input_type,
                                        'selected' => '0',
                                        'grouping' => '1'
                                    ]);
                                }
                            } else {
                                array_push($step_arr2['activity'], [
                                    'id' => $activity->id,
                                    'name' => $activity->name,
                                    'type' => $activity->input_type,
                                    'selected' => '0',
                                    'grouping' => '0'
                                ]);
                            }
                        } else {
                            if ($activity->grouping != null && $activity->grouping > 0) {
                                if ($activity->grouping == 1) {
                                    array_push($step_arr2['activity'], [
                                        'id' => $activity->id,
                                        'name' => $activity->name,
                                        'type' => $activity->input_type,
                                        'selected' => '1',
                                        'grouping' => '1'
                                    ]);
                                }
                            } else {
                                array_push($step_arr2['activity'], [
                                    'id' => $activity->id,
                                    'name' => $activity->name,
                                    'type' => $activity->input_type,
                                    'selected' => '1',
                                    'grouping' => '0'
                                ]);
                            }
                        }
                    }

                }

                array_push($step_arr, $step_arr2);
            }
        }

//        return $step_arr;

        return response()->json($step_arr);
    }

    public function chevronProgress($clientid, Step $step)
    {
        // dd($step->process_id);
        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $client->process->getStageHex(0);

        // if($client->step_id == $step->id)
        //     $client_progress = $client->process->getStageHex(1);

        // if($client->step_id > $step->id)
        //     $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();
//dd($steps);
        $process_progress = $client->getProcessStepProgress($step);
        //dd($process_progress);
        $completed = "";
        $not_completed = "";
        $stepw = $process_progress[0];
        //dd($step['order']);
        foreach ($stepw['activities'] as $activity):
            if (isset($activity['value'])) {
                $completed .= '<tr><td>' . $activity['name'] . '</tr></td>';
            } else {
                $not_completed .= '<tr><td>' . $activity['name'] . '</tr></td>';
            }
        endforeach;

        $step_data = [];
        foreach ($steps as $a_step):
            $c_step_order = Step::where('id', $client->step_id)->where('process_id', $step->process_id)->withTrashed()->first();

            $progress_color = $client->process->getStageHex(0);

            if($c_step_order->order == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
            }

            if($c_step_order->order > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
            }

            if ($c_step_order["order"] == $a_step->order && $client->completed_at != null && $a_step->process_id == $client->process_id) {
                $progress_color = $client->process->getStageHex(2);
            }


            $tmp_step = [
                'id' => $a_step->id,
                'process_id' => $a_step->process_id,
                'name' => $a_step->name,
               'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        if (count($step_data) > 1) {
            foreach ($step_data as $key => $part) {
                $sort[$key] = strtotime($part['order']);
            }

            array_multisort($sort, SORT_ASC, $step_data);
        }

        $max_step = Step::where('process_id', $client->process_id)->max('id');

        $client_details = $this->helper->clientDetails($client, 1)['forms'];

        $next_step = $step->id;
        if($next_step == $max_step)
            $next_step = $max_step;
        else
            $next_step = $step->id + 1;

        //dd();
        //dd(User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'));
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');
        $parameters = [
            'client' => $client,
            'step' => $step,
            'next_step' => $next_step,
            'process_progress' => $process_progress,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'completed' => $completed,
            'not_completed' => $not_completed,
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
            'client_details' => $client_details,
            'client_emails' => $this->helper->clientEmails($client, 1),
            'in_details_basket' => $this->helper->detailedClientBasket($client, 1)['cd'],
            'process_id' => $step->process_id,
            'trade_name' => ($client->crm_id == 4 ? FormInputTextData::where('form_input_text_id',465)->where('client_id',$client->id)->orderBy('created_at','desc')->first()->data : ''),
            'is_form' => 0,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
        ];

        return $parameters;
    }

    public function pdf(Request $request,$contact_report_id){

        $report = ContactReport::find($contact_report_id);
        $client = Client::where('id', $report->client_id)->orderBy('company')->first();

        $client_details = $this->helper->clientDetails($client, 1)['forms'];
        $inputs = $client_details[3]['Profile']['data'][38]['inputs'];
        $activities = collect($inputs)->sortBy('id')->toArray();

        $acts1 = [];
        $acts2 = [];
        $acts3 = [];
        $acts4 = [];
        $acts5 = [];
        $acts6 = [];

        foreach($activities as $activity){
            // dd($activity['id']);
            if($activity['id'] == 728 || $activity['id'] == 729 || $activity['id'] == 730 || $activity['id'] == 732 || $activity['id'] == 733){
                array_push($acts1, $activity);
            }
            if($activity['id'] == 739 || $activity['id'] == 740 || $activity['id'] == 741 || $activity['id'] == 743 || $activity['id'] == 744){
                array_push($acts2, $activity);
            }
            if($activity['id'] == 748 || $activity['id'] == 749 || $activity['id'] == 750 || $activity['id'] == 752 || $activity['id'] == 753){
                array_push($acts3, $activity);
            }
            if($activity['id'] == 757 || $activity['id'] == 758 || $activity['id'] == 759 || $activity['id'] == 761 || $activity['id'] == 762){
                array_push($acts4, $activity);
            }
            if($activity['id'] == 766 || $activity['id'] == 767 || $activity['id'] == 768 || $activity['id'] == 770 || $activity['id'] == 771){
                array_push($acts5, $activity);
            }
            if($activity['id'] == 775 || $activity['id'] == 776 || $activity['id'] == 777 || $activity['id'] == 779 || $activity['id'] == 780){
                array_push($acts6, $activity);
            }
        }

        // dd($acts6);

        $parameters = [
            'report' => $report,
            'client' => $client,
            'acts1' => $acts1,
            'acts2' => $acts2,
            'acts3' => $acts3,
            'acts4' => $acts4,
            'acts5' => $acts5,
            'acts6' => $acts6,
            // 'progress' => $progress,
            // 'consultants' => $consultants
        ];

        $pdf = PDF::loadView('contactreports.pdf', $parameters);

        return $pdf->download($report->name.'.pdf');

        // return view('contactreports.show')->with($parameters);
    }
}
