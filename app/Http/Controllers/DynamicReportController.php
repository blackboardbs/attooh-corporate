<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableDocumentData;
use App\ActionableTextData;
use App\ActionableTextareaData;
use App\ActionableTemplateEmailData;
use App\ActionableDocumentEmailData;
use App\ActionableIntegerData;
use App\ActionablePercentageData;
use App\ActionableAmountData;
use App\Actions;
use App\ActionsAssigned;
use App\Activity;
use App\ActivityInClientBasket;
use App\ActivityLog;
use App\ActivityStepVisibilityRule;
use App\ActivityVisibilityRule;
use App\BillboardMessage;
use App\Client;
use App\ClientActivity;
use App\ClientComment;
use App\ClientCRFForm;
use App\ClientHelper;
use App\ClientProcess;
use App\ClientUser;
use App\ClientVisibleActivity;
use App\ClientVisibleStep;
use App\Committee;
use App\Config;
use App\Document;
use App\EmailTemplate;
use App\Events\NotificationEvent;
use App\FormInputAmountData;
use App\FormInputCheckboxData;
use App\FormInputDropdownData;
use App\FormInputIntegerData;
use App\FormInputPercentageData;
use App\FormInputRadioData;
use App\FormLog;
use App\Forms;
use App\FormSection;
use App\FormSectionInputs;
use App\HelperFunction;
use App\Http\Requests\StoreClientFormRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreFollowRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Log;
use App\Mail\AssignedConsultantNotify;
use App\Mail\NotificationMail;
use App\Notification;
use App\OfficeUser;
use App\Process;
use App\ProcessArea;
use App\Project;
use App\Referrer;
use App\RelatedPartiesTree;
use App\RelatedParty;
use App\Step;
use App\Template;
use App\TokenStore\MicrosoftTokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TriggerType;
use App\UserNotification;
use App\UserTask;
use App\WhatsappTemplate;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\TemplateMail;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use App\ClientForm;
use App\BusinessUnits;
use App\Presentation;
use App\FormInputTextData;
use App\FormInputBooleanData;
use App\FormInputDateData;
use App\FormInputTextareaData;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;
use App\Mail\ClientMail;
use App\FormSectionInputVisibilityRule;
use App\ClientVisibleInput;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\EventType;
use App\Event;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF; 
use App\FormInputDropdownItem;

class DynamicReportController extends Controller
{
    private $helper;
    public function __construct()
    {
        $this->middleware('auth')->except(['sendtemplate', 'sendnotification']);
        $this->middleware('auth:api')->only(['sendtemplate', 'sendnotification']);
        $this->middleware('permission:maintain_client')->except(['create', 'store']);

        $this->progress_colours = [
            'not_started' => 'background-color: rgba(64,159,255, 0.15)',
            'started' => 'background-color: rgba(255,255,70, 0.15)',
            'progress_completed' => 'background-color: rgba(60,255,70, 0.15)',
        ];

        $this->helper = new ClientHelper();
    }

    public function paginate($items, $perPage = 10, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $total = count($items);
        $currentpage = $page;
        $offset = ($currentpage * $perPage) - $perPage ;
        $itemstoshow = array_slice($items , $offset , $perPage);
        return new LengthAwarePaginator($itemstoshow ,$total   ,$perPage);
    }

    public function index(Request $request)
    {
        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        if ($request->has('q') && $request->input('q') != '') {
            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('email', 'like', "%" . $request->input('q') . "%")
                ->orHaving('contact', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        $event_type_filter = null;
        $event_date_from = null;
        $event_date_to = null;

        if ($request->has('event_type') && $request->input('event_type') != '' && $request->input('event_type') != 'all') {
            $event_type_filter = $request->input('event_type');
        }

        if ($request->has('event_date_from') && $request->input('event_date_from') != '') {
            $event_date_from = $request->input('event_date_from');
        }

        if ($request->has('event_date_to') && $request->input('event_date_to') != '') {
            $event_date_to = $request->input('event_date_to');
        }

        if ($request->has('client_filter') && $request->input('client_filter') != '' && $request->input('client_filter') != 'all') {
            $client_filter = $request->input('client_filter'); 
        }

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

            if($event_type_filter != null && $event_date_from == null && $event_date_to == null){
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }else {
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }

            $event_arr = [];
            foreach ($events as $event) {            
                    $event_arr[$event->id][] = [
                        'id' => $event->id,
                        'type' => $event->event_type_id,
                        'date_scheduled' => $event->date_scheduled,
                        'notes' => $event->event_notes,
                    ];
            }

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'events' => $event_arr
                ];
        }

        
        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=no&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=no');
                }
            }
            if ($request->input('step') == 1001) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1002) {
                return redirect('clients?qa=yes');
            }
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($request->has('q') && $request->input('q') != '' && $crm == 4){
            // dd($request->input('q'));
            $c_array = collect($c_array)->filter(function($client) use($request){
                return false !== stristr($client["company"], $request->q);
            })->values();
        }

        if ($request->has('fc_filter')) {
            if($request->input('fc_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $request->input('fc_filter'));
                })->values();
            }
        }

        if ($request->has('client_filter')) {
            if($request->input('client_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $request->input('client_filter'));
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null && $client['events'] != null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');
        // dd($parents);

        $clients_filter = Client::orderBy('created_at', 'ASC')->select('id', 'company', 'first_name', 'last_name')->get();

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'event_types' => EventType::all(),
            'clients_filter' => $clients_filter
        ];

        return view('dynamicreports.index')->with($parameters);
    }

    public function pdf(Request $request){

        $fc_filter = $request->input('fc_filter');
        $client_filter = $request->input('client_filter');
        $event_type_filter = $request->input('event_type');
        $event_date_from = $request->input('event_date_from');
        $event_date_to = $request->input('event_date_to');

        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

            if($event_type_filter != null && $event_date_from == null && $event_date_to == null){
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }else {
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }

            $event_arr = [];
            foreach ($events as $event) {            
                    $event_arr[$event->id][] = [
                        'id' => $event->id,
                        'type' => $event->event_type_id,
                        'date_scheduled' => $event->date_scheduled,
                        'notes' => $event->event_notes,
                    ];
            }

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'events' => $event_arr
                ];
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($fc_filter != null && $fc_filter != '') {
            if($fc_filter == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $fc_filter);
                })->values();
            }
        }

        if ($client_filter) {
            if($client_filter == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $client_filter);
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null && $client['events'] != null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'event_types' => EventType::all()
        ];

        $pdf = PDF::loadView('dynamicreports.pdf', $parameters);

        return $pdf->download('eventreport'.Carbon::now().'.pdf');

        // return view('fcreports.show')->with($parameters);
    }

    public function show(Request $request){

        $fc_filter = $request->input('fc_filter');
        $client_filter = $request->input('client_filter');
        $event_type_filter = $request->input('event_type');
        $event_date_from = $request->input('event_date_from');
        $event_date_to = $request->input('event_date_to');

        dd(NULL);

        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

            if($event_type_filter != null && $event_date_from == null && $event_date_to == null){
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }else {
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }

            $event_arr = [];
            foreach ($events as $event) {            
                    $event_arr[$event->id][] = [
                        'id' => $event->id,
                        'type' => $event->event_type_id,
                        'date_scheduled' => $event->date_scheduled,
                        'notes' => $event->event_notes,
                    ];
            }

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'events' => $event_arr
                ];
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($fc_filter != null && $fc_filter != '') {
            if($fc_filter == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $fc_filter);
                })->values();
            }
        }

        if ($client_filter) {
            if($client_filter == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $client_filter);
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null && $client['events'] != null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'event_types' => EventType::all()
        ];

        $pdf = PDF::loadView('dynamicreports.pdf', $parameters);

        return $pdf->download('eventreport'.Carbon::now().'.pdf');

        // return view('fcreports.show')->with($parameters);
    }

    public function outstanding(Request $request)
    {
        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        if ($request->has('q') && $request->input('q') != '') {
            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('email', 'like', "%" . $request->input('q') . "%")
                ->orHaving('contact', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        $event_type_filter = null;
        $event_date_from = null;
        $event_date_to = null;

        if ($request->has('event_type') && $request->input('event_type') != '' && $request->input('event_type') != 'all') {
            $event_type_filter = $request->input('event_type');
            // dd($event_type_filter);
        }

        if ($request->has('event_date_from') && $request->input('event_date_from') != '') {
            $event_date_from = $request->input('event_date_from');
        }

        if ($request->has('event_date_to') && $request->input('event_date_to') != '') {
            $event_date_to = $request->input('event_date_to');
        }

        if ($request->has('client_filter') && $request->input('client_filter') != '' && $request->input('client_filter') != 'all') {
            $client_filter = $request->input('client_filter'); 
        }

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

            // if($event_type_filter != null && $event_date_from == null && $event_date_to == null){
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', '!=',$event_type_filter)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to == null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from == null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to == null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from == null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }else {
            //     $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            // }

            if($event_date_from == null && $event_date_to == null){
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }else {
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }

            $event_arr = [];
            foreach ($events as $event) {           
                    $event_arr[] = [
                        'id' => $event->id,
                        'type' => $event->event_type_id,
                        'date_scheduled' => $event->date_scheduled,
                        'notes' => $event->event_notes,
                    ];
            }

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'events' => $event_arr
                ];
        }

        
        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=no&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=no');
                }
            }
            if ($request->input('step') == 1001) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1002) {
                return redirect('clients?qa=yes');
            }
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($request->has('q') && $request->input('q') != '' && $crm == 4){
            // dd($request->input('q'));
            $c_array = collect($c_array)->filter(function($client) use($request){
                return false !== stristr($client["company"], $request->q);
            })->values();
        }

        if ($request->has('fc_filter')) {
            if($request->input('fc_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $request->input('fc_filter'));
                })->values();
            }
        }

        if ($request->has('client_filter')) {
            if($request->input('client_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $request->input('client_filter'));
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $clients_filter = Client::orderBy('created_at', 'ASC')->select('id', 'company', 'first_name', 'last_name')->get();

        // dd($parents[3]);

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'event_types' => EventType::all(),
            'clients_filter' => $clients_filter,
            'event_type_filter' => $event_type_filter
        ];

        return view('dynamicreports.outstanding')->with($parameters);
    }

    public function outstanding_pdf(Request $request){

        $fc_filter = $request->input('fc_filter');
        $client_filter = $request->input('client_filter');
        $event_type_filter = $request->input('event_type');
        $event_date_from = $request->input('event_date_from');
        $event_date_to = $request->input('event_date_to');

        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        if ($request->has('q') && $request->input('q') != '') {
            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('email', 'like', "%" . $request->input('q') . "%")
                ->orHaving('contact', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        $event_type_filter = null;
        $event_date_from = null;
        $event_date_to = null;

        if ($request->has('event_type') && $request->input('event_type') != '' && $request->input('event_type') != 'all') {
            $event_type_filter = $request->input('event_type');
            // dd($event_type_filter);
        }

        if ($request->has('event_date_from') && $request->input('event_date_from') != '') {
            $event_date_from = $request->input('event_date_from');
        }

        if ($request->has('event_date_to') && $request->input('event_date_to') != '') {
            $event_date_to = $request->input('event_date_to');
        }

        if ($request->has('client_filter') && $request->input('client_filter') != '' && $request->input('client_filter') != 'all') {
            $client_filter = $request->input('client_filter'); 
        }

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

            // if($event_type_filter != null && $event_date_from == null && $event_date_to == null){
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', '!=',$event_type_filter)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to == null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from == null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter == null && $event_date_from != null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to == null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from == null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }elseif ($event_type_filter != null && $event_date_from != null && $event_date_to != null) {
            //     $events = Event::where('client_id', $client->id)->where('event_type_id', $event_type_filter)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            // }else {
            //     $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            // }

            if($event_date_from == null && $event_date_to == null){
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from != null && $event_date_to == null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from == null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }elseif ($event_date_from != null && $event_date_to != null) {
                $events = Event::where('client_id', $client->id)->where('date_scheduled', '>=', $event_date_from)->where('date_scheduled', '<=', $event_date_to)->orderBy('id', 'desc')->get();
            }else {
                $events = Event::where('client_id', $client->id)->orderBy('id', 'desc')->get();
            }

            $event_arr = [];
            foreach ($events as $event) {           
                    $event_arr[] = [
                        'id' => $event->id,
                        'type' => $event->event_type_id,
                        'date_scheduled' => $event->date_scheduled,
                        'notes' => $event->event_notes,
                    ];
            }

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'events' => $event_arr
                ];
        }

        
        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=no&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=no');
                }
            }
            if ($request->input('step') == 1001) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1002) {
                return redirect('clients?qa=yes');
            }
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($request->has('q') && $request->input('q') != '' && $crm == 4){
            // dd($request->input('q'));
            $c_array = collect($c_array)->filter(function($client) use($request){
                return false !== stristr($client["company"], $request->q);
            })->values();
        }

        if ($request->has('fc_filter')) {
            if($request->input('fc_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $request->input('fc_filter'));
                })->values();
            }
        }

        if ($request->has('client_filter')) {
            if($request->input('client_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $request->input('client_filter'));
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $clients_filter = Client::orderBy('created_at', 'ASC')->select('id', 'company', 'first_name', 'last_name')->get();

        // dd($parents[3]);

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'event_types' => EventType::all(),
            'clients_filter' => $clients_filter,
            'event_type_filter' => $event_type_filter
        ];

        $pdf = PDF::loadView('dynamicreports.outstandingpdf', $parameters);

        return $pdf->download('outstandingreport'.Carbon::now().'.pdf');

        // return view('fcreports.show')->with($parameters);
    }

    public function provider(Request $request){

        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        if ($request->has('q') && $request->input('q') != '') {
            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('email', 'like', "%" . $request->input('q') . "%")
                ->orHaving('contact', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if ($request->has('client_filter') && $request->input('client_filter') != '' && $request->input('client_filter') != 'all') {
            $client_filter = $request->input('client_filter'); 
        }

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

                $risk = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 60)->first();
                $risk_type = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 61)->first();
                $risk_renewal = FormInputDateData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_date_id', 96)->first();
                $fund = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 74)->first();
                $medical_aid = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 78)->first();
                $medical_insurance = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 80)->first();
                $gap_cover = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 82)->first();
                $category = FormInputTextData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_text_id', 595)->first();

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'risk' => (isset($risk->item->name) ? $risk->item->name : ''),
                    'risk_type' => (isset($risk_type->item->name) ? $risk_type->item->name : ''),
                    'risk_renewal' => (isset($risk_renewal->data) ? $risk_renewal->data : ''),
                    'fund' => (isset($fund->item->name) ? $fund->item->name : ''),
                    'medical_aid' => (isset($medical_aid->item->name) ? $medical_aid->item->name : ''),
                    'medical_insurance' => (isset($medical_insurance->item->name) ? $medical_insurance->item->name : ''),
                    'gap_cover' => (isset($gap_cover->item->name) ? $gap_cover->item->name : ''),
                    'category' => (isset($category->data) ? $category->data : ''),
                ];
        }
        // dd($client_arr[738]);
        
        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=no&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=no');
                }
            }
            if ($request->input('step') == 1001) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1002) {
                return redirect('clients?qa=yes');
            }
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($request->has('q') && $request->input('q') != '' && $crm == 4){
            // dd($request->input('q'));
            $c_array = collect($c_array)->filter(function($client) use($request){
                return false !== stristr($client["company"], $request->q);
            })->values();
        }

        if ($request->has('fc_filter')) {
            if($request->input('fc_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $request->input('fc_filter'));
                })->values();
            }
        }

        if ($request->has('client_filter')) {
            if($request->input('client_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $request->input('client_filter'));
                })->values();
            }
        }

        if ($request->has('risk_filter')) {
            if($request->input('risk_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["risk"], $request->input('risk_filter'));
                })->values();
            }
        }

        if ($request->has('risk_type_filter')) {
            if($request->input('risk_type_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== strpos($client["risk_type"], $request->input('risk_type_filter'));
                })->values();
            }
        }

        if ($request->has('fund_filter')) {
            if($request->input('fund_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["fund"], $request->input('fund_filter'));
                })->values();
            }
        }

        if ($request->has('medical_aid_filter')) {
            if($request->input('medical_aid_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["medical_aid"], $request->input('medical_aid_filter'));
                })->values();
            }
        }

        if ($request->has('medical_insurance_filter')) {
            if($request->input('medical_insurance_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["medical_insurance"], $request->input('medical_insurance_filter'));
                })->values();
            }
        }

        if ($request->has('gap_cover_filter')) { 
            if($request->input('gap_cover_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["gap_cover"], $request->input('gap_cover_filter'));
                })->values();
            }
        }

        if ($request->has('risk_renewal')) {
            if($request->input('risk_renewal') == '' || $request->input('risk_renewal') == NULL){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    if($client["risk_renewal"] != ''){
                        return false !== (Carbon::parse($client["risk_renewal"])->format('F Y') == $request->input('risk_renewal'));
                    }
                })->values();
            }
        }

        if ($request->has('category')) { 
            if($request->input('category') == ''){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["category"], $request->input('category'));
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $clients_filter = Client::where('parent_id', NULL)->orderBy('created_at', 'ASC')->select('id', 'company', 'first_name', 'last_name')->get();
        $risk_filter = FormInputDropdownItem::where('form_input_dropdown_id', 60)->select('name')->orderBy('id')->get();
        $risk_type_filter = FormInputDropdownItem::where('form_input_dropdown_id', 61)->select('name')->orderBy('id')->get();
        $fund_filter = FormInputDropdownItem::where('form_input_dropdown_id', 74)->select('name')->orderBy('id')->get();
        $medical_aid_filter = FormInputDropdownItem::where('form_input_dropdown_id', 78)->select('name')->orderBy('id')->get();
        $medical_insurance_filter = FormInputDropdownItem::where('form_input_dropdown_id', 80)->select('name')->orderBy('id')->get();
        $gap_cover_filter = FormInputDropdownItem::where('form_input_dropdown_id', 82)->select('name')->orderBy('id')->get();

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'clients_filter' => $clients_filter,
            'risk_filter' => $risk_filter,
            'risk_type_filter' => $risk_type_filter,
            'fund_filter' => $fund_filter,
            'medical_aid_filter' => $medical_aid_filter,
            'medical_insurance_filter' => $medical_insurance_filter,
            'gap_cover_filter' => $gap_cover_filter,
        ];

        return view('dynamicreports.provider')->with($parameters);
    }

    public function provider_pdf(Request $request){

        $fc_filter = $request->input('fc_filter');
        $client_filter = $request->input('client_filter');
        $risk_filter = $request->input('risk_filter');
        $risk_type_filter = $request->input('risk_type_filter');
        $fund_filter = $request->input('fund_filter');
        $medical_aid_filter = $request->input('medical_aid_filter');
        $medical_insurance_filter = $request->input('medical_insurance_filter');
        $gap_cover_filter = $request->input('gap_cover_filter');
        $risk_renewal = $request->input('risk_renewal');
        $category = $request->input('category');

        $offices = OfficeUser::select('office_id')->where('user_id',Auth::id())->get();

        $np = 0;
        $qa = 0;

        $c_array = array();
        $cb_array = array();
        $client_arr = array();
        $billboard_arr   = array();
        $user_task_arr   = array();
        $client_list = array();
        $client_birthday_arr = array();

        $clients = new Client();
        $clients->unHide();

        $crm = 4;

        $clients = $clients->select('*',
            DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number'),
            DB::raw('CAST(`contact` AS CHAR(50)) AS contact'),
            DB::raw('CAST(`email` AS CHAR(50)) AS email')
        )->orderBy('id', 'asc')->whereHas('processes');

        if ($request->has('q') && $request->input('q') != '') {
            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('email', 'like', "%" . $request->input('q') . "%")
                ->orHaving('contact', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if ($request->has('client_filter') && $request->input('client_filter') != '' && $request->input('client_filter') != 'all') {
            $client_filter = $request->input('client_filter'); 
        }

        $clients = $clients->where('crm_id',$crm);

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $client_name = '';
            $fund_consultant = null;
            $parent_branch = '';

                $risk = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 60)->first();
                $risk_type = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 61)->first();
                $risk_renewal = FormInputDateData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_date_id', 96)->first();
                $fund = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 74)->first();
                $medical_aid = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 78)->first();
                $medical_insurance = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 80)->first();
                $gap_cover = FormInputDropdownData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_dropdown_id', 82)->first();
                $category = FormInputTextData::orderBy('created_at', 'DESC')->where('client_id', $client->id)->where('form_input_text_id', 595)->first();

                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email' => $client->email,
                    'contact' => (substr($client->contact, 0, 1) == '0' ? '+27' . substr($client->contact, 1) : $client->contact),
                    'process_id' => $client->process_id,
                    'step_id' => $client->step_id,
                    'process' => $data,
                    'section' => $client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer) ? $client->introducer->avatar : null,
                    'introducer' => $client->introducer->first_name . ' ' . $client->introducer->last_name,
                    'completed_at' => ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                    'created_at' => ($client->created_at != null ? Carbon::parse($client->created_at)->format('Y-m-d') : ''),
                    'client_name' => $client_name,
                    'fund_consultant' => $fund_consultant,
                    'parent_branch' => $parent_branch,
                    'parent_id' => $client->parent_id,
                    'consultant_name' => $client->consultant_name,
                    'risk' => (isset($risk->item->name) ? $risk->item->name : ''),
                    'risk_type' => (isset($risk_type->item->name) ? $risk_type->item->name : ''),
                    'risk_renewal' => (isset($risk_renewal->data) ? $risk_renewal->data : ''),
                    'fund' => (isset($fund->item->name) ? $fund->item->name : ''),
                    'medical_aid' => (isset($medical_aid->item->name) ? $medical_aid->item->name : ''),
                    'medical_insurance' => (isset($medical_insurance->item->name) ? $medical_insurance->item->name : ''),
                    'gap_cover' => (isset($gap_cover->item->name) ? $gap_cover->item->name : ''),
                    'category' => (isset($category->data) ? $category->data : ''),
                ];
        }
        // dd($client_arr[738]);
        
        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=no&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=no');
                }
            }
            if ($request->input('step') == 1001) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1002) {
                return redirect('clients?qa=yes');
            }
        }

        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        foreach ($client_birthday_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($cb_array, $value2);
            }
        }

        if ($request->has('q') && $request->input('q') != '' && $crm == 4){
            // dd($request->input('q'));
            $c_array = collect($c_array)->filter(function($client) use($request){
                return false !== stristr($client["company"], $request->q);
            })->values();
        }

        if ($request->has('fc_filter')) {
            if($request->input('fc_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["consultant_name"], $request->input('fc_filter'));
                })->values();
            }
        }

        if ($request->has('client_filter')) {
            if($request->input('client_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["id"], $request->input('client_filter'));
                })->values();
            }
        }

        if ($request->has('risk_filter')) {
            if($request->input('risk_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["risk"], $request->input('risk_filter'));
                })->values();
            }
        }

        if ($request->has('risk_type_filter')) {
            if($request->input('risk_type_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== strpos($client["risk_type"], $request->input('risk_type_filter'));
                })->values();
            }
        }

        if ($request->has('fund_filter')) {
            if($request->input('fund_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["fund"], $request->input('fund_filter'));
                })->values();
            }
        }

        if ($request->has('medical_aid_filter')) {
            if($request->input('medical_aid_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["medical_aid"], $request->input('medical_aid_filter'));
                })->values();
            }
        }

        if ($request->has('medical_insurance_filter')) {
            if($request->input('medical_insurance_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["medical_insurance"], $request->input('medical_insurance_filter'));
                })->values();
            }
        }

        if ($request->has('gap_cover_filter')) { 
            if($request->input('gap_cover_filter') == 'all'){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["gap_cover"], $request->input('gap_cover_filter'));
                })->values();
            }
        }

        if ($request->has('risk_renewal')) {
            if($request->input('risk_renewal') == '' || $request->input('risk_renewal') == NULL){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    if($client["risk_renewal"] != ''){
                        return false !== (Carbon::parse($client["risk_renewal"])->format('F Y') == $request->input('risk_renewal'));
                    }
                })->values();
            }
        }

        if ($request->has('category')) { 
            if($request->input('category') == ''){
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return $client;
                })->values();
            }
            else{
                $c_array = collect($c_array)->filter(function ($client) use ($request) {
                    return false !== stristr($client["category"], $request->input('category'));
                })->values();
            }
        }

        $clients_list = Client::whereHas('processes')->whereIn('office_id', collect($offices)->toArray())->where('crm_id','!=',5)->get();

        foreach ($clients_list as $list){
            array_push($client_list,["id"=>$list->id,"client_name"=>(isset($list->company ) && $list->company != null ? $list->company  : $list->first_name.' '.$list->last_name)]);
        }

        $parents = [];
        $branches = [];

        foreach($c_array as $client){
            if($client['parent_id'] == null){
                array_push($parents,$client);
            }
        }
        foreach($c_array as $client){
            if($client['parent_id'] != null){
                array_push($branches,$client);
            }
        }

        $fund_consultants = ['Natasha le Roux' => 'Natasha le Roux','Douw Smit' => 'Douw Smit','George Herman' => 'George Herman','Amanda White' => 'Amanda White','Nadine Lewis' => 'Nadine Lewis','Liesl Nienaber' => 'Liesl Nienaber','Sam van Zyl' => 'Sam van Zyl'];

        // $parents = $this->paginate($parents);
        // $parents->withPath('');

        $clients_filter = Client::where('parent_id', NULL)->orderBy('created_at', 'ASC')->select('id', 'company', 'first_name', 'last_name')->get();
        $risk_filter = FormInputDropdownItem::where('form_input_dropdown_id', 60)->select('name')->orderBy('id')->get();
        $risk_type_filter = FormInputDropdownItem::where('form_input_dropdown_id', 61)->select('name')->orderBy('id')->get();
        $fund_filter = FormInputDropdownItem::where('form_input_dropdown_id', 74)->select('name')->orderBy('id')->get();
        $medical_aid_filter = FormInputDropdownItem::where('form_input_dropdown_id', 78)->select('name')->orderBy('id')->get();
        $medical_insurance_filter = FormInputDropdownItem::where('form_input_dropdown_id', 80)->select('name')->orderBy('id')->get();
        $gap_cover_filter = FormInputDropdownItem::where('form_input_dropdown_id', 82)->select('name')->orderBy('id')->get();

        $parameters = [
            'crm' => $crm,
            'np' => $np,
            'qa' => $qa,
            'client_list' => $client_list,
            'messages' => $billboard_arr,
            'user_tasks' => $user_task_arr,
            'clients_birthdays' => (isset($client_birthday_arr) ? (empty($cb_array) ? $client_birthday_arr : $cb_array) : []),
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'parents' => $parents,
            'branches' => $branches,
            'message_users' => User::where('id', '!=', Auth::id())->get(),
            'whatsapp_templates' => WhatsappTemplate::pluck('name','id')->prepend('Select',''),
            'fund_consultants' => $fund_consultants,
            'email_clients' => Client::all(),
            'clients_filter' => $clients_filter,
            'risk_filter' => $risk_filter,
            'risk_type_filter' => $risk_type_filter,
            'fund_filter' => $fund_filter,
            'medical_aid_filter' => $medical_aid_filter,
            'medical_insurance_filter' => $medical_insurance_filter,
            'gap_cover_filter' => $gap_cover_filter,
        ];

        $pdf = PDF::loadView('dynamicreports.providerpdf', $parameters);

        return $pdf->download('providerreport'.Carbon::now().'.pdf');

        // return view('fcreports.show')->with($parameters);
    }
}
