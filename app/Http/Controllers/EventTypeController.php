<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventType;

class EventTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        $event_types = EventType::all();

        return view('master_data.event_types.index')->with('event_types', $event_types);
    }

    function create()
    {

        return view('master_data.event_types.create');
    }

    public function store(Request $request)
    {
        $event_type = new EventType();
        $event_type->name = $request->input('name');
        $event_type->save();

        return redirect(route('event_types.index'))->with('flash_success','Event Type captured successfully');
    }

    public function edit(EventType $event_type)
    {
        return view('master_data.event_types.edit')->with(['event_type'=>$event_type]);
    }

    public function update(Request $request, EventType $event_type)
    {
        $event_type->name = $request->input('name');
        $event_type->save();

        return redirect(route('event_types.index'))->with('flash_success','Event Type updated successfully');
    }

    public function show(EventType $event_type)
    {
        $res = EventType::find($event_type->id);
        $res->delete();

        return redirect(route('event_types.index'))->with('flash_success','Event Type deleted successfully');
    }

    public function destroy(EventType $event_type)
    {
        $res = EventType::find($event_type->id);
        $res->delete();

        return redirect(route('event_types.index'))->with('flash_success','Event Type deleted successfully');
    }
}
