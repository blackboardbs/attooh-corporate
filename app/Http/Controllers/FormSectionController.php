<?php

namespace App\Http\Controllers;

use App\FormInputDropdown;
use App\FormInputDropdownItem;
use App\FormInputAmount;
use App\FormInputPercentage;
use App\FormInputInteger;
use App\FormInputVideo;
use App\FormInputText;
use App\FormInputDate;
use App\FormInputBoolean;
use App\FormInputHeading;
use App\FormInputSubheading;
use App\FormInputClient;
use App\FormInputId;
use App\FormInputPassport;
use App\FormInputTextarea;
use App\FormInputDocument;
use App\Forms;
use App\FormSection;
use App\FormSectionInputs;
use App\FormSectionInputMirrorValue;
use App\Process;
use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Schema;
use App\FormSectionInputSectionVisibilityRule;
use App\FormSectionInputVisibilityRule;


class FormSectionController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
        //return $this->middleware('auth')->except('index');
    }

    public function create(Forms $form){
        $all_columns = Schema::getColumnListing('clients');
        asort($all_columns);
        $exclude_columns = [
            '',
            'id',
            'referrer_id',
            'introducer_id',
            'office_id',
            'process_id',
            'step_id',
            'is_progressing',
            'not_progressing_date',
            'needs_approval',
            'cif_code',
            'business_unit_id',
            'case_number',
            'is_qa',
            'qa_start_date',
            'qa_end_date',
            'qa_consultant',
            'hash_first_name',
            'hash_last_name',
            'hash_company',
            'hash_email',
            'hash_contact',
            'hash_id_number',
            'hash_cif_code',
            'hash_company_registration_number',
            'consultant_id',
            'committee_id',
            'project_id',
            'trigger_type_id',
            'instruction_date',
            'assigned_date',
            'viewed',
            'completed',
            'completed_date',
            'completed_by',
            'out_of_scope',
            'work_item_qa',
            'work_item_qa_date',
            'crm_id',
            'parent_id',
            'deleted_at'
        ];
        $get_columns = array_diff($all_columns, $exclude_columns);

        return view('forms.sections.create')->with(['forms' => $form->load('sections'),'fields' => json_encode($get_columns)]);
    }

    public function store(Request $request,Forms $form){
        //dd($request);
        $section = new FormSection();
        $section->name = $request->input('name');
        $section->show_name_in_tabs = $request->input('show_name_in_tab');
        $section->group = $request->input('group_section');
        $section->form_id = $form->id;
        $section->order = FormSection::where('form_id', $form->id)->max('order') + 1;
        $section->save();

        //loop over each activity input
        foreach ($request->input('inputs') as $input_key => $input_input) {
            $input = new FormSectionInputs();
            $form_input = $this->createInput($input_input['type']);

            $input->name = $input_input['name'];
            $input->order = $input_key + 1;
            $input->input_id = $form_input->id;
            $input->input_type = $this->getInputType($input_input['type']);
            $input->form_section_id = $section->id;
            $input->kpi = (isset($input_input['kpi']) && $input_input['kpi'] == "on") ? 1 : null;
            $input->email = (isset($input_input['email']) && $input_input['email'] == "on") ? 1 : 0;
            $input->multiple_selection = (isset($activity_input['multiple_selection']) && $activity_input['multiple_selection'] == "on") ? 1 : 0;
            $input->future_date = (isset($activity_input['future_date']) && $activity_input['future_date'] == "on") ? 1 : 0;
            if ($input_input['type'] == 'heading' || $input_input['type'] == 'subheading') {
                $input->client_bucket = 1;
            } else {
                $input->client_bucket = (isset($input_input['client_bucket']) && $input_input['client_bucket'] == "on") ? 1 : 0;
            }
            $input->level = (isset($input_input['level']) ? $input_input['level'] : 0);
            $input->color = (isset($input_input['color']) && $input_input['color'] != '#hsla(0,0%,0%,0)' ? $input_input['color'] : null);
            $input->grouped = (isset($input_input['grouping']) && $input_input['grouping'] == "on") ? 1 : 0;
            $input->grouping = (isset($input_input['grouping_value'])) ? $input_input['grouping_value'] : 0;
            $input->save();

            //if activity is a dropdown type
            if ($input_input['type'] == 'dropdown') {

                //only add dropdown items if there is input
                if (isset($input_input['dropdown_items'])) {
                    //dd($input_input['dropdown_items']);
                    //loop over each dropdown item
                    foreach ($input_input['dropdown_items'] as $dropdown_item) {
                        $actionable_dropdown_item = new FormInputDropdownItem;
                        $actionable_dropdown_item->form_input_dropdown_id = $form_input->id;
                        $actionable_dropdown_item->name = $dropdown_item;
                        $actionable_dropdown_item->save();
                    }
                }
            }

            //if activity is a dropdown type
            if ($input_input['type'] == 'radio') {

                //only add dropdown items if there is input
                if (isset($input_input['radio_items'])) {
                    //dd($input_input['dropdown_items']);
                    //loop over each dropdown item
                    foreach ($input_input['radio_items'] as $radio_item) {
                        $actionable_radio_item = new FormInputRadioItem;
                        $actionable_radio_item->form_input_radio_id = $form_input->id;
                        $actionable_radio_item->name = $radio_item;
                        $actionable_radio_item->save();
                    }
                }
            }

            //if activity is a dropdown type
            if ($input_input['type'] == 'checkbox') {

                //only add dropdown items if there is input
                if (isset($input_input['checkbox_items'])) {
                    //dd($input_input['dropdown_items']);
                    //loop over each dropdown item
                    foreach ($input_input['checkbox_items'] as $checkbox_item) {
                        $actionable_checkbox_item = new FormInputCheckboxItem;
                        $actionable_checkbox_item->form_input_checkbox_id = $form_input->id;
                        $actionable_checkbox_item->name = $checkbox_item;
                        $actionable_checkbox_item->save();
                    }
                }
            }
        }

        return redirect(route('forms.show', $form->id))->with('flash_success', 'Form Section successfully saved.');
    }

    public function edit($formid){

        $form = FormSection::find($formid);

        $section_inputs_array = [];
        foreach ($form->form_section_inputs as $inputs) {

            $section_input_array = [
                'id' => $inputs->id,
                'step_id' => $inputs->form_section_id,
                'name' => $inputs->name,
                'tooltip' => $inputs->tooltip,
                'type' => $inputs->getFormTypeName(),
                'kpi' => ($inputs->kpi ==1 ? true : false),
                'level' => $inputs->level,
                'color' => $inputs->color,
                'email' => ($inputs->email == "1" ? true : false),
                'mapped_field' => $inputs->mapped_field,
                'client_bucket' => ($inputs->client_bucket == "1" ? true : false),
                'dropdown_item' => '',
                'dropdown_items' => [],
                'is_grouping_items_shown' => false,
                'grouping_value' => ($inputs->grouping != null ? $inputs->grouping : 0),
                'is_dropdown_items_shown' => false,
                'radio_item' => '',
                'radio_items' => [],
                'is_tooltip_shown' => false,
                'is_radio_items_shown' => false,
                'checkbox_item' => '',
                'checkbox_items' => [],
                'is_checkbox_items_shown' => false,
                'show_mirror' => false,
                'show_mirror_crm' => false,
                'show_mirror_activity' => false,
                'show_mirror_default' => false,
                'show_rules' => false,
                'show_step_rules' => false,
                'show_activity_rules' => false,
                'multiple_selection'=>($inputs->multiple_selection == "1" ? true : false),
                'future_date'=>($inputs->future_date == "1" ? true : false)
            ];

            $arule_array = [];
            $arules = FormSectionInputVisibilityRule::where('input_id',$inputs->id)->get();

            foreach ($arules as $arule){

                $activity2 = FormSectionInputs::find($arule->preceding_input);

                $act = FormSectionInputs::where('form_section_id',$inputs->form_section_id)->where('id','!=',$inputs->id)->where('order','<',$inputs->order)->orderBy('order')->pluck('name','id');

                if($activity2->getFormTypeName() == 'dropdown'){
                    $arule_array[] = ['activities'=>(count($act) > 0  ? $act : ''),'dropdown'=>true,'boolean'=>false,'text'=>false,'dropdownitems'=>(isset($activity2->input->items) ? $activity2->input->items->pluck('name')->toArray() : null),'activity_id'=>$arule->preceding_input,'activity_value'=>$arule->input_value];
                } elseif($activity2->getFormTypeName() == 'boolean' || $activity2->getFormTypeName() == 'notification' || $activity2->getFormTypeName() == 'multiple_attachment' || $activity2->getFormTypeName() == 'document' || $activity2->getFormTypeName() == 'document'){
                    $arule_array[] = ['activities'=>(count($act) > 0  ? $act : ''),'dropdown'=>false,'boolean'=>true,'text'=>false,'dropdownitems'=>null,'activity_id'=>$arule->preceding_input,'activity_value'=>$arule->input_value];
                } else {
                    $arule_array[] = ['activities'=>(count($act) > 0  ? $act : ''),'dropdown'=>false,'boolean'=>false,'text'=>true,'dropdownitems'=>null,'activity_id'=>$arule->preceding_input,'activity_value'=>$arule->input_value];
                }
            }

            //$section_input_array['arules'] = [];
            $section_input_array['arules'] = (isset($arule_array) && count($arule_array) > 0 ? $arule_array : [['activities'=>FormSectionInputs::where('form_section_id',$inputs->form_section_id)->where('id','!=',$inputs->id)->where('order','<',$inputs->order)->orderBy('order')->pluck('name','id'),'dropdown'=>false,'boolean'=>false,'text'=>true,'dropdownitems'=>'','activity_id'=>'','activity_value'=>'']]);

            $srule_array = [];
            /*$srules = ActivityStepVisibilityRule::where('activity_id',$activity->id)->get();

            foreach ($srules as $srule){

                $steps = Step::where('process_id',$activity->step->process->id)->where('id','!=',$activity->step_id)->orderBy('order')->pluck('name','id');

                if($activity->getTypeName() == 'dropdown'){
                    $srule_array[] = ['steps'=>(count($steps) > 0  ? $steps : ''),'dropdown'=>true,'boolean'=>false,'text'=>false,'dropdownitems'=>(isset($activity->actionable->items) ? $activity->actionable->items->pluck('name')->toArray() : null),'step_id'=>$srule->activity_step,'activity_value'=>$srule->activity_value];
                } elseif($activity->getTypeName() == 'boolean' || $activity->getTypeName() == 'notification' || $activity->getTypeName() == 'multiple_attachment' || $activity->getTypeName() == 'document' || $activity->getTypeName() == 'document'){
                    $srule_array[] = ['steps'=>(count($steps) > 0  ? $steps : ''),'dropdown'=>false,'boolean'=>true,'text'=>false,'dropdownitems'=>null,'step_id'=>$srule->activity_step,'activity_value'=>$srule->activity_value];
                } else {
                    $srule_array[] = ['steps'=>(count($steps) > 0  ? $steps : ''),'dropdown'=>false,'boolean'=>false,'text'=>true,'dropdownitems'=>null,'step_id'=>$srule->activity_step,'activity_value'=>$srule->activity_value];
                }
            }*/

            $section_input_array['srules'] = [];

            if ($inputs->getFormTypeName() == 'dropdown') {

                $section_input_array['dropdown_items'] = FormInputDropdownItem::where('form_input_dropdown_id',$inputs->input_id)->pluck('name')->toArray();
            }

            if ($inputs->getFormTypeName() == 'radio') {
                $section_input_array['radio_items'] = FormInputRadioItem::where('form_input_radio_id',$inputs->input_id)->pluck('name')->toArray();
                //dd($inputs->id);
            }

            if ($inputs->getFormTypeName() == 'checkbox') {
                $section_input_array['checkbox_items'] = FormInputCheckboxItem::where('form_input_checkbox_id',$inputs->input_id)->pluck('name')->toArray();
            }

            $amirror_array = [];
            $cmirror_array = [];
            $dmirror_array = [];
            $mirrors = FormSectionInputMirrorValue::where('activity_id',$inputs->id)->get();

            foreach ($mirrors as $mirror){
                if($mirror->mirror_type == 'activity'){
                    $amirror_array[] = [
                        'mirror_atype'=>$this->getInputType($inputs->getFormTypeName()),
                        'mirror_process'=>$mirror->mirror_process_id,
                        'mirror_step'=>$mirror->mirror_step_id,
                        'mirror_activity'=>$mirror->mirror_activity_id,
                        'processs'=>Process::orderBy('name')->pluck('name','id'),
                        'stepss'=>Step::where('process_id',$mirror->mirror_process_id)->orderBy('order')->pluck('name','id'),
                        'activitiess'=>Activity::where('step_id',$mirror->mirror_step_id)->orderBy('order')->pluck('name','id')
                    ];
                } elseif($mirror->mirror_type == 'crm'){
                    $cmirror_array[] = [
                        'mirror_atype'=>$this->getInputType($inputs->getFormTypeName()),
                        'mirror_process'=>$mirror->mirror_process_id,
                        'mirror_step'=>$mirror->mirror_step_id,
                        'mirror_activity'=>$mirror->mirror_activity_id,
                        'processs'=>Forms::orderBy('name')->pluck('name','id'),
                        'stepss'=>FormSection::where('form_id',$mirror->mirror_process_id)->orderBy('order')->pluck('name','id'),
                        'activitiess'=>FormSectionInputs::where('form_section_id',$mirror->mirror_step_id)->orderBy('order')->pluck('name','id')
                    ];
                } else {
                    $dmirror_array[] = [
                        'mirror_atype'=>$this->getInputType($inputs->getFormTypeName()),
                        'mirror_process'=>$mirror->mirror_process_id,
                        'mirror_step'=>$mirror->mirror_step_id,
                        'mirror_activity'=>$mirror->mirror_activity_id,
                        'mirror_value'=>$mirror->mirror_column,
                        'processs'=>['full_name'=>'Full Name','first_name'=>'First Name','last_name'=>'Surname','initials'=>'Initials','id_number'=>'ID/Password Number','email'=>'Email','contact'=>'Cellphone Number'],
                        'stepss'=>FormSection::where('form_id',$mirror->mirror_process_id)->orderBy('order')->pluck('name','id'),
                        'activitiess'=>FormSectionInputs::where('form_section_id',$mirror->mirror_step_id)->orderBy('order')->pluck('name','id')
                    ];
                }
            }

            $section_input_array['amirrors'] = (isset($amirror_array) && count($amirror_array) > 0 ? $amirror_array : [['processs'=>Process::orderBy('name')->pluck('name','id'),'stepss'=>'','activitiess'=>'']]);
            $section_input_array['cmirrors'] = (isset($cmirror_array) && count($cmirror_array) > 0 ? $cmirror_array : [['processs'=>Forms::orderBy('name')->pluck('name','id'),'stepss'=>'','activitiess'=>'']]);
            $section_input_array['dmirrors'] = (isset($dmirror_array) && count($dmirror_array) > 0 ? $dmirror_array : [['processs'=>['full_name'=>'Full Name','first_name'=>'First Name','last_name'=>'Surname','initials'=>'Initials','id_number'=>'ID/Password Number','email'=>'Email','contact'=>'Cellphone Number'],'stepss'=>'','activitiess'=>'']]);

            array_push($section_inputs_array, $section_input_array);
        }
        // dd($section_inputs_array);


        $all_columns = Schema::getColumnListing('clients');
        asort($all_columns);
        $exclude_columns = [
            '',
            'id',
            'referrer_id',
            'introducer_id',
            'office_id',
            'process_id',
            'step_id',
            'is_progressing',
            'not_progressing_date',
            'needs_approval',
            'cif_code',
            'business_unit_id',
            'case_number',
            'is_qa',
            'qa_start_date',
            'qa_end_date',
            'qa_consultant',
            'hash_first_name',
            'hash_last_name',
            'hash_company',
            'hash_email',
            'hash_contact',
            'hash_id_number',
            'hash_cif_code',
            'hash_company_registration_number',
            'consultant_id',
            'committee_id',
            'project_id',
            'trigger_type_id',
            'instruction_date',
            'assigned_date',
            'viewed',
            'completed',
            'completed_date',
            'completed_by',
            'out_of_scope',
            'work_item_qa',
            'work_item_qa_date',
            'crm_id',
            'parent_id',
            'deleted_at'
        ];
        $get_columns = array_diff($all_columns, $exclude_columns);

         //dd($section_inputs_array);

        $paramaters = [
            'form' => $form,
            'inputs' => json_encode($section_inputs_array),
            'fields' => json_encode($get_columns),
            'roles' => Role::orderBy('name')->get()
        ];


        return view('forms.sections.edit')->with($paramaters);
    }

    public function update(FormSection $form_section,Request $request){
        //dd($request->input());
        /*$form_section = FormSection::find($form_sections->id);*/
        $existing_section = FormSection::where('name',$form_section->name)->first();

        if($existing_section != null){
            $section_id = $existing_section->id;

            $form_section->name = $request->input('name');
            $form_section->show_name_in_tabs = $request->input('show_name_in_tab');
            $form_section->group = ($request->input('group_section') != null ? $request->input('group_section') : '0');

            $form_section->save();
        }
        //dd($request->input('activities'));
        $pinputs = array();
        if($request->input("inputs") != null) {
            foreach ($request->input("inputs") as $input) {
                //dd($activities);
                array_push($pinputs, $input["id"]);
            }
        }
        FormSectionInputs::where('form_section_id',$form_section->id)->whereNotIn('id',$pinputs)->delete();


        // dd($request->input('inputs'));
        //loop over each activity input
        if($request->input("inputs") != null) {
            foreach ($request->input('inputs') as $activity_key => $activity_input) {

                $activity = $form_section->form_section_inputs()->where('id', $activity_input['id'])->get()->first();
                $activity_type = $form_section->form_section_inputs()->where('id', $activity_input['id'])->where('input_type', $this->getInputType($activity_input['type']))->get()->first();

                //if there is a previous activity matching the name and type, reactivate it else create a new one
                if (!$activity) {
                    $new_activity = true;
                    if (!$activity_type) {
                        $new_activity_type = true;
                        $activity = new FormSectionInputs;
                        $actionable = $this->createInput($activity_input['type']);
                    } else {
                        $new_activity_type = false;
                        $activity->restore();
                        $actionable = $activity->input;
                    }

                } else {
                    $new_activity = false;
                    if (!$activity_type) {
                        $new_activity_type = true;
                        $activity = FormSectionInputs::find($activity_input['id']);
                        $actionable = $this->createInput($activity_input['type']);
                    } else {
                        $new_activity_type = false;
                        $activity->restore();
                        $actionable = $activity->input;
                    }

                }

                $activity->name = $activity_input['name'];
                $activity->order = $activity_key + 1;
                $activity->input_id = (isset($actionable->id) ? $actionable->id : $actionable);
                $activity->input_type = $this->getInputType($activity_input['type']);
                $activity->form_section_id = $form_section->id;
                $activity->mapped_field = (isset($activity_input['mapped_field']) ? $activity_input['mapped_field'] : null);
                $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : 0;
                $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                $activity->email = (isset($activity_input['email']) && $activity_input['email'] == "on") ? 1 : 0;
                $activity->multiple_selection = (isset($activity_input['multiple_selection']) && $activity_input['multiple_selection'] == "on") ? 1 : 0;
                $activity->future_date = (isset($activity_input['future_date']) && $activity_input['future_date'] == "on") ? 1 : 0;
                if ($activity_input['type'] == 'heading' || $activity_input['type'] == 'subheading') {
                    $activity->client_bucket = 1;
                } else {
                    $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                }
                $activity->level = (isset($activity_input['level']) ? $activity_input['level'] : 0);
                $activity->color = (isset($activity_input['color']) && $activity_input['color'] != '#hsla(0,0%,0%,0)' ? $activity_input['color'] : null);
                $activity->save();


                //if activity is a dropdown type
                if ($activity_input['type'] == 'dropdown') {

                    //delete all previous dropdown items
                    FormInputDropdownItem::where('form_input_dropdown_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                    //only add dropdown items if there is input
                    if (isset($activity_input['dropdown_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['dropdown_items'] as $dropdown_item) {

                            //if this is a reactivated activity, search for all old dropdowns
                            if (!$new_activity_type) {

                                //find if there already a dropdown item for that activity
                                $item = $actionable->items()->withTrashed()->where('name', $dropdown_item)->get()->first();

                                //if there is a previous dropdown item, reactivate it else create a new one
                                if (!$item) {
                                    $actionable_dropdown_item = new FormInputDropdownItem;
                                    $actionable_dropdown_item->form_input_dropdown_id = $actionable->id;
                                    $actionable_dropdown_item->name = $dropdown_item;
                                    $actionable_dropdown_item->save();
                                } else {
                                    $item->restore();
                                }

                            } // otherwise create a new dropdown item without searching
                            else {
                                $actionable_dropdown_item = new FormInputDropdownItem;
                                $actionable_dropdown_item->form_input_dropdown_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                $actionable_dropdown_item->name = $dropdown_item;
                                $actionable_dropdown_item->save();
                            }
                        }
                    }
                }

                //if activity is a radio type
                if ($activity_input['type'] == 'radio') {   

                    //delete all previous dropdown items
                    FormInputRadioItem::where('form_input_radio_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                    //only add dropdown items if there is input
                    if (isset($activity_input['radio_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['radio_items'] as $radio_item) {

                            //if this is a reactivated activity, search for all old dropdowns
                            if (!$new_activity_type) {

                                //find if there already a dropdown item for that activity
                                $item = $actionable->items()->withTrashed()->where('name', $radio_item)->get()->first();

                                //if there is a previous dropdown item, reactivate it else create a new one
                                if (!$item) {
                                    $actionable_radio_item = new FormInputRadioItem;
                                    $actionable_radio_item->form_input_radio_id = $actionable->id;
                                    $actionable_radio_item->name = $radio_item;
                                    $actionable_radio_item->save();
                                } else {
                                    $item->restore();
                                }

                            } // otherwise create a new dropdown item without searching
                            else {
                                $actionable_radio_item = new FormInputRadioItem;
                                $actionable_radio_item->form_input_radio_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                $actionable_radio_item->name = $radio_item;
                                $actionable_radio_item->save();
                            }
                        }
                    }
                }


                //if activity is a checkbox type
                if ($activity_input['type'] == 'checkbox') {

                    //delete all previous dropdown items
                    FormInputCheckboxItem::where('form_input_checkbox_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                    //only add dropdown items if there is input
                    if (isset($activity_input['checkbox_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['checkbox_items'] as $checkbox_item) {

                            //if this is a reactivated activity, search for all old dropdowns
                            if (!$new_activity_type) {

                                //find if there already a dropdown item for that activity
                                $item = $actionable->items()->withTrashed()->where('name', $checkbox_item)->get()->first();

                                //if there is a previous dropdown item, reactivate it else create a new one
                                if (!$item) {
                                    $actionable_checkbox_item = new FormInputCheckboxItem;
                                    $actionable_checkbox_item->form_input_checkbox_id = $actionable->id;
                                    $actionable_checkbox_item->name = $checkbox_item;
                                    $actionable_checkbox_item->save();
                                } else {
                                    $item->restore();
                                }

                            } // otherwise create a new dropdown item without searching
                            else {
                                $actionable_checkbox_item = new FormInputCheckboxItem;
                                $actionable_checkbox_item->form_input_checkbox_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                $actionable_checkbox_item->name = $checkbox_item;
                                $actionable_checkbox_item->save();
                            }
                        }
                    }
                }

                FormSectionInputVisibilityRule::where('input_id',$activity->id)->delete();

                if(isset($activity_input['arules'])) {
                    foreach ($activity_input['arules'] as $arule) {
                        if(isset($arule['activity_id']) && isset($arule['activity_value'])) {
                            $arules = new FormSectionInputVisibilityRule;
                            $arules->input_id = $activity->id;
                            $arules->preceding_input = $arule['activity_id'];
                            $arules->input_value = $arule['activity_value'];
                            $arules->input_section = $activity->form_section_id;
                            $arules->save();
                        }
                    }
                }

                FormSectionInputSectionVisibilityRule::where('input_id',$activity->id)->delete();

                if(isset($activity_input['srules'])) {
                    foreach ($activity_input['srules'] as $srule) {
                        if(isset($srule['rule_step']) && isset($srule['rule_value'])) {
                            $arules = new FormSectionInputSectionVisibilityRule;
                            $arules->input_id = $activity->id;
                            $arules->input_value = $srule['rule_value'];
                            $arules->input_section = $srule['rule_step'];
                            $arules->save();
                        }
                    }
                }

                FormSectionInputMirrorValue::where('activity_id',$activity->id)->delete();

                    if(isset($activity_input['amirrors'])) {
                        foreach ($activity_input['amirrors'] as $mirrorvalue) {
                            if (isset($mirrorvalue['mirror_process']) && isset($mirrorvalue['mirror_step']) && isset($mirrorvalue['mirror_activity'])) {
                                $mirror = new FormSectionInputMirrorValue;
                                $mirror->activity_id = $activity->id;
                                $mirror->mirror_type = 'activity';
                                $mirror->mirror_process_id = $mirrorvalue["mirror_process"];
                                $mirror->mirror_step_id = $mirrorvalue["mirror_step"];
                                $mirror->mirror_activity_id = $mirrorvalue["mirror_activity"];
                                $mirror->save();
                            }
                        }
                    }

                    if(isset($activity_input['cmirrors'])) {
                        foreach ($activity_input['cmirrors'] as $mirrorvalue) {
                            if (isset($mirrorvalue['mirror_process']) && isset($mirrorvalue['mirror_step']) && isset($mirrorvalue['mirror_activity'])) {
                                $mirror = new FormSectionInputMirrorValue;
                                $mirror->activity_id = $activity->id;
                                $mirror->mirror_type = 'crm';
                                $mirror->mirror_process_id = $mirrorvalue["mirror_process"];
                                $mirror->mirror_step_id = $mirrorvalue["mirror_step"];
                                $mirror->mirror_activity_id = $mirrorvalue["mirror_activity"];
                                $mirror->save();
                            }
                        }
                    }

                    if(isset($activity_input['dmirrors'])) {
                        foreach ($activity_input['dmirrors'] as $mirrorvalue) {
                            if (isset($mirrorvalue['mirror_value'])) {
                                $mirror = new FormSectionInputMirrorValue;
                                $mirror->activity_id = $activity->id;
                                $mirror->mirror_type = 'default';
                                $mirror->mirror_column = $mirrorvalue["mirror_value"];
                                $mirror->save();
                            }
                        }
                    }
            }
        }
        return redirect(route('forms.show', $form_section->form_id))->with('flash_success', 'Form updated successfully.');
    }

    public function destroy(FormSection $form){
        $form_id = $form->form_id;

        $form->delete();

        return redirect(route('forms.show', $form_id))->with('flash_success', 'Form Section successfully deleted.');
    }

    public function move(FormSection $form, Request $request)
    {

        if ($request->input('direction') == 'up') {
            $next_step = FormSection::where('form_id', $form->form->id)->where('order', '<', $form->order)->orderBy('order', 'desc')->first();

            if ($next_step) {
                $old_order = $form->order;
                $new_order = $next_step ->order;
                $form->order = $new_order;
                $next_step->order = $old_order;
                $next_step->save();
                $form->save();
            }
        }

        if ($request->input('direction') == 'down') {
            $next_step = FormSection::where('form_id', $form->form->id)->where('order', '<', $form->order)->orderBy('order', 'desc')->first();

            if ($next_step) {
                $old_order = $form->order;
                $new_order = $next_step ->order;
                $form->order = $new_order;
                $next_step->order = $old_order;
                $next_step->save();
                $form->save();
            }
        }

        return redirect(route('forms.show', $form->form))->with('flash_success', 'Form updated successfully.');
    }

    public function getInputType($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return 'App\FormInputText';
                break;
            case 'heading':
                return 'App\FormInputHeading';
                break;
            case 'subheading':
                return 'App\FormInputSubheading';
                break;
            case 'amount':
                return 'App\FormInputAmount';
                break;
            case 'percentage':
                return 'App\FormInputPercentage';
                break;
            case 'integer':
                return 'App\FormInputInteger';
                break;
            case 'video':
                return 'App\FormInputVideo';
                break;
            case 'textarea':
                return 'App\FormInputTextarea';
                break;
            case 'dropdown':
                return 'App\FormInputDropdown';
                break;
            case 'radio':
                return 'App\FormInputRadio';
                break;
            case 'checkbox':
                return 'App\FormInputCheckbox';
                break;
            case 'date':
                return 'App\FormInputDate';
                break;
            case 'boolean':
                return 'App\FormInputBoolean';
                break;
            case 'client':
                return 'App\FormInputClient';
                break;
            case 'id':
                return 'App\FormInputId';
                break;
            case 'passport':
                return 'App\FormInputPassport';
                break;
            case 'document':
                return 'App\FormInputDocument';
                break;
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function createInput($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return FormInputText::create();
                break;
            case 'heading':
                return FormInputHeading::create();
                break;
            case 'subheading':
                return FormInputSubheading::create();
                break;
            case 'amount':
                return FormInputAmount::create();
                break;
            case 'percentage':
                return FormInputPercentage::create();
                break;
            case 'integer':
                return FormInputInteger::create();
                break;
            case 'video':
                return FormInputVideo::create();
                break;
            case 'textarea':
                return FormInputTextarea::create();
                break;
            case 'dropdown':
                return FormInputDropdown::create();
                break;
            case 'date':
                return FormInputDate::create();
                break;
            case 'boolean':
                return FormInputBoolean::create();
                break;
            case 'client':
                return FormInputClient::create();
                break;
            case 'id':
                return FormInputId::create();
                break;
            case 'passport':
                return FormInputPassport::create();
                break;
            case 'document':
                return FormInputDocument::create();
                break;
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getSections(Request $request){

        $sections = FormSection::where('form_id',$request->input('form_id'))->orderBy('order')->get();

        foreach ($sections as $p){
            $section[$p->id] = $p->name;
        }
        return $section;
    }

    public function getRemainingSections(Request $request){

        $process = FormSection::where('id',$request->input('step_id'))->first();

        $steps = FormSection::where('id','!=',$request->input('step_id'))->where('form_id',$process->form_id)->orderBy('order')->get();

        $step = array();

        foreach ($steps as $p){
            $step[$p->id] = $p->name;
        }
        return $step;
    }

    public function copy(Request $request, $duplication_group, $client_id)
    {
        $form_inputs = FormSectionInputs::where('duplication_group', $duplication_group)->get();
        // dd($form_inputs);
        // $sections = FormSection::with('form_section_inputs')->where('form_id',4)->orderBy('order')->get();
        $sections = FormSection::with(['tabs','form_section_inputs.input.data'])->where('form_id',4)->orderBy('order')->whereHas('tabs')->get();

        foreach($sections as $section){
            // dd($section->form_section_inputs[2]);
            foreach($section->form_section_inputs as $form_input){
                // dd($form_input);
                if($form_input->duplication_group == $duplication_group){
                // dd($form_input);
                
                $input_name = $this->getFormTypeDisplayName($form_input->input_type);
                // dd($input_name);
                $input_new = $this->createInput($input_name);
                // dd($input_new);
                $form_input_new = new FormSectionInputs();
                $form_input_new->name = $form_input->name;
                $form_input_new->order = $form_input->order;
                $form_input_new->input_id = $input_new->id;
                $form_input_new->input_type = $form_input->input_type;
                $form_input_new->form_section_id = $form_input->form_section_id;
                $form_input_new->mapped_field = $form_input->mapped_field;
                $form_input_new->grouped = $form_input->grouped;
                $form_input_new->grouping = $form_input->grouping;
                $form_input_new->kpi = $form_input->kpi;
                $form_input_new->email = $form_input->email;
                $form_input_new->duplication_group = $form_input->duplication_group;
                $form_input_new->duplication_group = ++$form_input_new->duplication_group;
                $form_input_new->client_bucket = $form_input->client_bucket;
                $form_input_new->level = $form_input->level;
                $form_input_new->color = $form_input->color;
                $form_input_new->client_id = $client_id;
                $form_input_new->save();
                // dd($form_input_new);
    
                $form_input->visible = 1;
                $form_input->save();
    
                if ($input_name == 'dropdown') {
                    // dd($form_input_new['input_id']);

                    $dropdown_items = FormInputDropdownItem::where('form_input_dropdown_id', $form_input['input_id'])->get();
                    // dd($dropdown_items);

                    foreach($dropdown_items as $dropdown_item){
                        // dd($dropdown_item->name);
                        $actionable_dropdown_item = new FormInputDropdownItem;
                        $actionable_dropdown_item->form_input_dropdown_id = $form_input_new->input_id;
                        $actionable_dropdown_item->name = $dropdown_item->name;
                        $actionable_dropdown_item->save();
                    }
    
                    // if (isset($form_input['dropdown_items'])) {
                    //     foreach ($form_input['dropdown_items'] as $dropdown_item) {
                    //         $actionable_dropdown_item = new FormInputDropdownItem;
                    //         $actionable_dropdown_item->form_input_dropdown_id = $form_input_new->id;
                    //         $actionable_dropdown_item->name = $dropdown_item;
                    //         $actionable_dropdown_item->save();
                    //     }
                    // }
                }
        
                //if activity is a dropdown type
                if ($input_name == 'radio') {
        
                    //only add dropdown items if there is input
                    if (isset($form_input['radio_items'])) {
                        //dd($input_input['dropdown_items']);
                        //loop over each dropdown item
                        foreach ($form_input['radio_items'] as $radio_item) {
                            $actionable_radio_item = new FormInputRadioItem;
                            $actionable_radio_item->form_input_radio_id = $form_input_new->id;
                            $actionable_radio_item->name = $radio_item;
                            $actionable_radio_item->save();
                        }
                    }
                }
        
                //if activity is a dropdown type
                if ($input_name == 'checkbox') {
        
                    //only add dropdown items if there is input
                    if (isset($form_input['checkbox_items'])) {
                        //dd($input_input['dropdown_items']);
                        //loop over each dropdown item
                        foreach ($form_input['checkbox_items'] as $checkbox_item) {
                            $actionable_checkbox_item = new FormInputCheckboxItem;
                            $actionable_checkbox_item->form_input_checkbox_id = $form_input_new->id;
                            $actionable_checkbox_item->name = $checkbox_item;
                            $actionable_checkbox_item->save();
                        }
                    }
                }
            }
        }
        }


        

        

        $request->session()->put('flash_success','Activities successfully duplicated.');

        return response()->json($form_input_new);
        // return redirect(back());
    }

    public function getFormTypeDisplayName($input)
    {
        //activity type hook
        switch ($input) {
            case 'App\FormInputText':
                return 'text';
                break;
            case 'App\FormInputAmount':
                return 'amount';
                break;
            case 'App\FormInputPercentage':
                return 'percentage';
                break;
            case 'App\FormInputInteger':
                return 'integer';
                break;
            case 'App\FormInputVideo':
                return 'video';
                break;
            case 'App\FormInputTextarea':
                return 'textarea';
                break;
            case 'App\FormInputBoolean':
            case 'App\FormInputDropdown':
                return 'dropdown';
                break;
            case 'App\FormInputRadio':
                return 'radio';
                break;
            case 'App\FormInputCheckbox':
                return 'checkbox';
                break;
            case 'App\FormInputDate':
                return 'date';
                break;
            case 'App\FormInputHeading':
                return 'heading';
                break;
            case 'App\FormInputSubheading':
                return 'subheading';
                break;
            default:
                return 'error';
                break;
        }
    }
}
