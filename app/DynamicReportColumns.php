<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicReportColumns extends Model
{
    protected $table = 'dynamic_report_columns';

    public function activity_name(){
        return $this->belongsTo('App\Activity','activity_id','id');
    }

    public function crm_name(){
        return $this->belongsTo('App\FormSectionInputs','activity_id','id');
    }

    public function crm_type($input_type){
$type = '';

        switch ($input_type) {
            case 'App\FormInputBoolean':
                $type = 'boolean';
                break;
            case 'App\FormInputDate':
                $type = 'date';
                break;
            case 'App\FormInputText':
                $type = 'text';
                break;
            case 'App\FormInputTextarea':
                $type = 'textarea';
                break;
            case 'App\FormInputDropdown':
                $type = 'dropdown';
                break;
            case 'App\FormInputDocument':
                $type = 'document';
                break;
            case 'App\FormInputTemplateEmail':
                $type = 'template_email';
                break;
            case 'App\FormInputNotification':
                $type = 'notification';
                break;
            case 'App\FormInputMultipleAttachment':
                $type = 'multiple_attachment';
                break;
            default:
                $type = '';
                break;
        }

        return $type;
    }
}
