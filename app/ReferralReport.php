<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferralReport extends Model
{
    protected $table = 'referral_report';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    // public function fc_report_columns(){
    //     return $this->hasMany('App\FcReportColumns','branch_report_id','id');
    // }
}
