@extends('flow.default')

<style>
    .ui-datepicker-calendar {
    display: none;
}
</style>

@section('title') Provider Report @endsection

@section('header')
    <div class="container-fluid" style="margin-top: 1%;margin-bottom: 7px;">
        <h3 style="padding-bottom: 5px;">@yield('title')</h3>
        <br>
        <br>
        <div style="margin-top: 3%;padding-bottom: 5px;display:block;">
            <form autocomplete="off">
                <div class="form-row" style="">
                    @if($crm == 4)
                    <div style="display: block;">
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="fc_filter">Fund Consultant</label>
                            <select name="fc_filter" id="fc_filter" class="form-control form-control-sm">
                                {{-- <option value="all">Fund Consultant</option> --}}
                                <option value="all">All</option>
                                {{-- <option value="blank">Blank</option> --}}
                                @foreach($fund_consultants as $fn)
                                    <option value="{{$fn}}">{{$fn}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="client_filter">Client</label>
                            <select name="client_filter" id="client_filter" class="form-control form-control-sm">
                                {{-- <option value="all">Fund Consultant</option> --}}
                                <option value="all">All</option>
                                {{-- <option value="blank">Blank</option> --}}
                                @foreach($clients_filter as $client)
                                    <option value="{{$client->id}}">{{isset($client->company) ? $client->company : $client->first_name.' '.$client->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="risk_filter">Risk</label>
                            <select name="risk_filter" id="risk_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($risk_filter as $risk)
                                    <option value="{{$risk->name}}">{{$risk->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="risk_type_filter">Risk Type</label>
                            <select name="risk_type_filter" id="risk_type_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($risk_type_filter as $risk_type)
                                    <option value="{{$risk_type->name}}">{{$risk_type->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="risk_renewal">Risk Renewal</label>
                            <input type="text" id="risk_renewal" name="risk_renewal" placeholder="Select month">
                        </div>
                    </div>
                    <div style="display: block;">
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="fund_filter">Fund</label>
                            <select name="fund_filter" id="fund_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($fund_filter as $fund)
                                    <option value="{{$fund->name}}">{{$fund->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="medical_aid_filter">Medical Aid</label>
                            <select name="medical_aid_filter" id="medical_aid_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($medical_aid_filter as $medical_aid)
                                    <option value="{{$medical_aid->name}}">{{$medical_aid->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="medical_insurance_filter">Medical Insurance</label>
                            <select name="medical_insurance_filter" id="medical_insurance_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($medical_insurance_filter as $medical_insurance)
                                    <option value="{{$medical_insurance->name}}">{{$medical_insurance->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="gap_cover_filter">Gap Cover Risk</label>
                            <select name="gap_cover_filter" id="gap_cover_filter" class="form-control form-control-sm">
                                <option value="all">All</option>
                                @foreach($gap_cover_filter as $gap_cover)
                                    <option value="{{$gap_cover->name}}">{{$gap_cover->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mr-3" style="display: inline-block">
                            <label for="category">Category</label>
                            <input type="text" id="category" name="category" placeholder="Category">
                        </div>
                    </div>
                    {{-- <div class="form-group mr-3" style="display: inline-block">
                        <label for="event_date_from">Date From</label>
                        <input type="date" id="event_date_from" name="event_date_from">
                    </div>
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="event_date_to">Date To</label>
                        <input type="date" id="event_date_to" name="event_date_to">
                    </div> --}}
                    @endif
                    <br>
                    
                    {{-- <div class="form-group" style="display: inline-block; margin-left:50%;">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div style="display: block;">
                    <button type="submit" class="btn btn-sm btn-secondary" style="">Submit</button>
                </div>
            </form>
        </div>
        <div style="margin-top: 1%;padding-bottom: 5px;display:inline-block;">
            <a href="{{route('provider_report.pdf')}}" class="btn btn-secondary">PDF</a>
            <a href="#" onclick="excelExport()" class="btn btn-secondary">Excel</a>
        </div>
        <div style="padding-bottom: 5px;display:inline-block;float: right;margin-top:2%;width:400px;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                    <div class="form-group" style="display: inline-block; margin-left:50%;">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')

        <div class="content-container page-content">
            <div class="row col-md-12 h-100 pr-0">
                <div class="container-fluid index-container-content">
                    @yield('header')
                    <div class="table-responsive w-100 float-left client-index" style="height: 43%;position:relative;">
                        <table id="report_table" name="report_table" class="table table-bordered table-hover table-sm table-fixed">
                            <thead>
                            <tr>
                                <th nowrap>@sortablelink('company', 'Client Name')</th>
                                <th nowrap>@sortablelink('consultant', 'Fund Consultant')</th>
                                <th nowrap>Category</th>
                                <th nowrap>Risk</th>
                                <th nowrap>Risk Type</th>
                                <th nowrap>Risk Renewal</th>
                                <th nowrap>Fund</th>
                                <th nowrap>Medical Aid</th>
                                <th nowrap>Medical Insurance</th>
                                <th nowrap>Gap Cover Risk</th>
                                {{-- <th nowrap>Actions</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($parents as $parent)
                                <tr id="row{{$parent['id']}}">
                                    <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
                                    <td>{{$parent['consultant_name']}}</td>
                                    <td>{{$parent['category']}}</td>
                                    <td>{{$parent['risk']}}</td>
                                    <td>{{$parent['risk_type']}}</td>
                                    <td>{{$parent['risk_renewal']}}</td>
                                    <td>{{$parent['fund']}}</td>
                                    <td>{{$parent['medical_aid']}}</td>
                                    <td>{{$parent['medical_insurance']}}</td>
                                    <td>{{$parent['gap_cover']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                    {{-- <div style="position: relative;float:left;bottom: 2%;left: 0px;height:40px;width: 50%;text-align: right;">{{ $parents->links() }}</div> --}}
                    <div style="position: relative;float:right;bottom: 0px;left: 0px;height:40px;width: 50%;text-align: right;"><strong>{{count($parents)}}</strong> Clients shown</div>
                 </div>
            </div>
         </div>

@endsection
@section('extra-js')
    <script>
        $( function() {
            $( "#risk_renewal" ).datepicker({
                changeMonth: true,
                changeYear: true,
                changeDate: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) { 
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
        });
        } );
    </script>
@endsection
