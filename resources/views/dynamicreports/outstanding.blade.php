@extends('flow.default')

@section('title') Outstanding Event Report @endsection

@section('header')
    <div class="container-fluid" style="margin-top: 1%;margin-bottom: 7px;">
        <h3 style="padding-bottom: 5px;">@yield('title')</h3>
        <br>
        <br>
        <div style="margin-top: 3%;padding-bottom: 5px;display:block;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                    @if($crm == 4)
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="fc_filter">Fund Consultant</label>
                        <select name="fc_filter" id="fc_filter" class="form-control form-control-sm">
                            {{-- <option value="all">Fund Consultant</option> --}}
                            <option value="all">All</option>
                            {{-- <option value="blank">Blank</option> --}}
                            @foreach($fund_consultants as $fn)
                                <option value="{{$fn}}">{{$fn}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="event_type">Event Type</label>
                        <select name="event_type" id="event_type" class="form-control form-control-sm">
                            {{-- <option value="all">Event Type</option> --}}
                            <option value="all">All</option>
                            {{-- <option value="blank">Blank</option> --}}
                            @foreach($event_types as $event_type)
                                <option value="{{$event_type->name}}">{{$event_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="client_filter">Client</label>
                        <select name="client_filter" id="client_filter" class="form-control form-control-sm">
                            {{-- <option value="all">Fund Consultant</option> --}}
                            <option value="all">All</option>
                            {{-- <option value="blank">Blank</option> --}}
                            @foreach($clients_filter as $client)
                                <option value="{{$client->id}}">{{isset($client->company) ? $client->company : $client->first_name.' '.$client->last_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="event_date_from">Date From</label>
                        <input type="date" id="event_date_from" name="event_date_from">
                    </div>
                    <div class="form-group mr-3" style="display: inline-block">
                        <label for="event_date_to">Date To</label>
                        <input type="date" id="event_date_to" name="event_date_to">
                    </div>
                    @endif
                    <br>
                    
                    {{-- <div class="form-group" style="display: inline-block; margin-left:50%;">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div style="display: block;">
                    <button type="submit" class="btn btn-sm btn-secondary" style="">Submit</button>
                </div>
            </form>
        </div>
        <div style="margin-top: 2%;padding-bottom: 5px;display:inline-block;">
            <a href="{{route('outstanding_report.pdf')}}" class="btn btn-secondary">PDF</a>
            <a href="#" onclick="excelExport()" class="btn btn-secondary">Excel</a>
        </div>
        <div style="padding-bottom: 5px;display:inline-block;float: right;margin-top:2%;width:400px;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                    <div class="form-group" style="display: inline-block; margin-left:50%;">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')

        <div class="content-container page-content">
            <div class="row col-md-12 h-100 pr-0">
                <div class="container-fluid index-container-content">
                    @yield('header')
                    <div class="table-responsive w-100 float-left client-index" style="height: 48%;position:relative;margin-bottom:15px;">
                        <table id="report_table" name="report_table" class="table table-bordered table-hover table-sm table-fixed">
                            <thead>
                            <tr>
                                <th nowrap>@sortablelink('company', 'Client Name')</th>
                                <th nowrap>@sortablelink('consultant', 'Fund Consultant')</th>
                                <th nowrap>Event Type</th>
                                {{-- <th nowrap>Event Date</th> --}}
                                {{-- <th nowrap>Event Notes</th> --}}
                                {{-- <th nowrap>Actions</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $event_count = 0;
                                @endphp
                                @foreach($parents as $parent)
                                <tr id="row{{$parent['id']}}">
                                    <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
                                    <td>{{$parent['consultant_name']}}</td>
                                    <td>
                                        @forelse ($parent['events'] as $event)
                                            @foreach ($event_types as $event_type)
                                                @if ($event_type->id != $event['type'])
                                                    @if (isset($event_type_filter))
                                                        @if($event_type_filter == $event_type->name)
                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>{{$event_type->name}}</td>
                                                            </tr>
                                                            @php
                                                                $event_count++;
                                                            @endphp
                                                        @endif
                                                    @else
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>{{$event_type->name}}</td>
                                                        </tr>
                                                        @php
                                                            $event_count++;
                                                        @endphp
                                                    @endif
                                                @endif
                                            @endforeach
                                            @break
                                        @empty 
                                            @foreach ($event_types as $event_type)
                                                @if (isset($event_type_filter))
                                                    @if($event_type_filter == $event_type->name)
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>{{$event_type->name}}</td>
                                                        </tr>
                                                        @php
                                                            $event_count++;
                                                        @endphp
                                                    @endif
                                                @else
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td>{{$event_type->name}}</td>
                                                    </tr>
                                                    @php
                                                        $event_count++;
                                                    @endphp
                                                @endif
                                            @endforeach
                                        @endforelse
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                    {{-- <div style="position: relative;float:left;bottom: 2%;left: 0px;height:40px;width: 50%;text-align: right;">{{ $parents->links() }}</div> --}}
                    <div style="position: relative;float:right;bottom: 0px;left: 0px;height:40px;width: 50%;text-align: right;"><strong>{{count($parents)}}</strong> Clients shown <strong>{{$event_count}}</strong> Outstanding events</div>
                 </div>
            </div>
         </div>

@endsection
