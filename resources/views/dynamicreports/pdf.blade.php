<table id="report_table" name="report_table" class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 30px);">
    <thead>
    <tr>
        <th nowrap>@sortablelink('company', 'Client Name')</th>
        <th nowrap>@sortablelink('consultant', 'Fund Consultant')</th>
        <th nowrap>Event Type</th>
        <th nowrap>Event Date</th>
        <th nowrap>Event Notes</th>
        {{-- <th nowrap>Actions</th> --}}
    </tr>
    </thead>
    <tbody>
        @foreach($parents as $parent)
        <tr id="row{{$parent['id']}}">
            <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
            <td>{{$parent['consultant_name']}}</td>
            <td>
                {{-- <table class="table table-bordered table-hover table-sm table-fixed">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody> --}}
                            @foreach ($parent['events'] as $event)
                                <tr>
                                    {{-- <td>{{$event[0]['type']}}</td> --}}
                                    <td colspan="2"></td>
                                    <td>
                                        @foreach ($event_types as $event_type)
                                            @if ($event_type->id == $event[0]['type'])
                                                {{$event_type->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$event[0]['date_scheduled']}}</td>
                                    <td>{{$event[0]['notes']}}</td>
                                </tr>
                            @endforeach
                    {{-- </tbody>
                </table> --}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>