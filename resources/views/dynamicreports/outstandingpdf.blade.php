<table id="report_table" name="report_table" class="table table-bordered table-hover table-sm table-fixed">
    <thead>
    <tr>
        <th nowrap>@sortablelink('company', 'Client Name')</th>
        <th nowrap>@sortablelink('consultant', 'Fund Consultant')</th>
        <th nowrap>Event Type</th>
        {{-- <th nowrap>Event Date</th> --}}
        {{-- <th nowrap>Event Notes</th> --}}
        {{-- <th nowrap>Actions</th> --}}
    </tr>
    </thead>
    <tbody>
        @php
            $event_count = 0;
        @endphp
        @foreach($parents as $parent)
        <tr id="row{{$parent['id']}}">
            <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
            <td>{{$parent['consultant_name']}}</td>
            <td>
                @forelse ($parent['events'] as $event)
                    @foreach ($event_types as $event_type)
                        @if ($event_type->id != $event['type'])
                            @if (isset($event_type_filter))
                                @if($event_type_filter == $event_type->name)
                                    <tr>
                                        <td colspan="2"></td>
                                        <td>{{$event_type->name}}</td>
                                    </tr>
                                    @php
                                        $event_count++;
                                    @endphp
                                @endif
                            @else
                                <tr>
                                    <td colspan="2"></td>
                                    <td>{{$event_type->name}}</td>
                                </tr>
                                @php
                                    $event_count++;
                                @endphp
                            @endif
                        @endif
                    @endforeach
                    @break
                @empty 
                    @foreach ($event_types as $event_type)
                        @if (isset($event_type_filter))
                            @if($event_type_filter == $event_type->name)
                                <tr>
                                    <td colspan="2"></td>
                                    <td>{{$event_type->name}}</td>
                                </tr>
                                @php
                                    $event_count++;
                                @endphp
                            @endif
                        @else
                            <tr>
                                <td colspan="2"></td>
                                <td>{{$event_type->name}}</td>
                            </tr>
                            @php
                                $event_count++;
                            @endphp
                        @endif
                    @endforeach
                @endforelse
            </td>
        </tr>
    @endforeach
    </tbody>
</table>