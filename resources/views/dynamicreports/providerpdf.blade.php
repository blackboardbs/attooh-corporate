<table id="report_table" name="report_table" class="table table-bordered table-hover table-sm table-fixed">
    <thead>
    <tr>
        <th nowrap>@sortablelink('company', 'Client Name')</th>
        <th nowrap>@sortablelink('consultant', 'Fund Consultant')</th>
        <th nowrap>Category</th>
        <th nowrap>Risk</th>
        <th nowrap>Risk Type</th>
        <th nowrap>Risk Renewal</th>
        <th nowrap>Fund</th>
        <th nowrap>Medical Aid</th>
        <th nowrap>Medical Insurance</th>
        <th nowrap>Gap Cover Risk</th>
        {{-- <th nowrap>Actions</th> --}}
    </tr>
    </thead>
    <tbody>
        @foreach($parents as $parent)
        <tr id="row{{$parent['id']}}">
            <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
            <td>{{$parent['consultant_name']}}</td>
            <td>{{$parent['category']}}</td>
            <td>{{$parent['risk']}}</td>
            <td>{{$parent['risk_type']}}</td>
            <td>{{$parent['risk_renewal']}}</td>
            <td>{{$parent['fund']}}</td>
            <td>{{$parent['medical_aid']}}</td>
            <td>{{$parent['medical_insurance']}}</td>
            <td>{{$parent['gap_cover']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>