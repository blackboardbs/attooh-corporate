<table id="report_table" class="table table-bordered table-sm table-hover">
    <thead>
        <tr>
            <th style="min-width: 150px !important; position: sticky;">Client</th>
            {{-- <th>Date</th> --}}
            {{-- <th>Activity Name</th>
            <th>Activity Value</th> --}}
            {{-- <th>Completed</th> --}}
            {{-- <th>Not completed</th> --}}
            {{-- <th>Actions</th> --}}
            @foreach ($clients as $client)
                @foreach ($activities[$client->id] as $activity)
                    @if (isset($activity['name']))
                        <th style="margin-left: 150px;">{{$activity['name']}}</th>
                    @else
                        <th style="margin-left: 150px !important;">{{$activity}}</th>
                    @endif
                @endforeach
                @php
                    break;
                @endphp
            @endforeach
        </tr>
    </thead>
    <tbody>
            @php
                $counter = 0;
            @endphp
            <tr>
            @foreach ($clients as $client)
                <tr>
                    <td>{{$client->company}}</td>
                
                @foreach ($activities[$client->id] as $activity)
                    @if ($activity['type'] != 'subheading' && $activity['type'] != 'heading' && isset($activity['value']))
                        {{-- <tr> --}}
                            {{-- <td>{{$activity['created_at']}}</td> --}}
                            {{-- <td>{{$activity['name']}}</td> --}}
                            <td>
                                @if($activity['type'] == 'dropdown')
                                    @php

                                        $arr = (array)$activity['dropdown_items'];
                                        $arr2 = (array)$activity['dropdown_values'];

                                    @endphp
                                    <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                                    {{$arr[$activity['value']]}}
                                @else
                                    <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{old($activity['id'])}}">
                                @endif
                                @if($activity['type']=='text')
                                    {{-- {{Form::text($activity['id'],(isset($activity['value'])?$activity['value']:old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...','spellcheck'=>'true','disabled'])}} --}}
                                    {{(isset($activity['value'])?$activity['value']:'')}}
                                @endif

                                @if($activity['type']=='percentage')
                                    {{-- <input disabled type="number" min="0" step="1" max="100" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                                    {{(isset($activity['value']) ? $activity['value'] : '')}}
                                @endif

                                @if($activity['type']=='integer')
                                    {{-- <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                                    {{(isset($activity['value']) ? $activity['value'] : '')}}
                                @endif

                                @if($activity['type']=='amount')
                                    {{-- <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                                    {{(isset($activity['value']) ? $activity['value'] : '')}}
                                @endif

                                @if($activity['type']=='date' )
                                    {{-- <input disabled name="{{$activity['id']}}" type="date" min="1900-01-01" max="2030-12-30" value="{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}" class="form-control form-control-sm" placeholder="Insert date..." /> --}}
                                    {{(isset($activity['value']) ? $activity['value'] : '')}}
                                @endif

                                @if($activity['type']=='textarea')
                                    {{-- <textarea disabled spellcheck="true" rows="5" name="{{$activity['id']}}" class="form-control form-control-sm text-area">{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}</textarea> --}}
                                    {{(isset($activity['value']) ? $activity['value'] : '')}}
                                @endif

                                @if($activity['type']=='boolean')

                                    {{$activity['value'] == 1 ? 'Yes' : 'No'}}
                                    {{--{{Form::select($activity['id'],[1=>'Yes',0=>'No'],(isset($activity['value'])?$activity['value']:old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}--}}
                                @endif

                                {{-- @if($activity['type']=='dropdown' )

                                    <select disabled multiple="multiple" name="{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                                        @php
                                            foreach((array) $arr as $key2 => $value2){
                                                echo '<option value="'.$key2.'" '.(in_array($key2,$arr2) ? 'selected' : '').'>'.$value2.'</option>';
                                            }
                                        @endphp
                                    </select>

                                @endif --}}
                            </td>
                        {{-- </tr> --}}
                        
                    @else
                        <td></td>
                    @endif
                @endforeach
                </tr>
            @endforeach
    </tbody>
</table>