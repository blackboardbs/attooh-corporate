<table class="table table-bordered table-sm table-hover overflow: scroll;">
    <thead>
        <tr>
            <th>Client Name</th>
            @foreach ($fields as $field)
            @if (isset($field['name']))
                <th>{{$field['name']}}</th>
            @else
                <th>{{$field}}</th>
            @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($clients as $client)
        @php
            // $counter = 0;
            $max = count($client['data']);
        @endphp
            <tr>
                <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["client_name"] ) && $client["client_name"] != ' ' && $client["client_name"] != null ? $client["client_name"]  : $client["company"])}}</a></td>
                @for ($i = 0; $i < $max; $i++)
                    <td>{{$client['data'][$i]}}</td>
                @endfor
            </tr>
        @endforeach
    </tbody>
</table>