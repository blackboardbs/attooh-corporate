{{-- <table class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: table;border-collapse: collapse">
    <thead>
    <tr>
        <th>Name</th>
        @foreach($fields as $key => $val)
            @if($val != null)
                <th>{{$val}}</th>
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody>
    @forelse($clients as $client)
        @if(isset($client['id']))
            <tr>
                <td class="table100-firstcol">{{$client['company']}}</td>
                @foreach($client["data"] as $key => $val)
                    <td>@if($val != strip_tags($val)) {!! $val !!} @else {{$val}} @endif</td>
                @endforeach
            </tr>
        @endif
    @empty
        <tr>
            <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
        </tr>
    @endforelse
    </tbody>
</table> --}}

<table id="report_table" class="table table-bordered table-sm table-hover;">
    <thead>
        <tr>
            <th style="min-width: 150px !important; position: sticky;">Client Name</th>
            @foreach ($fields as $field)
                @if (isset($field['name']))
                    <th style="margin-left: 150px;">{{$field['name']}}</th>
                @else
                    <th style="margin-left: 150px !important;">{{$field}}</th>
                @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($clients as $client)
        @php
            // $counter = 0;
            $max = count($client['data']);
        @endphp
            <tr> 
                <td style="position: sticky;"><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["client_name"] ) && $client["client_name"] != ' ' && $client["client_name"] != null ? $client["client_name"]  : $client["company"])}}</a></td>
                @for ($i = 0; $i < $max; $i++)
                    <td style="margin-left: 150px;">{{$client['data'][$i]}}</td>
                @endfor
            </tr>
        @endforeach
    </tbody>
</table>