<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <span class="logo-mini"><img src="{!! asset('assets/my-virtual-practice.png') !!}" alt="Blackboard Logo"></span>
        <span class="logo-lg"><img src="{!! asset('assets/my-virtual-practice.png') !!}" alt="Blackboard Logo"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar blackboard-scrollbar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{route('avatar',['q'=>(null !== Auth::user()) ? Auth::user()->avatar : ''])}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('profile')}}" class="d-block">{{ Auth::check() ? Auth::user()->first_name.' '.Auth::user()->last_name : '' }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2" style="padding-bottom: 150px;">
            @if(Auth::check())
                <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-item has-treeview">
                        <a href="{{route('dashboard')}}" class="nav-link {{ (\Request::route()->getName() == 'dashboard') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    @permission('maintain_client')
                    <li class="nav-item has-treeview">
                        <a href="{{route('clients.index')}}" class="nav-link {{ (\Request::route()->getName() == 'clients.index') ? 'active' : '' }}">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Clients
                            </p>
                        </a>
                        <ul class="nav nav-treeview admin-menu">
                            <li class="nav-item has-treeview">
                                <a href="{{route('clients.index')}}?client_type=4" class="nav-link {{ (\Request::route()->getName() == 'templates.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Company
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="{{route('clients.index')}}?client_type=2" class="nav-link {{ (\Request::route()->getName() == 'templates.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Individual
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endpermission
                    <li class="nav-item has-treeview">
                        <a href="{{route('clients.create')}}" class="nav-link {{ (\Request::route()->getName() == 'clients.create') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-plus"></i>
                            <p>
                                Capture Client
                            </p>
                        </a>
                    </li>
                    {{-- <li class="nav-item has-treeview">
                        <a href="{{route('recents')}}" class="nav-link {{ (\Request::route()->getName() == 'recents') ? 'active' : '' }}">
                            <i class="nav-icon far fa-clock"></i>
                            <p>
                                Recents
                            </p>
                        </a>
                    </li> --}}

                    <li class="nav-item has-treeview">
                        <a href="{{route('workflows')}}" class="nav-link {{ (\Request::route()->getName() == 'workflows') ? 'active' : '' }}">
                            <i class="nav-icon fab fa-trello"></i>
                            <p>
                                Pipeline
                            </p>
                        </a>
                    </li>

                    {{-- <li class="nav-item has-treeview">
                        <a href="{{route('reports.myworkday')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.myworkday') ? 'active' : '' }}">
                            <i class="nav-icon fab fa-trello"></i>
                            <p>
                                My Workday
                            </p>
                        </a>
                    </li> --}}

                    <li class="nav-item has-treeview">
                        <a href="{{route('calendar.index')}}" class="nav-link {{ (\Request::route()->getName() == 'calendarevents.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-book"></i>
                            <p>
                                Calendar
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{route('custom_report.index') }}" class="nav-link {{ (\Request::route()->getName() == 'custom_report.index') ? 'active' : '' }}">
                            <i class="nav-icon far fa-chart-bar"></i>
                            <p>
                                Reports
                                <i class="right fa fa-angle-right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('dynamic_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'dynamic_report.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Event Report
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('dynamic_report.outstanding')}}" class="nav-link {{ (\Request::route()->getName() == 'dynamic_report.outstanding') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Outstanding Report
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('dynamic_report.provider')}}" class="nav-link {{ (\Request::route()->getName() == 'dynamic_report.provider') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Provider Report
                                    </p>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a href="{{route('branch_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.branch_report') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Branch Reports
                                    </p>
                                </a>
                            </li> --}}
                            <li class="nav-item">
                                <a href="{{route('consultant_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Consultant Reports
                                    </p>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a href="{{route('fc_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        FC Activity Reports
                                    </p>
                                </a>
                            </li> --}}
                            {{-- <li class="nav-item">
                                <a href="{{route('risk_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Risk Reports
                                    </p>
                                </a>
                            </li> --}}
                            {{-- <li class="nav-item">
                                <a href="{{route('commission_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Commission Reports
                                    </p>
                                </a>
                            </li> --}}
                            {{-- <li class="nav-item">
                                <a href="{{route('contact_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Contact Reports
                                    </p>
                                </a>
                            </li> --}}
                            {{-- <li class="nav-item">
                                <a href="{{route('referral_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Referral Reports
                                    </p>
                                </a>
                            </li> --}}
                            <li class="nav-item">
                                <a href="{{route('custom_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'custom_report.index') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>
                                        Custom Reports
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    @permission('admin')
                    <li class="header"><a href="javascript:void(0)" name="admin-menu" id="admin-menu"><i class="nav-icon fa fa-briefcase"></i><p>ADMIN</p></a></li>
                    @permission('process_editor')
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{route('processesgroup.index')}}" class="nav-link {{ Request::get('t') && (\Request::route()->getName() == 'processesgroup.index') ? '' : ((\Request::route()->getName() == 'processesgroup.index') ? 'active' : '') }}">
                            <i class="nav-icon fa fa-tasks"></i>
                            <p>
                                Processes
                            </p>
                        </a>
                    </li>
                    @endrole
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{route('forms.index')}}" class="nav-link {{ (\Request::route()->getName() == 'forms.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-file"></i>
                            <p>
                                CRM
                            </p>
                        </a>
                    </li>
                    @endrole
                    @endpermission
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{route('card.list')}}" class="nav-link {{ (\Request::route()->getName() == 'card.list') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-alt"></i>
                            <p>
                                Card
                            </p>
                        </a>
                    </li>
                    @endrole
                    @permission('maintain_document')
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{route('documents.index')}}" class="nav-link {{ (\Request::route()->getName() == 'documents.index') ? 'active' : '' }}">
                            <i class="nav-icon far fa-file-alt"></i>
                            <p>
                                Documents
                            </p>
                        </a>
                    </li>
                    @endrole
                    @endpermission
                    @permission('maintain_template')
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="javascript:void(0)" class="nav-link {{ (\Request::route()->getName() == 'templates.index') ? 'active' : '' }}">
                            <i class="nav-icon far fa-file"></i>
                            <p>
                                Templates
                            </p>
                        </a>
                        <ul class="nav nav-treeview admin-menu">
                            <li class="nav-item has-treeview">
                                <a href="{{route('templates.index')}}" class="nav-link {{ (\Request::route()->getName() == 'templates.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Document Templates
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="{{route('emailtemplates.index')}}" class="nav-link {{ (\Request::route()->getName() == 'emailtemplates.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Email Templates
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endrole
                    @endpermission
                    {{-- @permission('actions')
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{route('action.index')}}" class="nav-link {{ (\Request::route()->getName() == 'action.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-project-diagram"></i>
                            <p>
                                Actions
                            </p>
                        </a>
                    </li>
                    @endrole
                    @endpermission --}}
                    @role('manager')
                    <li class="nav-item has-treeview admin-menu">
                        <a href="javascript:void(0)" class="nav-link">
                            <i class="nav-icon fa fa-globe"></i>
                            <p>
                                Master Data
                            </p>
                        </a>
                        <ul class="nav nav-treeview admin-menu">
                            <li class="nav-item has-treeview">
                                <a href="{{route('locations.index')}}" class="nav-link {{ (\Request::route()->getName() == 'locations.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Locations
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="{{route('event_types.index')}}" class="nav-link {{ (\Request::route()->getName() == 'event_types.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        Event Types
                                    </p>
                                </a>
                            </li>
                            {{--<li class="nav-item has-treeview">
                                <a href="{{route('crm_categories.index')}}" class="nav-link {{ (\Request::route()->getName() == 'crm_categories.index') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-circle"></i>
                                    <p>
                                        CRM Categories
                                    </p>
                                </a>
                            </li>--}}
                        </ul>
                    </li>
                    @endrole
                    <li class="nav-item has-treeview admin-menu">
                        <a href="{{ route('users.index')}}" class="nav-link {{ (\Request::route()->getName() == 'users.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-users"></i>
                            <p>
                                Users
                            </p>
                        </a>
                    </li>
                    @role('manager')
                    <li class="nav-item admin-menu">
                        <a href="{{ route('roles.index') }}" class="nav-link {{ (\Request::route()->getName() == 'roles.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-cog"></i>
                            <p>
                                Roles
                            </p>
                        </a>
                    </li>
                    <li class="nav-item admin-menu">
                        <a href="{{route('configs.index')}}" class="nav-link {{ (\Request::route()->getName() == 'configs.index') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-cogs"></i>
                            <p>
                                Configs
                            </p>
                        </a>
                    </li>
                    @endrole
                    @endpermission

                    @endif
                </ul>
                <ul class="nav nav-sidebar support flex-column" role="menu" data-accordion="false" style="width:93%;position: absolute;bottom: 0;">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="https://support.blackboardbs.com/requester/tickets/create" target="_blank" class="nav-link">
                            <i class="nav-icon fas fa-question-circle"></i>
                            <p>
                                Support
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="javascript:void(0)" class="nav-link">
                            <i class="nav-icon fas fa-phone"></i>
                            <p>
                                081 578 9429
                            </p>
                        </a>
                    </li>
                </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
