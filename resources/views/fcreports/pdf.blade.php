<table class="table table-bordered table-sm table-hover">
    <thead>
        <tr>
            {{-- <th>Client</th> --}}
            <th>Date</th>
            <th>Activity Name</th>
            <th>Activity Value</th>
            {{-- <th>Completed</th> --}}
            {{-- <th>Not completed</th> --}}
            {{-- <th>Actions</th> --}}
        </tr>
    </thead>
    <tbody>
        @foreach ($activities as $activity)
        @if ($activity['deleted_at'] == null && $activity['type'] != 'subheading' && $activity['type'] != 'heading' && strtotime($activity['created_at']) >= strtotime($report->date_from) && strtotime($activity['created_at']) <= strtotime($report->date_to) && $report->client_id == $activity['client_id'] && isset($activity['value']))
            <tr>
                <td>{{$activity['created_at']}}</td>
                <td>{{$activity['name']}}</td>
                <td>
                    @if($activity['type'] == 'dropdown')
                        @php

                            $arr = (array)$activity['dropdown_items'];
                            $arr2 = (array)$activity['dropdown_values'];

                        @endphp
                        <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                        {{$arr[$activity['value']]}}
                    @else
                        <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{old($activity['id'])}}">
                    @endif
                    @if($activity['type']=='text')
                        {{-- {{Form::text($activity['id'],(isset($activity['value'])?$activity['value']:old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...','spellcheck'=>'true','disabled'])}} --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='percentage')
                        {{-- <input disabled type="number" min="0" step="1" max="100" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='integer')
                        {{-- <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='amount')
                        {{-- <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" /> --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='date' )
                        {{-- <input disabled name="{{$activity['id']}}" type="date" min="1900-01-01" max="2030-12-30" value="{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}" class="form-control form-control-sm" placeholder="Insert date..." /> --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='textarea')
                        {{-- <textarea disabled spellcheck="true" rows="5" name="{{$activity['id']}}" class="form-control form-control-sm text-area">{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}</textarea> --}}
                        {{(isset($activity['value']) ? $activity['value'] : '')}}
                    @endif

                    @if($activity['type']=='boolean')

                        <div role="radiogroup">
                            <input disabled type="radio" value="1" name="{{$activity["id"]}}" id="{{$activity["id"]}}-enabled" {{(isset($activity["value"]) && $activity["value"] == 1 ? 'checked' : '')}}>
                            <label for="{{$activity["id"]}}-enabled">Yes</label><!-- remove whitespace
                    --><input disabled type="radio" value="0" name="{{$activity["id"]}}" id="{{$activity["id"]}}-disabled" {{(isset($activity["value"]) && $activity["value"] == 1 ? '' : 'checked')}}><!-- remove whitespace
                    --><label for="{{$activity["id"]}}-disabled">No</label>

                            <span class="selection-indicator"></span>
                        </div>
                        {{--{{Form::select($activity['id'],[1=>'Yes',0=>'No'],(isset($activity['value'])?$activity['value']:old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}--}}
                    @endif

                    {{-- @if($activity['type']=='dropdown' )

                        <select disabled multiple="multiple" name="{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                            @php
                                foreach((array) $arr as $key2 => $value2){
                                    echo '<option value="'.$key2.'" '.(in_array($key2,$arr2) ? 'selected' : '').'>'.$value2.'</option>';
                                }
                            @endphp
                        </select>

                    @endif --}}
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>