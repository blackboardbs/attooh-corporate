@extends('client.show')

@section('tab-content')
    <div class="content-container page-content">
        <div class="row col-md-12">
            @yield('header')
            <div class="container table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered table-sm table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Activity Name</th>
                            <th>Activity Value</th>
                        </tr>
                    </thead>
                    <tbody>
                            @forelse ($activities as $activity)
                                @if ($activity['deleted_at'] == null && $activity['type'] != 'subheading' && $activity['type'] != 'heading' && isset($activity['value']))
                                    <tr>
                                        <td>{{$activity['created_at']}}</td>
                                        <td>{{$activity['name']}}</td>
                                        <td>
                                            @if($activity['type'] == 'dropdown')
                                                @php

                                                    $arr = (array)$activity['dropdown_items'];
                                                    $arr2 = (array)$activity['dropdown_values'];

                                                @endphp
                                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                                            @else
                                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{old($activity['id'])}}">
                                            @endif
                                            @if($activity['type']=='text')
                                                {{Form::text($activity['id'],(isset($activity['value'])?$activity['value']:old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...','spellcheck'=>'true','disabled'])}}
                                            @endif

                                            @if($activity['type']=='percentage')
                                                <input disabled type="number" min="0" step="1" max="100" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                            @endif

                                            @if($activity['type']=='integer')
                                                <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                            @endif

                                            @if($activity['type']=='amount')
                                                <input disabled type="number" min="0" step="1" name="{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                            @endif

                                            @if($activity['type']=='date' )
                                                <input disabled name="{{$activity['id']}}" type="date" min="1900-01-01" max="2030-12-30" value="{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}" class="form-control form-control-sm" placeholder="Insert date..." />
                                            @endif

                                            @if($activity['type']=='textarea')
                                                <textarea disabled spellcheck="true" rows="5" name="{{$activity['id']}}" class="form-control form-control-sm text-area">{{(isset($activity['value'])?$activity['value']:old($activity['id']))}}</textarea>
                                            @endif

                                            @if($activity['type']=='boolean')

                                                <div role="radiogroup">
                                                    <input disabled type="radio" value="1" name="{{$activity["id"]}}" id="{{$activity["id"]}}-enabled" {{(isset($activity["value"]) && $activity["value"] == 1 ? 'checked' : '')}}>
                                                    <label for="{{$activity["id"]}}-enabled">Yes</label>
                                                    <input disabled type="radio" value="0" name="{{$activity["id"]}}" id="{{$activity["id"]}}-disabled" {{(isset($activity["value"]) && $activity["value"] == 1 ? '' : 'checked')}}>
                                                    <label for="{{$activity["id"]}}-disabled">No</label>

                                                    <span class="selection-indicator"></span>
                                                </div>
                                            @endif

                                            @if($activity['type']=='dropdown' )

                                                <select disabled multiple="multiple" name="{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                                                    @php
                                                        foreach((array) $arr as $key2 => $value2){
                                                            echo '<option value="'.$key2.'" '.(in_array($key2,$arr2) ? 'selected' : '').'>'.$value2.'</option>';
                                                        }
                                                    @endphp
                                                </select>

                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <span>No activities to show.</span>
                            @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .my-custom-scrollbar {
        position: relative;
        height: 800px;
        overflow: auto;
        }
        .table-wrapper-scroll-y {
        display: block;
        }
        thead th {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 2;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
            background: #FFFFFF;
        }

        /* tbody td:first-child {
            position: -webkit-sticky;
            position: sticky;
            left: 0;
        }
        thead th:first-child {
            left: -1px;
            z-index: 3;
        }
        tbody td:first-child {
            left: -1px;
            z-index: 1;
            background: #FFFFFF;
            border-left: 1px solid #ffffff
        }

        .column-shadow{
            box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -webkit-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            border-left: 1px solid #dee2e6;
        } */
    </style>
@endsection
@section('extra-js')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<script>
    $(document).ready(function() {

        var table = $('#report').DataTable( {
            sPlaceHolder: "head:before",
            orderCellsTop: true,
            info: false,
            paging: true,
            dom: 'lfBtip',
            buttons: ['excel', {extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',exportOptions: {
                    columns: ':visible'
                },
                customize: function(doc) {
                    doc.content.forEach(function(item) {
                        /* if (item.table) {
                            item.table.widths = [40, '*','*']
                         } */
                    })
                }}],
            "pagingType": "full_numbers",
            initComplete: function () {
                this.api().columns().every( function () {
                    var title = this.header();
                    var column = this;
                    var name = $("#report thead tr:eq(1) th").eq(column.index()).data('title')

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('text-input')){
                        console.log('text-input');
                        $(column.header()).append("<br>")
                        var input = $('<input type="text" class="form-control form-control-sm column_search">')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('keyup', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });
                    }

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('date-input')) {
                        $(column.header()).append("<br>")
                        var input = $('<input type="text" class="form-control form-control-sm column_search datepicker">')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('change click', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });

                        $( ".datepicker" ).datepicker({
                            dateFormat: "yy-mm-dd",
                            showOn: "button",
                            showAnim: 'slideDown',
                            showButtonPanel: true ,
                            autoSize: true,
                            buttonImage: "//jqueryui.com/resources/demos/datepicker/images/calendar.gif",
                            buttonImageOnly: true,
                            buttonText: "Select date",
                            closeText: "Clear"
                        });

                    }

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('dropdown-input')) {
                        var select = $('<select id="'+title+'" class="form-control form-control-sm select2 column_search"><option value="">'+name+'</option></select>')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });

                    }
                } );
                $("#report").css('display','table');
            },
            columnDefs: [
                { "width": "20%", "targets": 0 },
            ],
        } );
    } );
    $(document).ready(function()
    {
        $('select').on('change', function () {
            $(this).closest('form').submit();
        });
        $('.js-pscroll').each(function () {
            var ps = new PerfectScrollbar(this);

            $(window).on('resize', function () {
                ps.update();
            })

            $(this).on('ps-x-reach-start', function () {
                $('.table100-firstcol').removeClass('column-shadow');
            });

            $(this).on('ps-scroll-x', function () {
                $('.table100-firstcol').addClass('column-shadow');
            });

        });
    }
    )
</script>
@endsection