@extends('flow.default')

@section('title') Clients @endsection

@section('header')
    <div class="container-fluid container-title float-left clear" style="margin-top: 1%;margin-bottom: 7px;">
        <h3 style="padding-bottom: 5px;">@yield('title')</h3>
        <div class="nav-btn-group" style="top:30%;padding-bottom: 5px;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                    {{-- <div class="form-group mr-1" style="display: inline-block">
                        <select name="client_type" id="client_type" class="form-control form-control-sm">
                            <option value="">Client Type</option>
                            <option value="2" {{(isset($_GET['client_type']) && $_GET['client_type'] == '2' || $crm == 2 ? 'selected="selected"' : '')}}>Individual</option>
                            <option value="4" {{(isset($_GET['client_type']) && $_GET['client_type'] == '4' || $crm == 4 ? 'selected="selected"' : '')}}>Company</option>
                        </select>
                    </div> --}}
                    {{-- @if($crm == 4)
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="fc_filter" id="fc_filter" class="form-control form-control-sm">
                            <option value="all">Fund Consultant</option>
                            <option value="all">All</option>
                            @foreach($fund_consultants as $fn)
                                <option value="{{$fn}}">{{$fn}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="type_filter" id="type_filter" class="form-control form-control-sm">
                            <option value="all">Parent or Branch</option>
                            <option value="Branch">Branch</option>
                            <option value="Parent">Parent</option>
                        </select>
                    </div>
                    @endif --}}
                    {{-- <div class="form-group" style="display: inline-block">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')

        <div class="content-container page-content">
            <div class="row col-md-12 h-100 pr-0">
                <div class="container-fluid index-container-content h-100">
                    <div class="table-responsive d-inline-block pl-0" style="height: 25%;width:100%;">
                        <table class="table table-borderless table-sm table-fixed task-table">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Tasks</h4></th>
                                <th class="last" nowrap style="box-shadow: none;vertical-align: top;">
                                    <a href="javascript:void(0)" onclick="composeUserTask()" class="btn btn-sm btn-primary submit-btn" style="line-height: 1rem !important;">Add a task</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user_tasks as $user_task)
                                <tr class="task-{{$user_task["id"]}}">
                                    <td class="usertask" colspan="100%">{{--<a href="javascript:void(0)" style="display: block;" onclick="showBillboardMessage({{$message->id}})">{{$message->message}}</a>--}}
                                        <span class="pull-right clickable close-icon" onclick="completeUserTask({{$user_task["id"]}})" data-effect="fadeOut"><input type="checkbox" /></span>
                                        <div class="card-block">
                                            <blockquote class="card-blockquote">
                                                <div class="blockquote-body" onclick="showUserTask({{$user_task["id"]}})"><strong>{{\Carbon\Carbon::parse($user_task["task_date_start"])->format('Y-m-d H:i')}} - {{\Carbon\Carbon::parse($user_task["task_date_start"])->format('Y-m-d H:i')}}</strong> : {{$user_task["task_type"]}}<br /><small class="text-muted">{{$user_task["client_name"]}}</small></div>
                                            </blockquote>
                                        </div>
                                    </td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no tasks to display.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                    @yield('header')
                    <div class="table-responsive w-100 float-left client-index" style="height: calc(66% - 30px);position:relative;">
                        @if($crm == 4)
                            <table class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 30px);">
                            <thead>
                            <tr>
                                <th nowrap></th>
                                <th nowrap>@sortablelink('company', 'Client Name')</th>
                                <th nowrap>@sortablelink('email', 'Fund Consultant')</th>
                                <th nowrap>@sortablelink('cell', 'Parent or Branch')</th>
                                <th nowrap>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($parents as $parent)
                                @if (Auth::user()->id == 1 || strtolower(Auth::user()->first_name.' '.Auth::user()->last_name) == strtolower($parent['consultant_name']))
                                <tr>
                                    @if ($parent['crm_id'] != 2)
                                        <td><a href="#" type="button" class="btn btn-secondary btn-sm" onclick="showHideBranches({{$parent['id']}})">+</a></td>
                                    @else
                                        <td></td>
                                    @endif
                                    {{-- <td><a href="#" type="button" class="btn btn-secondary btn-sm" onclick="showHideBranches({{$parent['id']}})">+</a></td> --}}
                                    <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
                                    <td>{{$parent['consultant_name']}}</td>
                                    @if ($parent['crm_id'] != 2)
                                        <td>Parent</td>
                                    @else
                                        <td>Individual</td>
                                    @endif
                                    <td class="last">
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$parent['id']}},{{$parent['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$parent['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$parent['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                    </td>
                                </tr >
                                @foreach ($branches as $key => $branch)
                                    @if ($branch['parent_id'] == $parent['id'])
                                    @if (Auth::user()->id == 1 || strtolower(Auth::user()->first_name.' '.Auth::user()->last_name) == strtolower($branch['consultant_name']))
                                        <tr name="branch{{$branch['parent_id']}}">
                                            <td></td>
                                            <td><a href="{{route('clients.overview',[$branch["id"],$branch["process_id"],$branch["step_id"]])}}">{{(isset($branch["client_name"] ) && $branch["client_name"] != ' ' && $branch["client_name"] != null ? $branch["client_name"]  : $branch["company"])}}</a></td>
                                            <td>{{$branch['consultant_name']}}</td>
                                            @if ($branch['crm_id'] === 2)
                                                <td>Individual</td>
                                            @else
                                                <td>Branch</td>
                                            @endif
                                            {{-- <td>Branch</td> --}}
                                            <td class="last">
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$branch['id']}},{{$branch['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$branch['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$branch['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                            </td>
                                        </tr >
                                        @php
                                            unset($branches[$key]);
                                        @endphp
                                    @endif
                                    @endif
                                @endforeach
                            {{-- @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                </tr>
                            @endforelse --}}
                            @endif
                            @endforeach
                            @foreach ($branches as $key => $branch)
                                @if (Auth::user()->id == 1 || strtolower(Auth::user()->first_name.' '.Auth::user()->last_name) == strtolower($branch['consultant_name']))
                                <tr name="branch{{$branch['parent_id']}}">
                                    <td></td>
                                    <td><a href="{{route('clients.overview',[$branch["id"],$branch["process_id"],$branch["step_id"]])}}">{{(isset($branch["client_name"] ) && $branch["client_name"] != ' ' && $branch["client_name"] != null ? $branch["client_name"]  : $branch["company"])}}</a></td>
                                    <td>{{$branch['consultant_name']}}</td>
                                    @if ($branch['crm_id'] == 2)
                                        <td>Individual</td>
                                    @else
                                        <td>Branch</td>
                                    @endif
                                    {{-- <td>Branch</td> --}}
                                    <td class="last">
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$branch['id']}},{{$branch['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$branch['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$branch['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                    </td>
                                </tr >
                                @endif
                            @endforeach
                            
                            
                            {{-- @forelse($parents as $key => $parent)
                                <tr>
                                    <td><a href="#" type="button" class="btn btn-secondary btn-sm" onclick="showHideBranches({{$parent['id']}})">+</a></td>
                                    <td><a href="{{route('clients.overview',[$parent["id"],$parent["process_id"],$parent["step_id"]])}}">{{(isset($parent["client_name"] ) && $parent["client_name"] != ' ' && $parent["client_name"] != null ? $parent["client_name"]  : $parent["company"])}}</a></td>
                                    <td>{{$parent['consultant_name']}}</td>
                                    <td>Parent</td>
                                    <td class="last">
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$parent['id']}},{{$parent['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$parent['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$parent['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                    </td>
                                </tr>
                                @php
                                    unset($parents[array_search($parent,$parents)]);
                                @endphp
                                @foreach ($branches as $key => $branch)
                                    @if ($branch['parent_id'] == $parent['id'])
                                        <tr name="branch{{$branch['parent_id']}}">
                                            <td></td>
                                            <td><a href="{{route('clients.overview',[$branch["id"],$branch["process_id"],$branch["step_id"]])}}">{{(isset($branch["client_name"] ) && $branch["client_name"] != ' ' && $branch["client_name"] != null ? $branch["client_name"]  : $branch["company"])}}</a></td>
                                            <td>{{$branch['consultant_name']}}</td>
                                            <td>Branch</td>
                                            <td class="last">
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$branch['id']}},{{$branch['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$branch['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$branch['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                            </td>
                                        </tr>
                                    @endif
                                    @php
                                        unset($branches[array_search($branch,$branches)]);
                                    @endphp
                                @endforeach
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                </tr>
                            @endforelse
                            @forelse($branches as $key => $client)
                                <tr>
                                    <td><a href="#" type="button" class="btn btn-secondary btn-sm" onclick="showHideBranches({{$client['id']}})">+</a></td>
                                    <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["client_name"] ) && $client["client_name"] != ' ' && $client["client_name"] != null ? $client["client_name"]  : $client["company"])}}</a></td>
                                    <td>{{$client['consultant_name']}}</td>
                                    <td>Branch</td>
                                    <td class="last">
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$client['id']}},{{$client['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$client['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$client['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                </tr>
                            @endforelse --}}
                            </tbody>
                        </table>

                        @else
                            <table class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 30px);">
                                <thead>
                                <tr>
                                    <th nowrap>@sortablelink('company', 'Name')</th>
                                    <th nowrap>@sortablelink('email', 'Email')</th>
                                    <th nowrap>@sortablelink('cell', 'Cellphone Number')</th>
                                    <th nowrap>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($clients as $client)
                                    <tr>
                                        <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["company"] ) && $client["company"] != ' ' ? $client["company"]  : 'Not Captured')}}</a></td>
                                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                                        <td class="last">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$client['id']}},{{$client['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$client['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$client['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-danger" onclick="submitForSignatures({{$client['id']}},{{$client["process_id"]}},{{$client["step_id"]}})" title="Submit an application for signatures"><i class="fas fa-share-square"></i> </a>
                                        </td>
                                    </tr >
                                @empty
                                    <tr>
                                        <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                    </tr>
                                @endforelse
                                </tbody>

                            </table>

                        @endif

                    </div>
                    <div style="position: relative;float:left;bottom: 0px;left: 0px;height:40px;width: 100%;text-align: right;"><strong>{{count($clients)}}</strong> Clients shown</div>
                 </div>

                  @include('client.modals.index')
            </div>
         </div>

@endsection
