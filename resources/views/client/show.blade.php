@extends('flow.default')

@section('title') {{($client->company == '' || $client->company == 'N/A' || $client->company == 'n/a' ? $client->first_name.' '.$client->last_name : $client->company )}} @endsection

@section('content')
    <div class="client-sidemenu elevation-3" style="opacity: 0">
        <a href="javascript:void(0)" onclick="hideClientInfo()" id="client-sidebar-btn" style="position: absolute;right: 0px;color: #000;z-index: 999;background: #FFF;top: -1px;padding:6px;">
            <span class="fa fa-angle-left"></span>
        </a>
        <input id="clientId" name="clientId" type="text" hidden value="{{$client->id}}">
        <div class="client-info" style="padding: 1.25rem;
    overflow-x: auto;
    height: 100%;">
            {{-- <img src="{{route('clients.avatar',$client)}}" class="client-avatar"/> --}}
            <input type="hidden" id="clientSidebarId" value="{{$client["id"]}}">
            {{-- <div style="margin-bottom:15px;">
                <img src="{{route('clients.avatar',$client)}}" class="client-avatar" style="width:3rem;height:3rem;margin-left:0px;margin-right:7px;margin-bottom:0px;" />
                <!--<span style="display:inline-block;height:3rem;vertical-align:middle;line-height:3rem;font-size:1.25rem;">-->
                <span style="display:inline-block;vertical-align:middle;font-size:1.25rem;width:190px;word-break:break-word;">
                @if($client->crm_id == 4)
                {{($client->company == '' ? 'No company name captured.' : $client->company)}}
                @else
                    {{($client->first_name == '' ? '' : $client->first_name)}} {{($client->last_name == '' ? '' : $client->last_name)}}
                @endif
                </span>
            </div> --}}
            
            {{-- <dd class="seperator">
                &nbsp;
            </dd> --}}
            @if($client->crm_id == '4')
                {{--@if($client->crm->show_default == 0)

                @else--}}
                <dt>
                    Trade Name
                </dt>
                <dd>
                    <span class="noedit trade_name">{{($trade_name == '' ? 'No full name captured.' : $trade_name)}}</span>
                    <span class="yesedit d-none"><input type="text" class="form-control form-control-sm m-0" value="{{$trade_name}}" name="trade_name"></span>
                </dd>
                <dt>
                    Contact Person
                </dt>
                <dd>
                    <span class="noedit contact_name">{{($client->first_name == '' ? 'No contact person captured.' : $client->first_name)}} {{$client->last_name}}</span>
                    <span class="yesedit d-none"><input type="text" class="form-control form-control-sm m-0" value="{{$client->first_name}} {{$client->last_name}}" name="contact_name"></span>
                </dd>
                <dd class="seperator">
                    &nbsp;
                </dd>
                <dt>
                    Contact Email
                </dt>
                <dd>
                    <span class="noedit contact_email">{{($client->email == '' ? 'No email captured.' : $client->email)}}</span>
                    <span class="yesedit d-none"><input type="text" class="form-control form-control-sm m-0" value="{{$client->email}}" name="contact_email"></span>
                </dd>
                <dt>
                    Contact Office Number
                </dt>
                <dd>
                    <span class="noedit contact_officenr">{{($client->contact_office == '' ? 'No office number captured.' : $client->contact_office )}}</span>
                    <span class="yesedit d-none"><input type="text" class="form-control form-control-sm m-0" value="{{$client->contact_office}}" name="contact_officenr"></span>
                </dd>
                <dt>
                    Contact Direct Number
                </dt>
                <dd>
                    <span class="noedit contact_cellnr">{{($client->contact == '' ? 'No cellphone number captured.' : $client->contact )}}</span>
                    <span class="yesedit d-none"><input type="text" class="form-control form-control-sm m-0" value="{{$client->contact}}" name="contact_cellnr"></span>
                </dd>
                <span class="yesedit d-none"><a href="javascript:void(0)" onclick="saveInfo({{$client->id}})" class="btn btn-sm btn-outline-success w-100">Save</a> </span>
                <small class="noedit w-100 text-right d-block"><a href="javascript:void(0)" onclick="editInfo({{$client->id}})">Edit Info</a></small>
                {{--@endif--}}
            @else
                <dt>
                    Full Name
                </dt>
                <dd>
                    {{($client->first_name == '' ? 'No full name captured.' : $client->first_name)}}
                </dd>
                <dt>
                    Surname
                </dt>
                <dd>
                    {{($client->last_name == '' ? 'No surname captured.' : $client->last_name)}}
                </dd>
                @if($client->crm_id != 5)
                    <dt>
                        ID Number
                    </dt>
                    <dd>
                        {{($client->id_number == '' ? 'No ID number captured.' : $client->id_number)}}
                    </dd>
                @endif()
                <dd class="seperator">
                    &nbsp;
                </dd>
                <dt>
                    Email Address
                </dt>
                <dd>
                    {{($client->email == '' ? 'No email captured.' : $client->email)}}
                </dd>
                <dt>
                    Contact Number
                </dt>
                <dd>
                    {{($client->contact == '' ? 'No contact number captured.' : (substr($client->contact,0,1) == '0' ? '+27'.substr($client->contact,1) : $client->contact ))}}
                </dd>
            @endif
            <dd class="seperator">
                &nbsp;
            </dd>
            <dd>
                <a href="javascript:void(0)" onclick="composeMail({{$client['id']}})" class="btn btn-block btn-sm btn-success">Send Mail</a>
            </dd>
            {{--<dd>
                <a href="javascript:void(0)" onclick="composeWhatsapp({{$client['id']}})" class="btn btn-block btn-sm btn-success">Send Whatsapp</a>
            </dd>--}}
            {{-- <dd> --}}
                {{-- <a href="javascript:void(0)" onclick="composeMessage({{$client['id']}},{{$client["process_id"]}},{{$client["step_id"]}})" class="btn btn-block btn-sm btn-success">Send Message</a> --}}
            {{-- </dd> --}}
            {{-- <dd>
                <a href="javascript:void(0)" onclick="toggelClientBasket()" class="btn btn-sm btn-block btn-primary">Add to client basket</a>
            </dd> --}}
            {{-- <dd>
                <a href="javascript:void(0)" class="btn btn-sm btn-block btn-outline-danger" onclick="submitForSignatures({{$client['id']}},{{$client["process_id"]}},{{$client["step_id"]}})" title="Submit an application for signatures">Submit for Signatures</a>
            </dd> --}}
            <dd class="seperator">
                &nbsp;
            </dd>
        </div>
        
    </div>
    <div class="client-basket elevation-3">
        <div class="client-basket-header">
            <span class="client-basket-title" style="display: inline-block;width: 80%;">Client Basket</span>
            <input type="checkbox" class="float-right pull-right client-basket-select-all-all" style="margin-right: 30px; margin-top:40px;" onclick="clientBasketGlobalSelectAll()" data-toggle="tooltip" data-html="true" title="Add all client details to client basket." />
        </div>
        <div class="client-basket-content">
            <form id="client-basket-form" onsubmit="return false">
                <div class="accordion"  id="accordion">
        @foreach($client_details as $data => $tab)
            @foreach($tab as $name => $sections)
                @foreach($sections["data"] as $key => $value)
                            <div class="card" data-name="{{strtolower(str_replace(' ','',$value["name"]))}}">
                                <div class="card-header" id="{{strtolower(str_replace(' ','',$value["name"]))}}-panel">
                                    <h2 class="mb-0">
                                        <button type="button" class="btn btn-link " data-toggle="collapse" data-target="#collapse-{{strtolower(str_replace(' ','',$value["name"]))}}" data-parent="#accordion" style="width: 14rem;">
                                            <span class="{{($data == 1000 ? 'extra' : '')}}">{{$value["name"]}}</span></button>
                                        <input type="checkbox" id="{{strtolower(str_replace(' ','',$value["name"]))}}"  class="float-right pull-right client-basket-select-all" style="margin-right:10px;position: relative;margin-top:14px;" data-toggle="tooltip" data-html="true" onclick="clientBasketSelectAll('{{strtolower(str_replace(' ','',$value["name"]))}}')" title="Add all {{$value["name"]}} activities to client basket." />
                                    </h2>

                                </div>
                                <div id="collapse-{{strtolower(str_replace(' ','',$value["name"]))}}" class="collapse" aria-labelledby="{{str_replace(' ','',$value["name"])}}-panel" data-parent="#accordionExample">
                                    <div class="card-body">
                                        @foreach($value["inputs"] as $input)
                                                <p><span style="display: inline-block;width: 13.45rem;">{{$input["name"]}}</span><input type="checkbox" class="select-this-{{strtolower(str_replace(' ','',$value["name"]))}}" style="position: relative;right:-10px;" onclick="clientBasketSelect('{{strtolower(str_replace(' ','',$value["name"]))}}')" name="add_to_basket[]" value="{{$input['id']}}" id="{{$input['id']}}" {{(in_array($input['id'],$in_details_basket) ? 'checked' : '')}}></p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                @endforeach
            @endforeach
        @endforeach
        </div>
            </form>
        </div>
        <div class="client-basket-footer">
            <div class="client-basket-footer-text">
                <a href="javascript:void(0)" onclick="toggelClientBasket()" class="btn btn-outline-primary">Cancel</a>
                <a href="javascript:void(0)" data-client="{{$client["id"]}}" id="client-basket-add" class="btn btn-success float-right">Add</a>
            </div>
        </div>
    </div>
    <div class="nav-client client" style="margin-left: 22rem; opacity: 0">
        <ul class="nav nav-tabs nav-fill">
            <li class="nav-item">
                <a class="nav-link {{active('clients.overview','active')}}" href="{{route('clients.overview',[$client,$process_id,$step["id"]])}}">Profile Overview</a>
            </li>
            <li class="nav-item">
                {{-- <a class="nav-link {{active('clients.details','active')}}" href="{{route('clients.details',[$client,$process_id,$step["id"],$is_form])}}">Client Details</a> --}}
                <a class="nav-link {{active('clients.details','active')}}" href="{{route('clients.edit',[$client,$process_id,$step['id'],$is_form])}}">Client Details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{active('clients.processes','active')}}" href="{{route('clients.processes',[$client,$process_id,$step["id"],0])}}">Processes</a>
                {{-- <a id="process_tab_link" name="process_tab_link" class="nav-link {{active('clients.processes','active')}}" href="{{route('clients.processes',[$client,$process_id,$step["id"],0])}}" onclick="process_tab_link()">Processes</a> --}}
            </li>
            <li class="nav-item">
                <a class="nav-link {{active('clients.documents','active')}}" href="{{route('clients.documents',[$client,$process_id,$step["id"],$is_form])}}">Document Vault</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link {{active('client.basket','active')}}" href="{{route('client.basket',[$client,$process_id,$step["id"],$is_form])}}">Client Basket</a>
            </li> --}}
            {{-- <li class="nav-item">
                <a class="nav-link {{active('clients.forms','active')}}" href="{{route('clients.forms',[$client,$process_id,$step["id"]])}}">Forms</a>
            </li> --}}
            @if($client->crm_id == 4)
            <li class="nav-item">
                <a class="nav-link {{active('clients.members','active')}}" href="{{route('clients.members',[$client,$process_id,$step["id"]])}}">Members</a>
            </li>
            @endif
            {{-- <li class="nav-item">
                <a class="nav-link {{active('clients.duplicatedactivities','active')}}" href="{{route('clients.duplicatedactivities',[$client,$process_id,$is_form])}}">Duplicated Activities</a>
            </li> --}}
            @if($client->crm_id == '4')
            <li class="nav-item">
                <a class="nav-link" href="{{route('clients.index')}}?client_type=4"><i class="fas fa-chevron-circle-left"></i>Back</a>
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link" href="{{route('clients.index')}}?client_type=2"><i class="fas fa-chevron-circle-left"></i>Back</a>
            </li>
            @endif
        </ul>
    </div>
    <div class="content-container client-content" style="display: none; opacity:0">
        <div class="col-md-12 p-0 h-100">
            @if($client->crm_id == 5)
                <div class="alert alert-info">Go back to <a href="{{route('clients.overview',[$client->parent->id,$client->parent->process_id,$client->parent->step_id])}}">{{$client->parent->name}}</a>.</div>
            @endif
        @yield('tab-content')
        </div>

    </div>
    @include('client.modals.index')
@endsection

@section('extra-js')

<script>
        // function process_tab_link(){
        //     console.log('testt');
        // } 
</script>

@endsection

{{-- New --}}

{{-- @extends('flow.default')

@section('title') {{($client->company == '' || $client->company == 'N/A' || $client->company == 'n/a' ? $client->first_name.' '.$client->last_name : $client->company )}} @endsection

@section('content')
    <div class="client-sidemenu elevation-3">
        <a href="javascript:void(0)" onclick="hideClientInfo()" id="client-sidebar-btn" style="position: absolute;right: 0px;color: #000;z-index: 999;background: #FFF;top: -1px;padding:6px;">
            <span class="fa fa-angle-left"></span>
        </a>
        <div class="client-info" style="padding: 1.25rem;
    overflow-x: hidden;
    height: 100%;">
                <div style="margin-bottom:15px;">
                <img src="{{route('clients.avatar',$client)}}" class="client-avatar" style="width:3rem;height:3rem;margin-left:0px;margin-right:7px;margin-bottom:0px;" />
                <span style="display:inline-block;vertical-align:middle;font-size:1.25rem;width:190px;word-break:break-word;">
                @if($client->crm_id == 4)
                {{($client->company == '' ? 'No company name captured.' : $client->company)}}
                @else
                    {{($client->first_name == '' ? '' : $client->first_name)}} {{($client->last_name == '' ? '' : $client->last_name)}}
                @endif
                </span>
                </div>
                <dd class="seperator mb-3">
                    &nbsp;
                </dd>
                @if($client->crm_id == 4)
                    {{($client->company == '' ? 'No company name captured.' : $client->company)}}
                <dt>
                    Company registration number
                </dt>
                <dd>
                    {{($client->company_registration_number == '' ? 'No company registration number captured.' : $client->company_registration_number)}}
                </dd>
                @endif
                    {{($client->crm_id == 4 ? 'Contact first name' : 'Full name')}}
                    {{($client->first_name == '' ? 'No full name captured.' : $client->first_name)}}
                    {{($client->crm_id == 4 ? 'Contact last name' : 'Surname')}}
                    {{($client->last_name == '' ? 'No surname captured.' : $client->last_name)}}
                @if($client->crm_id != 5 && $client->crm_id != 4)
                    @if($client->id_number != '')
                    <dt>
                        ID number
                    </dt>
                    <dd>
                        {{$client->id_number}}
                    </dd>
                    @endif
                    @if($client->passport_number != '')
                    <dt>
                        Passport number
                    </dt>
                    <dd>
                        {{$client->passport_number}}
                    </dd>
                    @endif
                @endif()
                <dt>
                    {{($client->crm_id == 4 ? 'Contact email' : 'Email address')}}
                </dt>
                <dd>
                    {{($client->email == '' ? 'No email captured.' : $client->email)}}
                </dd>
                <dt>
                    {{($client->crm_id == 4 ? 'Contact telephone number' : 'Contact number')}}
                </dt>
                <dd>
                    {{($client->contact == '' ? 'No contact number captured.' : (substr($client->contact,0,1) == '0' ? '+27'.substr($client->contact,1) : $client->contact ))}}
                </dd>
            <dd class="row pt-3">
                @if($can_update == false)
                    <div class="col-sm-6" style="padding-right: 5px;"><a href="" class="btn btn-block btn-sm btn-success isDisabled">Edit</a></div>
                @else
                    <div class="col-sm-6" style="padding-right: 5px;"><a href="{{route('clients.edit',[$client,$process_id,$step['id'],$is_form])}}" class="btn btn-block btn-sm btn-success">Edit</a></div>
                @endif
                @if($can_delete == false)
                    <div class="col-sm-6" style="padding-left: 5px;"><a href="javascript:void(0)" class="btn btn-block btn-sm btn-danger isDisabled">Delete</a></div>
                @else
                    @if($client->trashed())
                        <div class="col-sm-6" style="padding-left: 5px;"><a href="{{route('clients.delete',$client)}}" disabled class="btn btn-block btn-sm btn-danger deleteclient">Restore</a></div>
                    @else
                        <div class="col-sm-6" style="padding-left: 5px;"><a href="{{route('clients.delete',$client)}}" class="btn btn-block btn-sm btn-danger deleteclient">Delete</a></div>
                    @endif
                @endif
            </dd>
            <dd class="seperator">
                &nbsp;
            </dd>
            <br />
            <br />
            
        </div>
    </div>
    <div class="nav-client client" style="margin-left: 18rem;">
        <ul class="nav nav-tabs nav-fill">
            <li class="nav-item">
                <a class="nav-link {{active('clients.overview','active')}}" href="{{route('clients.overview',[$client,$process_id,$step["id"]])}}">Profile overview</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{active('clients.details','active')}}" href="{{route('clients.details',[$client,$process_id,$step["id"],$is_form])}}">Client details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{active('clients.processes','active')}}" href="{{route('clients.processes',[$client,$process_id,$step["id"],0])}}">Cases</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{active('clients.documents','active')}}" href="{{route('clients.documents',[$client,$process_id,$step["id"],$is_form])}}">Document vault</a>
            </li>
            @if($client->crm_id == 2)
                <li class="nav-item">
                    <a class="nav-link {{active('clients.business','active')}}" href="{{route('clients.business',[$client,$process_id,$step["id"]])}}">Company/Business</a>
                </li>
            @endif
            @if($client->crm_id == 4)
                <li class="nav-item">
                    <a class="nav-link {{active('clients.individuals','active')}}" href="{{route('clients.individuals',[$client,$process_id,$step["id"]])}}">Individuals</a>
                </li>
            @endif
        </ul>
    </div>
    <div class="content-container client-content" style="display: none">
        <div class="col-md-12 p-0 h-100">
        @if($client->crm_id == 4)
            @if($case["individual_client_id"] == '0' || $case["individual_client_id"] == '')
                <div class="alert alert-warning client-warning" style="padding-bottom:1.25rem"><span style="line-height: 40px;height: 24px;display: inline-block;width: fit-content;">There is no contact person selected for this case.</span> <a href="javascript:void(0)" onclick="selectIndividualContact({{$client->id}},{{$case->id}})" class="btn btn-warning float-right" style="color:#FFFFFF !important;text-decoration: none; font-size: 14px; line-height: 22px; border-radius: 1.5rem !important;">Select contact</a></div>
            @endif
        @endif
        @yield('tab-content')
        </div>

    </div>
    
    @include('client.modals.index')
@endsection --}}