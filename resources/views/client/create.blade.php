@extends('flow.default')

@section('title')
    Capture Client
    {{--Capture {{(request()->client_type != null ? ($crm->label == '' ? $crm->name : $crm->label) : '')}} Client--}}
@endsection

@section('content')
    <div class="content-container page-content">
        <div class="col-md-12 h-100">
            <div class="container-fluid container-title">
                <h3>@yield('title')</h3>
                {{Form::open(['url' => route('clients.create'), 'method' => 'post','autocomplete'=>'off','id'=>'clienttype'])}}

                    @if(count($client_type) > 2)
                        <div class="form-group form-inline col-md-4" style="padding: 0px 0px 0px 20px;margin-top: 5px;">
                            <select name="client_type" onchange="clientType()" class="form-control form-control-sm chosen-select">
                                @foreach($client_type as $ct)
                                    <option value="{{$ct->id}}" {{($crm->id === $ct->id ? 'selected' : '')}}>{{($ct->label == '' ? $ct->name : $ct->label)}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="form-group form-inline col-md-4" style="padding: 0px 0px 0px 20px;margin-top: -5px;">
                            <div role="radiogroup" class="mt-0">
                                @foreach($client_type as $ct)
                                    <input type="radio" class="group_step" value="{{$ct->id}}" name="client_type" id="crm_{{$ct->order}}" {{($ct->order == 1 ? 'ref="grouped"' : '')}} {{($crm->id === $ct->id ? 'checked' : '')}}><!-- remove whitespace
                                                                        -->
                                    <label for="crm_{{$ct->order}}">{{($ct->label == '' ? $ct->name : $ct->label)}}</label><!-- remove whitespace
                                                                        -->
                                @endforeach
                                    <span class="selection-indicator"></span>
                            </div>
                        </div>
                    @endif
                {{Form::close()}}
                <div class="nav-btn-group mt-2">
                    <button onclick="saveClientDetails()" class="btn btn-primary float-right ml-2">Save</button>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-md-12 pl-0 pr-0">
                    {{Form::open(['url' => route('clients.store'), 'method' => 'post','autocomplete'=>'off','class'=>'client-capture-content clientdetailsform2','style'=>'display:none;min-width:100%;'])}}
                    <nav class="tabbable">
                        <div class="nav nav-tabs2">
                            @if($crm->show_default == 1)
                            <a class="nav-link {{($crm->show_default == 0 ? '' : 'show active')}}" id="default-tab" data-toggle="tab" href="#default" role="tab" aria-controls="default" aria-selected="false">Default</a>
                            @endif
                            @foreach($forms as $key =>$value)
                                @foreach($value as $section =>$v1)
                                    <a class="nav-link {{(reset($forms) == $value && $crm->show_default == 0 ? 'show active' : '')}}" id="{{strtolower(str_replace(' ','_',$section))}}-tab" data-toggle="tab" href="#{{strtolower(str_replace(' ','_',$section))}}" role="tab" aria-controls="{{strtolower(str_replace(' ','_',$section))}}" aria-selected="true">{{$section}}</a>
                                @endforeach
                            @endforeach
                        </div>
                    </nav>
                    <div class="tab-content2" id="myTabContent">
                        <div class="tab-pane fade {{($crm->show_default == 0 ? '' : 'show active')}}" id="default" role="tabpanel" aria-labelledby="default-tab">

                            <div class="col-lg-12 pl-0 pr-0 mt-3">
                                <input type="hidden" name="process" value="{{($config->default_onboarding_process)}}">
                                <input type="hidden" id="crm_id" name="crm" value="{{($crm->id)}}">
                                <input type="hidden" id="parentId" name="parentId" value=""> 
                                <input type="hidden" id="consultant_name" name="consultant_name" value="">

                                @if($crm->id == '2')
                                <div class="form-group">
                                    {{Form::label('first_name', 'First Names')}}
                                    {{Form::text('first_name',old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                                    @foreach($errors->get('first_name') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{Form::label('last_name', 'Surname')}}
                                    {{Form::text('last_name',old('last_name'),['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
                                    @foreach($errors->get('last_name') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{Form::label('initials', 'Initials')}}
                                    {{Form::text('initials',old('initials'),['class'=>'form-control form-control-sm'. ($errors->has('initials') ? ' is-invalid' : ''),'placeholder'=>'Initials'])}}
                                    @foreach($errors->get('initials') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{Form::label('known_as', 'Known As')}}
                                    {{Form::text('known_as',old('known_as'),['class'=>'form-control form-control-sm'. ($errors->has('known_as') ? ' is-invalid' : ''),'placeholder'=>'Known As'])}}
                                    @foreach($errors->get('known_as') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{Form::label('id_number', 'ID/Passport Number')}}
                                    {{Form::text('id_number',old('id_number'),['class'=>'form-control form-control-sm'. ($errors->has('id_number') ? ' is-invalid' : ''),'placeholder'=>'ID Number', 'id'=>'id_number'])}}
                                    @foreach($errors->get('id_number') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                    {{--<div class="row pt-3" id="hide-smart-id" style="display: none">
                                    </div>--}}
                                </div>

                                <div class="form-group">
                                    {{Form::label('email', 'Email')}}
                                    {{Form::email('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                                    @foreach($errors->get('email') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    {{Form::label('contact', 'Cellphone Number')}}
                                    {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
                                    @foreach($errors->get('contact') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                                @else

                                    <div class="form-group">
                                        {{Form::label('first_name', 'Contact Firstname')}}
                                        {{Form::text('first_name',old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                                        @foreach($errors->get('first_name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        {{Form::label('last_name', 'Contact Surname')}}
                                        {{Form::text('last_name',old('last_name'),['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Contact Surname'])}}
                                        @foreach($errors->get('last_name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        {{Form::label('email', 'Contact Email')}}
                                        {{Form::email('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Contact Email'])}}
                                        @foreach($errors->get('email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        {{Form::label('contact', 'Contact Cellphone Number')}}
                                        {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Cellphone Number'])}}
                                        @foreach($errors->get('contact') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>

                                @endif


                            </div>

                        </div>
                        @foreach($forms as $key =>$value)
                            @foreach($value as $section =>$v1)
                                <div class="tab-pane fade {{(reset($forms) == $value && $crm->show_default == 0 ? 'show active' : '')}} p-3" id="{{strtolower(str_replace(' ','_',$section))}}" role="tabpanel" aria-labelledby="{{strtolower(str_replace(' ','_',$section))}}-tab" style="padding-bottom: 70px !important;">
                                    @if($crm->id == '5')
                                    <div class="list-group-item" style="width:100%;margin-left: calc(100% - 100%);background-color:#f5f5f5;border:1px solid rgba(0,0,0,.125);">
                                        <div style="display:inline-block;width:20px;vertical-align:top;"><i class="fa fa-circle" style="color:rgba(242, 99, 91, 0.7);"></i> </div>
                                        <div style="display: inline-block;width: calc(100% - 25px)">
                                            <span style="width:88%;float: left;display:block;">Company</span>
                                            <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline clientbasket">

                                            </div>
                                            <div class="clearfix"></div>
                                            <select name="parent_client" id="parent_client" class="form-control form-control-sm">
                                                <option value="">Please Select</option>
                                                @foreach($client_dropdown as $cd)
                                                    <option value="{{$cd["id"]}}">{{$cd["name"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    @foreach($v1 as $k1 =>$inputs)
                                        @if(isset($inputs["total_groups"]) && $inputs["total_groups"] > 0)
                                            <input type="hidden" class="max_group" value="{{$inputs['max_group']}}">
                                            <input type="hidden" class="total_groups" value="{{$inputs['total_groups']}}">
                                            @for($i=1;$i <= (int)$inputs['total_groups'];$i++)
                                                <div class="group-{{$i}}" style="{{($inputs['max_group'] != '' && $i <= $inputs['max_group']  ? '' : 'display:none;')}}">
                                            @foreach($inputs["inputs"] as $input)
                                                @if($input['type'] == 'dropdown')
                                                    @php

                                                        $arr = (array)$input['dropdown_items'];
                                                        $arr2 = (array)$input['dropdown_values'];

                                                    @endphp
                                                    <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($input['id']))}}">
                                                @else
                                                    <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{old($input['id'])}}">
                                                @endif
                                                @if($input['type']=='heading')
                                                    <h4 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h4>
                                                @elseif($input['type']=='subheading')
                                                    <h5 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h5>
                                                @else
                                                    <div class="list-group-item" style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' && $input['color'] != null ? $input['color'] : '#f5f5f5'}};border:1px solid rgba(0,0,0,.125);">
                                                        <div style="display:inline-block;width:20px;vertical-align:top;"><i class="fa fa-circle" style="color: {{isset($input['value']) && $input['value'] != null ? 'rgba(50, 193, 75, 0.7)' : 'rgba(242, 99, 91, 0.7)'}}"></i> </div>
                                                        <div style="display: inline-block;width: calc(100% - 25px)">
                                            <span style="width:88%;float: left;display:block;">
                                            {{$input["name"]}}
                                                <small class="text-muted"> [{{$input['type_display']}}] @if($input['kpi']==1) <span class="fa fa-asterisk" title="Activity is required for step completion" style="color:#FF0000"></span> @endif</small>
                                            </span>

                                                            <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline clientbasket">
                                                                <input type="checkbox" class="form-check-input" name="add_to_basket[]" id="{{$input['id']}}" value="{{$input['id']}}">
                                                                <label  for="{{$input['id']}}" class="form-check-label" style="font-weight:normal !important;"> </label>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            @if($input['type']=='text')
                                                                {{Form::text($input['id'],old($input['id']),['class'=>'form-control form-control-sm '. ($input['kpi']==1 ? 'kpi' : ''),'placeholder'=>'Insert text...','spellcheck'=>'true'])}}
                                                            @endif

                                                            @if($input['type']=='percentage')
                                                                <input type="number" min="0" step="1" max="100" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                            @endif

                                                            @if($input['type']=='integer')
                                                                <input type="number" min="0" step="1" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                            @endif

                                                            @if($input['type']=='amount')
                                                                <input type="number" min="0" step="1" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                            @endif

                                                            @if($input['type']=='date')
                                                                <input name="{{$input['id']}}" type="date" min="1900-01-01" max="2030-12-30" value="{{old($input['id'])}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" placeholder="Insert date..." />
                                                            @endif

                                                            @if($input['type']=='textarea')
                                                                <textarea spellcheck="true" rows="5" name="{{$input['id']}}" class="form-control form-control-sm text-area {{($input['kpi']==1 ? 'kpi' : '')}}"></textarea>
                                                            @endif

                                                            @if($input['type']=='boolean')
                                                                <div class="form-group">
                                                                    <label class="radio-inline"><input type="radio" name="{{$input["id"]}}" value="1" {{(isset($input["value"]) && $input["value"] == 1 ? 'checked' : '')}}><span class="ml-2">Yes</span></label>
                                                                    <label class="radio-inline ml-3"><input type="radio" name="{{$input["id"]}}" value="0" {{(isset($input["value"]) && $input["value"] == 0 ? 'checked' : '')}}><span class="ml-2">No</span></label>
                                                                    {{--{{Form::select($input['id'],[1=>'Yes',0=>'No'],old($input['id']),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}--}}
                                                                </div>
                                                            @endif
                                                            @if($input['type']=='dropdown')

                                                                {{-- here --}}
                                                                <select multiple="multiple" name="{{$input["id"]}}[]" class="form-control form-control-sm chosen-select {{($input['kpi']==1 ? 'kpi' : '')}}">
                                                                    @php
                                                                        foreach((array) $arr as $key => $value){
                                                                            echo '<option value="'.$key.'" '.(in_array($key,$arr2) ? 'selected' : '').'>'.$value.'</option>';
                                                                        }
                                                                    @endphp
                                                                </select>
                                                                <div>
                                                                    <small class="form-text text-muted">
                                                                        Search and select multiple entries
                                                                    </small>
                                                                </div>

                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                                </div>
                                            @endfor
                                                @if(isset($inputs["total_groups"]) && $inputs["total_groups"] > 0)
                                                    <div style="margin-top:10px;padding-bottom: 40px;">
                                                        <input type="button" class="btn btn-sm btn-secondary float-right" id="addGroup" value="Add More">
                                                    </div>
                                                @endif
                                        @else
                                        @php
                                            $counter = 1;
                                        @endphp
                                        @foreach($inputs["inputs"] as $input)
                                            @if ($input['visible'] == 0)
                                            @if($input['type'] == 'dropdown')
                                            @php

                                                $arr = (array)$input['dropdown_items'];
                                                $arr2 = (array)$input['dropdown_values'];

                                            @endphp
                                            <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($input['id']))}}">
                                        @else
                                            <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{old($input['id'])}}">
                                        @endif
                                        @if($input['type']=='heading')
                                            <h4 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h4>
                                        @elseif($input['type']=='subheading')
                                        @php
                                            $counter = $counter + 1;
                                        @endphp
                                            {{-- <h5 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h5> --}}
                                            <a class="hide_inputs" style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;display:block;" href="javascript:void(0)" onclick="showhideCreateInputs({{$counter}})"><i class="fas fa-sort-down"></i> <strong>{{$input['name']}}</strong></a>
                                        @else
                                        <div id="input{{$input['id']}}" name="input{{$counter}}" class="input">
                                        <div class="list-group-item list_{{$input['id']}}" style="{{(in_array($input['id'],$input_invisibil) ? 'display:none;' : 'display:table;')}};width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' && $input['color'] != null ? $input['color'] : ''}};border:1px solid rgba(0,0,0,.125);">
                                        <div style="display:inline-block;width:20px;vertical-align:top;"><i class="fa fa-circle" style="color: {{isset($input['value']) && $input['value'] != null ? 'rgba(50, 193, 75, 0.7)' : 'rgba(242, 99, 91, 0.7)'}}"></i> </div>
                                                    <div style="display: inline-block;width: calc(100% - 25px)">
                                            <span style="width:88%;float: left;display:block;">
                                        {{$input["name"]}}
                                            <small class="text-muted"> [{{$input['type_display']}}] @if($input['kpi']==1) <span class="fa fa-asterisk" title="Activity is required for step completion" style="color:#FF0000"></span> @endif</small>
                                        </span>

                                                    <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline clientbasket">
                                                        <input type="checkbox" class="form-check-input" name="add_to_basket[]" {{--id="{{$input['id']}}"--}} value="{{$input['id']}}">
                                                        <label  for="{{$input['id']}}" class="form-check-label" style="font-weight:normal !important;"> </label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    @if($input['type']=='text')
                                                    <input type="text" name="{{$input['id']}}" data-id="{{$input["id"]}}" value="{{old($input['id'])}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi2 ' : '')}}" placeholder="Insert text..." spellcheck="true">
                                                    @endif

                                                    @if($input['type']=='percentage')
                                                        <input type="number" data-id="{{$input["id"]}}" min="0" step="1" max="100" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                    @endif

                                                    @if($input['type']=='integer')
                                                        <input type="number" min="0" step="1" data-id="{{$input["id"]}}" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                    @endif

                                                    @if($input['type']=='amount')
                                                        <input type="number" min="0" step="1" data-id="{{$input["id"]}}" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" spellcheck="true" />
                                                    @endif

                                                    @if($input['type']=='date')
                                                        <input name="{{$input['id']}}" type="date" data-id="{{$input["id"]}}" min="1900-01-01" max="2030-12-30" value="{{old($input['id'])}}" class="form-control form-control-sm {{($input['kpi']==1 ? 'kpi' : '')}}" placeholder="Insert date..." />
                                                    @endif

                                                    @if($input['type']=='textarea')
                                                        <textarea spellcheck="true" rows="5" name="{{$input['id']}}" data-id="{{$input["id"]}}" class="form-control form-control-sm text-area {{($input['kpi']==1 ? 'kpi' : '')}}"></textarea>
                                                    @endif

                                                    @if($input['type']=='boolean')
                                                        <div class="form-group">
                                                            <label class="radio-inline"><input type="radio" name="{{$input["id"]}}" value="1" {{(isset($input["value"]) && $input["value"] == 1 ? 'checked' : '')}}><span class="ml-2">Yes</span></label>
                                                            <label class="radio-inline ml-3"><input type="radio" name="{{$input["id"]}}" value="0" checked {{(isset($input["value"]) && $input["value"] == 0 ? 'checked' : '')}}><span class="ml-2">No</span></label>
                                                            {{--{{Form::select($input['id'],[1=>'Yes',0=>'No'],old($input['id']),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}--}}
                                                        </div>
                                                    @endif
                                                    @if($input['type']=='dropdown')

                                                        <select  name="{{$input["id"]}}[]" class="form-control form-control-sm chosen-select {{($input['kpi']==1 ? 'kpi' : '')}}">
                                                            <option value="0" selected disabled>Select an option...</option>
                                                            @php
                                                                foreach((array) $arr as $key => $value){
                                                                    echo '<option value="'.$key.'" '.(in_array($key,$arr2) ? 'selected' : '').'>'.$value.'</option>';
                                                                }
                                                            @endphp
                                                        </select>
                                                        <div>
                                                            <small class="form-text text-muted">
                                                                Search and select multiple entries
                                                            </small>
                                                        </div>

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                            @endif
                                        @endforeach
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        @endforeach

                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="parentsModal" tabindex="-1" role="dialog" aria-labelledby="parentsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Select a parent this branch belongs to</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                {{-- <label for="parent_id">Select Parent</label> --}}
                <select name="parent_id" id="parent_id" class="form-control form-control-sm">
                    <option value="">Select Parent...</option>
                    @foreach ($parent_arr as $parent)
                        @if ($parent[0]['parent_branch'] == 'Parent')
                        <option value="{{$parent[0]['id']}}">{{$parent[0]['name']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="captureParent()">Save</button>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('extra-js')
    <script>

        $(document).ready(function() {

            var subheadings = document.getElementsByClassName('hide_inputs');

            for (let index = 2; index < subheadings.length; index++) {
                const element = subheadings[index];
                // element.click();
                showhideCreateInputs(index);
            }

            // Array.prototype.forEach.call(subheadings, function(subheading) {
            //     subheading.click()
            // });
        });

        /*$(function(){*/
            /*$(document).on('chosen:ready', function() {*/
        $(document).find('input[name ="client_type"]').on('click',function (){
            clientType();
        });
        $('.chosen-select').chosen().on('chosen:showing_dropdown', function () {
            $('select[name ="client_type"]').on('change',function (){
                clientType();
            });
        });

        

        // $(document).find('input[name ="client_type"]').on('click',function (){
        //     clientType();
        // });

        $('#addGroup').on('click', function() {
            //var cur = $(this).attr('class').match(/\d+$/)[0];
            let cur = parseInt($("#max_group").val());
            let next = cur+1;
            $('.group-'+next).css('display','table');
            $("#max_group").val(next)
        });

        function clientType() {
            $('.client-capture-content').hide();
            $('#overlay').fadeIn();
            $('#clienttype').submit();
        }

        $(".chosen-select").chosen().change(function(e, params){
            /*if(params.deselected) alert("deselected: " + params.deselected);
            else alert("selected: " + params.selected);*/
            var step_id = $('.section_id:visible').val();
            var activity_id = $(this).data('id');
            var activity_value = $(this).val();

            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/getdependantinputs/?input_id='+activity_id+'&input_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.activities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'table');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'table');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                    $.each(data.nonactivities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'none');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'none');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                }
            });

            var inputValues = $('form').find('select').find('option:selected').map(function() {

                if($(this).length === 1) {
                    return $(this).text();
                } else {
                    $(this).each(function(i,n){
                        return $(n).text();
                    });
                }
            }).toArray();

            console.log(inputValues);

            /*$.ajax({
                url: '/getdependantsteps/?activity_id='+activity_id+'&activity_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.steps, function(key, value) {
                        if($('#step_' + value).length) {
                            $('#step_' + value).css('display', 'table');
                            let cnt = $('.step-cnt-' + value).val();
                            $('.step-cnt-' + value).val(1);
                            var step_invisibil = $('#step_invisibil').val().split(',');
                            if(step_invisibil.includes(value)){
                                step_invisibil.splice(step_invisibil.indexOf(value), 1);
                            } else {
                            }

                            $('#step_invisibil').val(step_invisibil.join(','));

                        }
                    });
                    $.each(data.nonsteps, function(key, value) {
                        if($('#step_'+value).length) {
                            if($('.step-cnt-'+value).val() >= 1) {

                                let i = 0;
                                $.ajax({
                                    url: '/getdropdowntext/?option=' + params.deselected,
                                    type: "GET",
                                    dataType: "json",
                                    success: function (data) {
                                        if(data) {
                                            if (inputValues.includes(data)) {
                                            } else {
                                                console.log(data);
                                                i = 1;
                                                cnt--;
                                                $('.step-cnt-' + value).val(cnt);
                                                $('#step_' + value).css('display', 'none');
                                                var step_invisibil = $('#step_invisibil').val().split(',');
                                                if (step_invisibil.includes(value)) {
                                                } else {
                                                    step_invisibil.push(value)
                                                }

                                                $('#step_invisibil').val(step_invisibil.join(','));
                                            }
                                        }
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    }
                                });
                                let cnt = $('.step-cnt-' + value).val();
                                if(i === 1){

                                }

                            }
                        }
                    });
                }
            });*/
        });

        $( "input[type=radio]" ).change(function () {
            var step_id = $('.section_id:visible').val();
            var activity_id = $(this).data('id');
            var activity_value = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/getdependantinputs/?input_id='+activity_id+'&input_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.activities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'table');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'table');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                    $.each(data.nonactivities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'none');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'none');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                }
            });

            /*$.ajax({
                url: '/getdependantsteps/?activity_id='+activity_id+'&activity_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.steps, function(key, value) {
                        if($('#step_' + value).length) {

                            $('#step_' + value).css('display', 'table');
                            let cnt = $('.step-cnt-' + value).val();
                            $('.step-cnt-' + value).val(1);
                            var step_invisibil = $('#step_invisibil').val().split(',');
                            if(step_invisibil.includes(value)){
                                step_invisibil.splice(step_invisibil.indexOf(value), 1);
                            } else {
                            }

                            $('#step_invisibil').val(step_invisibil.join(','));
                        }
                    });
                    $.each(data.nonsteps, function(key, value) {
                        if($('#step_'+value).length) {
                            if($('.step-cnt-'+value).val() >= 1) {
                                let cnt = $('.step-cnt-' + value).val();
                                $('.step-cnt-' + value).val(cnt);
                                $('#step_' + value).css('display', 'none');
                                var step_invisibil = $('#step_invisibil').val().split(',');
                                if(step_invisibil.includes(value)){
                                } else {
                                    step_invisibil.push(value)
                                }

                                $('#step_invisibil').val(step_invisibil.join(','));

                                if(i === 1){

                                }

                            }
                        }
                    });
                }
            });*/
        });

        $( "input" ).keyup(function () {
            var step_id = $('.section_id:visible').val();
            var activity_id = $(this).data('id');
            var activity_value = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/getdependantinputs/?input_id='+activity_id+'&input_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.activities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'table');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'table');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                    $.each(data.nonactivities, function(key, value) {
                        if($('#list_'+value).length) {
                            $('#list_' + value).css('display', 'none');
                            $('#' + value).trigger('chosen:updated');
                        }
                        if($('.list_'+value).length) {
                            $('.list_' + value).css('display', 'none');
                            $('.' + value).trigger('chosen:updated');
                        }
                    });
                }
            });

            /*$.ajax({
                url: '/getdependantsteps/?activity_id='+activity_id+'&activity_value='+activity_value,
                type:"GET",
                dataType:"json",
                success:function(data){
                    $.each(data.steps, function(key, value) {
                        if($('#step_' + value).length) {
                            $('#step_' + value).css('display', 'table');
                        }
                    });
                    $.each(data.nonsteps, function(key, value) {
                        if($('#step_'+value).length) {
                            $('#step_' + value).css('display', 'none');
                        }
                    });
                }
            });*/
        });
    </script>
@endsection