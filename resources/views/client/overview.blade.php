@extends('client.show')

@section('tab-content')
    <div class="row col-md-12 h-100 m-0 p-0">
        <div class="col-md-6 h-100 pt-0 pb-0 pl-0">
            <div class="card-group">
                <div class="card h-100">
                    <h5 class="card-title">Add new event</h5>
                    <div class="card-body pt-0">
                        {{Form::open(['url' => route('event.store'), 'method' => 'post','id'=>'add_event'])}}
                            {{Form::text('event_client_id',$client->id,['class'=>'form-control form-control-sm','hidden','id'=>'event_client_id'])}}
                            {{Form::select('event_type',$event_types_dropdown,old('event_type'),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                            <label for="">Start At:</label>
                            {{-- {{Form::date('date_scheduled',old('date_scheduled'),['class'=>'form-control form-control-sm','placeholder'=>'Add date','id'=>'date_scheduled'])}}  --}}
                            <input type="datetime-local" id="date_scheduled" name="date_scheduled">
                            <label for="">End At:</label>
                            {{-- {{Form::date('end_at',old('end_at'),['class'=>'form-control form-control-sm','placeholder'=>'Add date','id'=>'end_at'])}}  --}}
                            <input type="datetime-local" id="end_at" name="end_at">
                            {{Form::textarea('event_notes',old('event_notes'),['cols'=>'10','rows'=>'3','class'=>'form-control form-control-sm','placeholder'=>'Type your note here','id'=>'event_notes'])}}
                        <input type="submit" class="btn btn-success overview-note-button float-right" value="Save event">
                        {{Form::close()}}
    
                    </div>
                </div>
            </div>
            <div class="card-group overview-addnote">
                <div class="card h-100">
                    <h5 class="card-title">Add a new note</h5>
                    <div class="card-body pt-0">
                        {{Form::open(['url' => route('clients.storecomment', $client), 'method' => 'post','id'=>'add_comment'])}}
                            {{Form::text('heading',old('heading'),['class'=>'form-control form-control-sm','placeholder'=>'Add heading','id'=>'title'])}}
                            {{Form::textarea('comment',old('comment'),['cols'=>'10','rows'=>'3','class'=>'form-control form-control-sm','placeholder'=>'Type your note here','id'=>'comment'])}}
                        <input type="submit" class="btn btn-success overview-note-button float-right" value="Save note">
                        {{Form::close()}}

                    </div>
                </div>
            </div>
            <div class="card-group overview-openapplications">
                <div class="card h-100">
                    <h5 class="card-title d-inline-block float-left">Processes in progress<a href="javascript:void(0)" onclick="startNewApplication({{$client->id}},{{$client->process_id}})" class="float-right d-inline-block" style="font-size: 14px;line-height: 24px;"><i class="fa fa-plus"></i> Start process</a></h5>
                    <div class="card-body overflow-auto open-applications grid-items">
                        <div class="spinner"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 h-100 pt-0 pb-0 pl-0">
            <div class="card-group">
                <div class="card h-100 overflow-auto" style="height: 350px !important">
                    <h5 class="card-title">Events</h5>
                    <div class="card-body pt-0">
                        <div style="height: 100%;margin:auto;width:100%">
                            @forelse ($client_events as $event)
                                @foreach ($event_types as $event_type)
                                    @if ($event_type->id == $event->event_type_id)
                                        {{-- <h5>{{$event_type->name}}</h5>
                                        <label for="date_scheduled">Date Scheduled:</label>
                                        <span>{{$event->date_scheduled}}</span>
                                        <br>
                                        <label for="notes">Notes:</label>
                                        <span>{{$event->event_notes}}</span>
                                        <br><br> --}}
                                        <div class="card">
                                            {{-- <span class="pull-right btn-danger clickable close-icon" onclick="" data-effect="fadeOut"><i class="fa fa-times"></i></span> --}}
                                            <a href="{{route('event.destroy',$event)}}" class="pull-right btn-danger clickable close-icon"><i class="fa fa-times"></i></a>
                                            <div class="card-block">
                                                <blockquote class="card-blockquote">
                                                    <div class="blockquote-header">{{$event_type->name}}</div>
                                                    <div class="blockquote-body">
                                                        {{-- <label for="date_scheduled">Date Scheduled:</label>
                                                        <br>
                                                        <br> --}}
                                                        <label for="">Starts at:</label>
                                                        <br>
                                                        <span>{{$event->date_scheduled}}</span>
                                                        <br>
                                                        <br>
                                                        <label for="">Ends at:</label>
                                                        <br>
                                                        <span>{{$event->end_at}}</span>
                                                        <br>
                                                        <br>
                                                        <label for="notes">Notes:</label>
                                                        <br>
                                                        <span>{{$event->event_notes}}</span>
                                                    </div>
                                                    <div class="blockquote-footer">{{$event->created_at}}</div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @empty
                                <div class="alert alert-info">There are currently no Events for this client.</div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-group">
                <div class="card h-100 overflow-auto" style="height: 350px !important">
                    <h5 class="card-title">Notes</h5>
                    <div class="card-body client-notes pt-0">
                        <div style="height: 100%;margin:auto;width:100%">
                        <div class="spinner"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="col-md-6 h-100 pt-0 pb-0 pr-0">
            <div class="card h-100 overflow-auto">
                <h5 class="card-title">Notes</h5>
                <div class="card-body client-notes pt-0">
                    <div style="height: 100%;margin:auto;width:100%">
                    <div class="spinner"></div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
@endsection
@section('extra-js')
    <script>

        function deletecomment(id){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/clients/deleteclientcomment/' + id,
                type: "POST",
                data: {comment: id},
                success: function (data) {
                    toastr.success('<strong>Success!</strong> ' + data);

                    toastr.options.timeOut = 1000;

                    $.ajax({
                        url: '/clients/' + {{ $client->id }} + '/getcomments',
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            let row = '';

                            $.each(data.data, function(key,value) {
                                row = row + '<div class="card">' +
                                    '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                    '<div class="card-block">' +
                                    '<blockquote class="card-blockquote">' +
                                    '<div class="blockquote-header">' + value.title + '</div>' +
                                    '<div class="blockquote-body">' + value.comment + '</div>' +
                                    '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                    '</blockquote>' +
                                    '</div>' +
                                    '</div>';
                            });

                            $('.client-notes').html(row);
                        }
                    });
                }
            });
        }
        $(function (){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/clients/' + {{ $client->id }} + '/getcomments',
                type: "GET",
                dataType: "json",
                success: function (data) {
                    let row = '';
                    let client_id = {{$client->id}}

                    if(data.data.length > 0) {
                        $.each(data.data, function(key,value) {
                            row = row + '<div class="card">' +
                                '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                '<div class="card-block">' +
                                '<blockquote class="card-blockquote">' +
                                '<div class="blockquote-header">' + value.title + '</div>' +
                                '<div class="blockquote-body">' + value.comment + '</div>' +
                                '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                '</blockquote>' +
                                '<a href="javascript:void(0)" onclick="composeNoteMail(\'' + value.comment + '\')" class="btn btn-sm btn-success" style="margin:1%;">Send Mail</a>' +
                                '</div>' +
                                '</div>';
                        });
                    } else {
                        row = row + '<div class="alert alert-info">There are currently no Notes for this client.</div>';
                    }

                    $('.client-notes').html(row);
                }
            });

            $.ajax({
                url: '/clients/' + {{ $client->id }} + '/current_applications',
                type: "GET",
                dataType: "json",
                success: function (data) {

                    let row = '';

                    if(data.length > 0) {
                        $.each(data, function (key, value) {
                            row = row + '<div class="d-table" style="width: 100%;border: 1px solid #ecf1f4;margin-bottom:0.75rem;">' +
                                '<div class="grid-icon">' +
                                '<i class="far fa-file-alt"></i>' +
                                '</div>' +
                                '<div class="grid-text">' +
                                '<span class="grid-heading">' + value.name + '</span>' +
                                'Date started: ' + value.created_at +
                                '</div>' +
                                '<div class="grid-btn">' +
                                '<a href="/clients/' + {{ $client->id }} +'/progress/' + value.process_id + '/' + value.step_id + '/0" class="btn btn-outline-primary btn-block">Continue</a>' +
                                '</div>' +
                                '</div>';
                        });
                    } else {
                        row = row + '<div class="alert alert-info">There are currently no Applications in progress for this client.</div>';
                    }

                    $('.open-applications').html(row);
                }
            });

            $('#add_comment').submit(function (e) {
                e.preventDefault();

                let err = 0;
                let title = $('#title').val();
                let comment = $('#comment').val();

                if(title.length === 0){
                    err++;
                    $('#title').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#title').removeClass('is-invalid').addClass('is-valid');
                }

                if(comment.length === 0){
                    err++;
                    $('#comment').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#comment').removeClass('is-invalid').addClass('is-valid');
                }

                if(err === 0) {
                    $.ajax({
                        url: '/clients/' + {{ $client->id }} +'/storecomment',
                        type: "POST",
                        data: {title: title, comment: comment},
                        success: function (data) {
                            toastr.success('<strong>Success!</strong> ' + data);

                            toastr.options.timeOut = 1000;

                            $('#title').removeClass('is-valid').val('');
                            $('#comment').removeClass('is-valid').val('');

                            $.ajax({
                                url: '/clients/' + {{ $client->id }} + '/getcomments',
                                type: "GET",
                                dataType: "json",
                                success: function (data) {
                                    let row = '';

                                    $.each(data.data, function(key,value) {
                                        row = row + '<div class="card">' +
                                            '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                            '<div class="card-block">' +
                                            '<blockquote class="card-blockquote">' +
                                            '<div class="blockquote-header">' + value.title + '</div>' +
                                            '<div class="blockquote-body">' + value.comment + '</div>' +
                                            '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                            '</blockquote>' +
                                            '</div>' +
                                            '</div>';
                                    });

                                    $('.client-notes').html(row);
                                }
                            });
                        }
                    });
                }
            });
        })
    </script>
@endsection
