@extends('flow.default')
<style>
    .ui-datepicker-calendar{
            display: none;
        }
</style>
@section('title') Create Report @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <a href="javascript:void(0)" onclick="saveBranchReport()" class="btn btn-success btn-sm mt-3 ml-2 float-right">Save</a>
            <a href="{{route('branch_report.index')}}" class="btn btn-outline-primary btn-sm mt-3 float-right">Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content" style="overflow: auto !important;">
                {{Form::open(['url' => route('branch_report.store'), 'method' => 'post','files'=>true,'id'=>'branchreportform'])}}
                <div class="form-group mt-3"> 
                    {{Form::label('name', 'Report Name:')}}
                    {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                    @foreach($errors->get('name') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group mt-3">
                    {{Form::label('parent_id', 'Parents:')}}
                    {{Form::select('parent_id',$parents,null,['class'=>'form-control form-control-sm'. ($errors->has('parent_id') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('parent_id') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                {{-- <div class="form-group mt-3">
                    {{Form::label('month', 'Month:')}}
                    {{Form::text('month',old('month'),['class'=>'form-control form-control-sm'. ($errors->has('month') ? ' is-invalid' : ''),'placeholder'=>'Month'])}}
                    @foreach($errors->get('month') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div> --}}
                <div class="form-group mt-3">
                    {{Form::label('report_type', 'Report Type')}}
                    <div class="col-lg-8">
                        <div role="radiogroup" class="mt-0">
                            <input type="radio" class="group_step" value="process" name="group_step" id="group_step-enabled" ref="grouped">
                            <label for="group_step-enabled">Process</label><!-- remove whitespace
                                                                    --><input type="radio" class="group_step" value="crm" name="group_step" id="group_step-disabled" checked><!-- remove whitespace
                                                                    --><label for="group_step-disabled">CRM</label>

                            <span class="selection-indicator"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3" id="processdiv" style="display: none;">
                    {{Form::label('process', 'Process to use for report:')}}

                    {{Form::select('process',$process ,old('process'),['class'=>'form-control form-control-sm','id' => 'process'])}}
                    @foreach($errors->get('process') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group mt-3" id="crmdiv" style="display: none;">
                    {{Form::label('crm', 'CRM to use for report:')}}

                    {{Form::select('crm',$crm ,old('crm'),['class'=>'form-control form-control-sm','id' => 'crm'])}}
                    @foreach($errors->get('process') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                {{-- <div class="form-group mt-3">
                    {{Form::label('group', 'Group Report')}}
                    <div class="col-lg-4 text-left">
                        <div>
                            <input name="group_report" id="group_report" ref="grouped" type="checkbox" />
                        </div>
                    </div>
                    @foreach($errors->get('group_report') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div> --}}
                <div class="form-group mt-3">
                    {{Form::label('activity', 'Activity columns to display on report:')}}
                    <input class="form-control form-control-sm" type="text" id="reportActivitySearch" name="reportActivitySearch" placeholder="Search...">
                </div>
                @foreach($errors->get('activity') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                <div class="form-group pb-3 mb-3" id="activities">
                    

                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(document).ready(function() {

            getActivities('crm', '2');

            if($('input[name="group_step"]:checked').val() === 'process'){
                $('#processdiv').show();
                $('#crmdiv').hide();
            }
            if($('input[name="group_step"]:checked').val() === 'crm'){
                $('#processdiv').hide();
                $('#crmdiv').show();
            }

            function getActivities(type,id) {
                // console.log(type);
                // console.log(id);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    dataType: 'json',
                    url: '/get_report_activities/' + id,
                    type: 'GET',
                    data: {type:type,process_id: id}
                }).done(function (data) {
                    let rows = '<div class="col-sm-12 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" name="activity__all" id ="activity_all" value="all" onclick="selectAll()" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;">Select All</span></div>';
                    let counter = 0;
                    $.each(data, function (key, value) {
                        // if (value.visible != 1) {
                            rows = rows + '<div class="col-sm-12 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><span style="display: table-cell;word-break: break-word;overflow-wrap: break-word;font-weight:bold;"><a href="javascript:void(0)" onclick="showhideReportInputs(' + counter + ')"><i class="fas fa-sort-down"></i> ' + value.name + '</a></span></div>';
                        // }
                        $.each(value.activity, function (key, value) {
                            if(value.grouping == 0) {
                                // if (value.visible != 1) {
                                    rows = rows + '<div class="col-sm-4 pull-left pb-2 activity" name="input' + counter + '" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" class="cactivity" name="activity[]" value="' + value.id + '" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;"><p>' + value.name + '</p></span></div>'; 
                                // }
                            } else {
                                // if (value.visible != 1) {
                                    rows = rows + '<div class="col-sm-4 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" class="cactivity" name="activity[]" value="' + value.id + '" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;">' + value.name + '&nbsp;&nbsp;&nbsp;<i class="fa fa-object-group" aria-hidden="true" style="font-size:0.75em"></i></span></div>';
                                // }
                            }
                        });
                        counter++;
                    });
                    //alert(rows);
                    $("#activities").html(rows);
                });
            }

            $('input[name="group_step"]').on("change",function(){
                //alert($('input[name="group_step"]:checked').val());
                if($('input[name="group_step"]:checked').val() === 'process'){
                    $('#processdiv').show();
                    $('#crmdiv').hide();
                }
                if($('input[name="group_step"]:checked').val() === 'crm'){
                    $('#processdiv').hide();
                    $('#crmdiv').show();
                }
                //getActivities();
            })

            $('#process').on("change",function(){
                getActivities('process',$('#process').val());
            })

            $('#crm').on("change",function(){
                getActivities('crm',$('#crm').val());
            })

        });

        function selectAll(){
            if($("#activity_all").is(':checked')){
                $('.cactivity').each(function () {
                    if($(this).is(':disabled')) {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', true);
                    }
                })
            } else {
                $('.cactivity').each(function () {
                    if($(this).is(':disabled')) {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', false);
                    }
                })
            }
        }
    </script>
@endsection