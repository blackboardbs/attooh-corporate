@extends('flow.default')
@section('title') Show Report - {{$report->name}}@endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <button onclick="excelExport()" type="button" id="export_button" class="btn btn-success btn-sm mt-3 float-right">Excel</button>
            <a href="{{route('contact_report.pdf', [$report->id])}}" class="btn btn-primary mt-3 btn-sm float-right">PDF</a>
            <a href="{{route('contact_report.index')}}" class="btn btn-outline-primary mt-3 btn-sm float-right">Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12">
            @yield('header')
            <div class="container table-wrapper-scroll-y my-custom-scrollbar">
                <table id="report_table" class="table table-bordered table-sm table-hover">
                    <thead>
                        <tr>
                            <th>Office Number</th>
                            <th>Website</th>
                            <th>Contact Person</th>
                            <th>Direct Number</th>
                            <th>Email Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @php
                                $counter = 0;
                            @endphp
                            @forelse ($acts1 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                        <tr>
                            @forelse ($acts2 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                        <tr>
                            @forelse ($acts3 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                        <tr>
                            @forelse ($acts4 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                        <tr>
                            @forelse ($acts5 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                        <tr>
                            @forelse ($acts6 as $act)
                                    {{-- <tr> --}}
                                        @if (isset($act['value']))
                                            @if ($act['name'] == 'Contact person')
                                            <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Website URL')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            
                                            @if ($act['name'] == 'Office Number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Direct number')
                                                <td>{{$act['value']}}</td>
                                            @endif

                                            @if ($act['name'] == 'Email address')
                                                <td>{{$act['value']}}</td>
                                            @endif
                                            @php
                                                $counter++;
                                            @endphp
                                        @else
                                            <td></td>
                                        @endif
                                    {{-- </tr> --}}
                            @empty
                                    
                            @endforelse
                        </tr>
                    </tbody>
                </table>
                {{-- <div style="position: relative;float:left;bottom: 0px;left: 0px;height:40px;width: 100%;text-align: right;"><strong>{{$counter}}</strong> records shown</div> --}}
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .my-custom-scrollbar {
        position: relative;
        height: 800px;
        overflow: auto;
        }
        .table-wrapper-scroll-y {
        display: block;
        }
        /* thead th {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 2;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
            background: #FFFFFF;
        }

        tbody td:first-child {
            position: -webkit-sticky;
            position: sticky;
            left: 0;
        }
        thead th:first-child {
            left: -1px;
            z-index: 3;
        }
        tbody td:first-child {
            left: -1px;
            z-index: 1;
            background: #FFFFFF;
            border-left: 1px solid #ffffff
        }

        .column-shadow{
            box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -webkit-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            border-left: 1px solid #dee2e6;
        } */
    </style>
@endsection
@section('extra-js')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        /*$('#report thead tr').clone(true).appendTo( '#report thead' );
        $('#report thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );*/

        var table = $('#report').DataTable( {
            sPlaceHolder: "head:before",
            orderCellsTop: true,
            info: false,
            paging: true,
            dom: 'lfBtip',
            buttons: ['excel', {extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',exportOptions: {
                    columns: ':visible'
                },
                customize: function(doc) {
                    doc.content.forEach(function(item) {
                        /* if (item.table) {
                            item.table.widths = [40, '*','*']
                         } */
                    })
                }}],
            "pagingType": "full_numbers",
            initComplete: function () {
                this.api().columns().every( function () {
                    var title = this.header();
                    var column = this;
                    var name = $("#report thead tr:eq(1) th").eq(column.index()).data('title')

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('text-input')){
                        console.log('text-input');
                        $(column.header()).append("<br>")
                        var input = $('<input type="text" class="form-control form-control-sm column_search">')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('keyup', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });
                    }

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('date-input')) {
                        $(column.header()).append("<br>")
                        var input = $('<input type="text" class="form-control form-control-sm column_search datepicker">')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('change click', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });

                        $( ".datepicker" ).datepicker({
                            dateFormat: "yy-mm-dd",
                            showOn: "button",
                            showAnim: 'slideDown',
                            showButtonPanel: true ,
                            autoSize: true,
                            buttonImage: "//jqueryui.com/resources/demos/datepicker/images/calendar.gif",
                            buttonImageOnly: true,
                            buttonText: "Select date",
                            closeText: "Clear"
                        });

                    }

                    if($("#report thead tr:eq(1) th").eq(column.index()).hasClass('dropdown-input')) {
                        var select = $('<select id="'+title+'" class="form-control form-control-sm select2 column_search"><option value="">'+name+'</option></select>')
                            .appendTo($("#report thead tr:eq(1) th").eq(column.index()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( this.value )
                                    .draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });

                    }
                } );
                $("#report").css('display','table');
            },
            columnDefs: [
                { "width": "20%", "targets": 0 },
            ],
        } );
    } );
    $(document).ready(function()
    {
        $('select').on('change', function () {
            $(this).closest('form').submit();
        });
        $('.js-pscroll').each(function () {
            var ps = new PerfectScrollbar(this);

            $(window).on('resize', function () {
                ps.update();
            })

            $(this).on('ps-x-reach-start', function () {
                $('.table100-firstcol').removeClass('column-shadow');
            });

            $(this).on('ps-scroll-x', function () {
                $('.table100-firstcol').addClass('column-shadow');
            });

        });
    }
    )
</script>
@endsection