@extends('flow.default')
@section('title') Create Report @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <a href="javascript:void(0)" onclick="saveContactReport()" class="btn btn-success btn-sm mt-3 ml-2 float-right">Save</a>
            <a href="{{route('contact_report.index')}}" class="btn btn-outline-primary btn-sm mt-3 float-right">Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content" style="overflow: auto !important;">
                {{Form::open(['url' => route('contact_report.store'), 'method' => 'post','files'=>true,'id'=>'contactreportform'])}}
                <div class="form-group mt-3">
                    {{Form::label('name', 'Report Name:')}}
                    {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                    @foreach($errors->get('name') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group mt-3">
                    {{Form::label('client_id', 'Client:')}}
                    {{Form::select('client_id',$clients,null,['class'=>'form-control form-control-sm'. ($errors->has('client_id') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('client_id') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(document).ready(function() {

            if($('input[name="group_step"]:checked').val() === 'process'){
                $('#processdiv').show();
                $('#crmdiv').hide();
            }
            if($('input[name="group_step"]:checked').val() === 'crm'){
                $('#processdiv').hide();
                $('#crmdiv').show();
            }

            getActivities('crm',4);

            // $('#crm').value = 4;
            // e = $('#crm');
            // e.options[e.selectedIndex] = 2;
            // document.getElementById('crm').getElementsByTagName('option')[4].selected = 'selected'

            function getActivities(type,id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    dataType: 'json',
                    url: '/get_report_activities/' + id,
                    type: 'GET',
                    data: {type:type,process_id: id}
                }).done(function (data) {
                    let rows = '<div class="col-sm-12 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" name="activity__all" id ="activity_all" value="all" onclick="selectAll()" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;">Select All</span></div>';
                    $.each(data, function (key, value) {
                        if (value.name == 'Fund Consultant Activity') {
                            rows = rows + '<div class="col-sm-12 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><span style="display: table-cell;word-break: break-word;overflow-wrap: break-word;font-weight:bold;">' + value.name + '</span></div>';
                            $.each(value.activity, function (key, value) {
                                if(value.grouping == 0) {
                                    rows = rows + '<div class="col-sm-4 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" class="cactivity" name="activity[]" value="' + value.id + '" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;">' + value.name + '</span></div>';
                                } else {
                                    rows = rows + '<div class="col-sm-4 pull-left pb-2" style="min-height:50px;display: inline-block"><span style="display: table-cell"><input type="checkbox" class="cactivity" name="activity[]" value="' + value.id + '" /></span><span style="display: table-cell;word-break: break-word;padding-left:5px;overflow-wrap: break-word;">' + value.name + '&nbsp;&nbsp;&nbsp;<i class="fa fa-object-group" aria-hidden="true" style="font-size:0.75em"></i></span></div>';
                                }
                            });
                        }
                    });
                    //alert(rows);
                    $("#activities").html(rows);
                });
            }

            $('input[name="group_step"]').on("change",function(){
                //alert($('input[name="group_step"]:checked').val());
                if($('input[name="group_step"]:checked').val() === 'process'){
                    $('#processdiv').show();
                    $('#crmdiv').hide();
                }
                if($('input[name="group_step"]:checked').val() === 'crm'){
                    $('#processdiv').hide();
                    $('#crmdiv').show();
                }
                //getActivities();
            })

            $('#process').on("change",function(){
                getActivities('process',$('#process').val());
            })

            $('#crm').on("change",function(){
                getActivities('crm',$('#crm').val());
            })

        });

        function selectAll(){
            if($("#activity_all").is(':checked')){
                $('.cactivity').each(function () {
                    if($(this).is(':disabled')) {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', true);
                    }
                })
            } else {
                $('.cactivity').each(function () {
                    if($(this).is(':disabled')) {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', false);
                    }
                })
            }
        }
    </script>
@endsection