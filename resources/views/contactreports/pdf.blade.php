<table id="report_table" class="table table-bordered table-sm table-hover">
    <thead>
        <tr>
            <th>Office Number</th>
            <th>Website</th>
            <th>Contact Person</th>
            <th>Direct Number</th>
            <th>Email Address</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            @forelse ($acts1 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
        <tr>
            @forelse ($acts2 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
        <tr>
            @forelse ($acts3 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
        <tr>
            @forelse ($acts4 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
        <tr>
            @forelse ($acts5 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
        <tr>
            @forelse ($acts6 as $act)
                    {{-- <tr> --}}
                        @if (isset($act['value']))
                            @if ($act['name'] == 'Contact person')
                            <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Website URL')
                                <td>{{$act['value']}}</td>
                            @endif
                            
                            @if ($act['name'] == 'Office Number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Direct number')
                                <td>{{$act['value']}}</td>
                            @endif

                            @if ($act['name'] == 'Email address')
                                <td>{{$act['value']}}</td>
                            @endif
                        @else
                            <td></td>
                        @endif
                    {{-- </tr> --}}
            @empty
                    
            @endforelse
        </tr>
    </tbody>
</table>