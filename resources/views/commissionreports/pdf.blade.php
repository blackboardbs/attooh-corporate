<table id="report_table" class="table table-bordered table-sm table-hover">
    <thead>
        <tr>
            <th>Consultant</th>
            <th>Commission %</th>
        </tr>
    </thead>
    <tbody>
            @php
                $counter = 0;
            @endphp
            @foreach ($fields as $field)
                    <tr>
                        <td>{{$field['name']}}</td>
                        <td>{{$field['value']}}%</td>
                    </tr>
                    @php
                        $counter++;
                    @endphp
            @endforeach
    </tbody>
</table>