@extends('flow.default')

@section('title') Event Types @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <form autocomplete="off">
                <div class="form-row">
                    <div class="mr-2 mt-3">
                        <div class="btn-group">
                            <a href="{{route('event_types.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Event Type</a>
                        </div>
                    </div>
                    {{-- <div class="form-group mt-2">
                        <div class="input-group">
                            {{Form::search('q',old('query'),['class'=>'form-control search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content">
                <div class="table-responsive h-100">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($event_types as $event_type)
                            <tr>
                                <td>{{$event_type->name}}</td>
                                <td>{{$event_type->created_at}}</td>
                                <td class="last">
                                    <a href="{{route('event_types.edit',$event_type)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="{{route('event_types.destroy',$event_type)}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No event types match those criteria.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
