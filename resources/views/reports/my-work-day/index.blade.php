@extends('flow.default')

@section('title') My Work Day @endsection
 
@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100">
            <div class="container-fluid" style="padding-top: 10px;">
                <h3>@yield('title')</h3>
                <div class="row m-0 p-0 col-sm-12" style="text-align: left;">
                    <form class="form-inline w-100">
                        <div class="form-row w-100">
                            {{-- <div class="form-group col-md-3">
                                <small>Board</small><br />
                                <select name="board" class="form-control form-control-sm w-100 mt-0">
                                    <option value="">Board</option>
                                    <option value="all">All</option>
                                    @foreach($boards as $board)
                                        <option value="{{$board->id}}" {{(isset($_GET['board']) && $_GET['board'] == $board->id ? 'selected="selected"' : '')}}>{{$board->name}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="form-group col-md-3">
                                <small>Section</small><br />
                                <select name="section" class="form-control form-control-sm w-100 mt-0">
                                    <option value="">Section</option>
                                    <option value="all">All</option>
                                    @foreach($sections as $section)
                                        <option value="{{$section->id}}" {{(isset($_GET['section']) && $_GET['section'] == $section->id ? 'selected="selected"' : '')}}>{{$section->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <small>Lead Status</small><br />
                                <select name="status" class="form-control form-control-sm w-100 mt-0">
                                    <option value="">Lead Status</option>
                                    <option value="all" {{(isset($_GET['status']) && $_GET['status'] == 'all' ? 'selected="selected"' : '')}}>All</option>
                                    <option value="Potential Lead" {{(isset($_GET['status']) && $_GET['status'] == 'Potential Lead' ? 'selected="selected"' : '')}}>Potential Lead</option>
                                    <option value="Attempted to Contact" {{(isset($_GET['status']) && $_GET['status'] == 'Attempted to Contact' ? 'selected="selected"' : '')}}>Attempted to Contact</option>
                                    <option value="Open Lead" {{(isset($_GET['status']) && $_GET['status'] == 'Open Lead' ? 'selected="selected"' : '')}}>Open Lead</option>
                                    <option value="Completed" {{(isset($_GET['status']) && $_GET['status'] == 'Completed' ? 'selected="selected"' : '')}}>Completed</option>
                                    <option value="Closed" {{(isset($_GET['status']) && $_GET['status'] == 'Closed' ? 'selected="selected"' : '')}}>Closed</option>
                                    <option value="Unqualified" {{(isset($_GET['status']) && $_GET['status'] == 'Unqualified' ? 'selected="selected"' : '')}}>Unqualified</option>
                                    <option value="Lead Sent to FA" {{(isset($_GET['status']) && $_GET['status'] == 'Lead Sent to FA' ? 'selected="selected"' : '')}}>Lead Sent to FA</option>
                                </select>
                            </div>
                            {{-- <div class="form-group col-md-2">
                                <small>From: date</small>
                                {{Form::date('f',old('f'),['class'=>'form-control form-control-sm col-sm-12 mt-0','style'=>'height: calc(1.8125rem + 2px);'])}}
                            </div>
                            <div class="form-group col-md-2">
                                <small>To: date</small>
                                {{Form::date('t',old('t'),['class'=>'form-control form-control-sm col-sm-12 mt-0','style'=>'height: calc(1.8125rem + 2px);'])}}
                            </div> --}}
                            {{-- <div class="form-group col-md-3 pt-0">
                                <small>Advisor on Record</small><br />
                                <select name="advisor" class="form-control form-control-sm w-100 mt-0">
                                    <option value="">Advisor on Record</option>
                                    <option value="all">All</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->first_name}} {{$user->last_name}}" {{(isset($_GET['advisor']) && $_GET['advisor'] == $user->first_name.' '.$user->last_name ? 'selected="selected"' : '')}}>{{$user->first_name}} {{$user->last_name}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            {{-- <div class="form-group col-md-3 pt-0">
                                <small>Assignee</small><br />
                                <select name="user" class="form-control form-control-sm w-100 mt-0">
                                    <option value="">Assignee</option>
                                    <option value="all">All</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->first_name}} {{$user->last_name}}" {{(isset($_GET['user']) && $_GET['user'] == $user->first_name.' '.$user->last_name ? 'selected="selected"' : '')}}>{{$user->first_name}} {{$user->last_name}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="form-group col-md-3 pt-0">
                                <small>&nbsp;</small><br />
                                <input type="text" name="q" value="{{(isset($_GET['q']) ? $_GET['q'] : '')}}" class="form-control form-control-sm w-100 mt-0" />
                            </div>
                            <div class="form-group col-md-3 pt-0">
                                <button type="submit" class="btn btn-sm btn-primary submit-btn ml-2 mt-2" style="width: 46%;">Search</button>&nbsp;
                                <a href="{{route('reports.myworkday')}}" class="btn btn-sm btn-outline-info submit-btn mt-2" style="width: 46%;">Clear</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="container-fluid container-content" style="height:calc(100% - 14rem);overflow: hidden;">
                <div class="table-responsive" style="height:100%;overflow: auto;">
                    <table class="table table-bordered table-sm table-hover" style="position: relative;">
                        <thead>
                        <tr>
                            {{-- <th>

                            </th>
                            <th>

                            </th> --}}
                            <th>
                                ID
                            </th>
                            <th nowrap style="width: 300px;">
                                Card Name
                            </th>
                            <th nowrap>
                                Create Date
                            </th>
                            <th>
                                Client
                            </th>
                            <th>
                                Created By
                            </th>
                            <th>
                                Section
                            </th>
                            <th>
                                Member
                            </th>
                            <th>
                                Contact
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Lead Type
                            </th>
                            <th>
                                Lead Origin
                            </th>
                            <th>
                                Director
                            </th>
                            <th>
                                Consultant
                            </th>
                            <th>
                                EB Admin
                            </th>
                            <th>
                                Lead Status
                            </th>
                        </tr>
                        </thead>
                        <tbody id="myworkday">
                        @forelse($cards as $card)
                            {{-- @if(isset($card->section->board)) --}}
                            <tr id="{{$card->id}}" data-id="{{$card->id}}" class="my-card {{($card->complete == '1' ? 'completed' : '')}}">
                                {{-- <td nowrap>
                                    @if(count($card["tasks"]) > 0) <a href="javascript:void(0)" onclick="showTasks({{$card->id}})" style="padding:0 7px;"><i class="fas fa-plus text-success"></i></a> @else <a href="javascript:void(0)" style="padding:0 7px;"><i class="fas fa-plus text-default"></i></a> @endif
                                </td>
                                <td nowrap>
                                    @if($card["complete"] == '1') <a href="javascript:void(0)" onclick="toggleCardStatus({{$card->id}})" style="padding:0 7px;" class="text-success" id="c{{$card->id}}t"><i class="fas fa-check"></i></a> @else <a href="javascript:void(0)" onclick="toggleCardStatus({{$card->id}})" style="padding:0 7px;" class="text-default" id="c{{$card->id}}t"><i class="fas fa-check"></i></a> @endif
                                </td> --}}
                                <td style="text-align: center;">{{$card->id}}</td>
                                <td nowrap>{{$card->name}}</td>
                                <td nowrap>{{date("Y-m-d",strtotime($card->created_at))}}</td>
                                <td nowrap>{{($card->client_name != null && $card->client_name != '' ? $card->client_name : '')}}</td>
                                <td nowrap>
                                    @foreach ($users as $user)
                                        @if ($user->id == $card->creator_id)
                                            {{$user->first_name.' '.$user->last_name}}
                                        @endif
                                    @endforeach
                                </td>
                                <td nowrap>
                                    @foreach ($sections as $section)
                                        @if ($section->id == $card->section_id)
                                            {{$section->name}}
                                        @endif
                                    @endforeach
                                </td>
                                <td nowrap>{{$card->member}}</td>
                                <td nowrap>{{$card->contact}}</td>
                                <td nowrap>{{$card->email}}</td>
                                <td nowrap>{{$card->lead_type}}</td>
                                <td nowrap>{{$card->origin_lead}}</td>
                                <td nowrap>{{$card->director}}</td>
                                <td nowrap>{{$card->consultant}}</td>
                                <td nowrap>{{$card->eb_admin}}</td>
                                <td nowrap>{{$card->lead_status}}</td>
                            </tr>
                            {{-- @endif --}}
                            @if(count($card["tasks"]) > 0)
                            <tr class="my-task tasks-{{$card->id}}" style="display: none;">
                                <td colspan="17" style="padding: 10px 0px !important;">
                                <table style="width: calc(100% - 2rem);margin-left: 2rem;">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Create Date</th>
                                        <th>Task Name</th>
                                        <th>Assignee</th>
                                        <th>Due Date</th>
                                        <th>Completed Date</th>
                                    </tr>
                                    @forelse($card["tasks"] as $card)
                                    <tr id="t{{$card->id}}" class="{{($card->due_date < \Carbon\Carbon::parse(now())->format('Y-m-d') && $card->status_id == 0 ? 'overdue' : ($card->status_id == 1 ? 'completed' : ''))}}">
                                        <td nowrap class="last">@if(count($card["subTasks"]) > 0) <a href="javascript:void(0)" onclick="showSubtasks({{$card->id}})" style="padding: 0 7px;"><i class="fas fa-plus text-success"></i></a> @else <a href="javascript:void(0)" style="padding: 0 7px;"><i class="fas fa-plus text-default"></i></a> @endif</td>
                                        <td nowrap class="last">@if($card["status_id"] == 1) <a href="javascript:void(0)" onclick="toggleTaskStatus({{$card->id}},{{$card->status_id}})" style="padding: 0 7px;" class="text-success" id="t{{$card->id}}t"><i class="fas fa-check"></i></a> @else <a href="javascript:void(0)" onclick="toggleTaskStatus({{$card->id}},{{$card->status_id}})" style="padding: 0 7px;" class="text-default" id="t{{$card->id}}t"><i class="fas fa-check"></i></a> @endif</td>
                                        <td nowrap>{{date("Y-m-d",strtotime($card->created_at))}}</td>
                                        <td>{{$card->name}}</td>
                                        <td>{{($card->assignee_name != null && $card->assignee_name != '' ? $card->assignee_name : '')}}</td>
                                        <td nowrap>{{$card->due_date}}</td>
                                        <td nowrap id="t{{$card["id"]}}-completed_date">{{($card->completed_date != '' ? \Carbon\Carbon::parse($card->completed_date)->format('Y-m-d') : '')}}</td>
                                    </tr>
                                        @if(count($card["subTasks"]) > 0)
                                            <tr class="my-subtask subtasks-{{$card->id}}" style="display: none;">
                                                <td colspan="17" style="padding: 10px 0px !important; border-bottom: 0px !important;border-left: 0px !important;border-right: 0px !important;">
                                                    <table style="width: calc(100% - 2rem);margin-left: 2rem;">
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th>Create Date</th>
                                                            <th>Task Name</th>
                                                            <th>Assignee</th>
                                                            <th>Due Date</th>
                                                            <th>Completed Date</th>
                                                        </tr>
                                                        @forelse($card["subTasks"] as $card)
                                                            <tr id="st{{$card->id}}" class="{{($card->due_date < \Carbon\Carbon::parse(now())->format('Y-m-d') && $card->status_id == 0 ? 'overdue' : ($card->status_id == 1 ? 'completed' : ''))}}">
                                                                <td nowrap class="last">@if(count($card["subTasks"]) > 0) <a href="javascript:void(0)" onclick="showSubtasks({{$card->id}})" style="padding: 0 7px;"><i class="fas fa-plus text-success"></i></a> @else <a href="javascript:void(0)" style="padding: 0 7px;"><i class="fas fa-plus text-default"></i></a> @endif</td>
                                                                <td nowrap class="last">@if($card["status_id"] == 1) <a href="javascript:void(0)" onclick="toggleTaskStatus({{$card->id}},{{$card->status_id}})" style="padding: 0 7px;" class="text-success" id="t{{$card->id}}t"><i class="fas fa-check"></i></a> @else <a href="javascript:void(0)" onclick="toggleTaskStatus({{$card->id}},{{$card->status_id}})" style="padding: 0 7px;" class="text-default" id="t{{$card->id}}t"><i class="fas fa-check"></i></a> @endif</td>
                                                                <td nowrap>{{date("Y-m-d",strtotime($card->created_at))}}</td>
                                                                <td>{{$card->name}}</td>
                                                                <td>{{($card->assignee_name != null && $card->assignee_name != '' ? $card->assignee_name : '')}}</td>
                                                                <td nowrap>{{$card->due_date}}</td>
                                                                <td nowrap>{{($card->completed_date != '' ? \Carbon\Carbon::parse($card->completed_date)->format('Y-m-d') : '')}}</td>
                                                            </tr>
                                                        @empty
                                                        @endforelse
                                                    </table>
                                                </td>

                                            </tr>
                                        @endif
                                    @empty
                                    @endforelse
                                </table>
                                </td>

                            </tr>
                            @endif


                        @empty
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalShowMyWorkDayItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 618px;max-width: 618px;">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 2px solid #06496f;padding:27px 27px 0px 27px;display: block !important;">
                    <small style="color: #06496f;opacity: 0.5;" class="section_name"></small>
                    <h5 class="modal-title card_name"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0 mb-0">
                    <input type="hidden" class="card_id">
                    <div class="col-md-12 pb-0 pt-0">
                        <small>Case Summary</small>
                        <span class="case_summary w-100 d-block"></span>
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>Client</small>
                        <span class="client w-100 d-block"></span>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Insurer</small>
                            <span class="insurer w-100 d-block"></span>
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Policy</small>
                            <span class="policy w-100 d-block"></span>
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Dependency</small>
                            <span class="dependency w-100 d-block"></span>
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Advisor on record</small>
                            <span class="advisor w-100 d-block"></span>
                        </div>
                        <div class="col-md-8 mb-0 mt-0 pt-0">
                            <small>Assign Team members</small>
                            <span class="team w-100 d-block"></span>
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-6 mb-0 mt-0 pt-0">
                            <small>Upfront Revenue</small>
                            <span class="upfront_revenue w-100 d-block"></span>
                        </div>
                        <div class="col-md-6 mb-0 mt-0 pt-0">
                            <small>Ongoing Revenue</small>
                            <span class="ongoing_revenue w-100 d-block"></span>
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Due Date</small>
                            <span class="due_date w-100 d-block"></span>
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Status</small>
                            <span class="status w-100 d-block"></span>
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Priority</small>
                            <span class="priority w-100 d-block"></span>
                        </div>
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>Feedback Notes From Client</small>
                        <span class="description w-100 d-block"></span>
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>File Prep</small>
                        <span class="description2 w-100 d-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>&nbsp;
                    <button class="btn btn-primary" onclick="editMyWorkDayItem()">Edit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditMyWorkDayItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 618px;max-width: 618px;">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 2px solid #06496f;padding:27px 27px 0px 27px;display: block !important;">
                    <small style="color: #06496f;opacity: 0.5;" class="section_name"></small>
                    <input type="text" class="form-control form-control-sm card_name mt-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0 mb-0">
                    <input type="hidden" class="card_id">
                    <div class="col-md-12 pb-0 pt-0">
                        <small>Case Summary</small>
                        <input type="text" class="form-control form-control-sm case_summary w-100">
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>Client</small>
                        <select class="form-control form-control-sm client w-100"></select>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Insurer</small>
                            <input type="text" class="form-control form-control-sm insurer w-100 d-block">
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Policy</small>
                            <input type="text" class="form-control form-control-sm policy w-100 d-block">
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Dependency</small>
                            <select class="form-control form-control-sm dependency w-100"></select>
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Advisor on record</small>
                            <select class="form-control form-control-sm advisor w-100"></select>
                        </div>
                        <div class="col-md-8 mb-0 mt-0 pt-0">
                            <small>Assign Team members</small>
                            <select multiple class="chosen-select form-control form-control-sm team w-100"></select>
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-6 mb-0 mt-0 pt-0">
                            <small>Upfront Revenue</small>
                            <input type="number" class="form-control form-control-sm upfront_revenue w-100 d-block">
                        </div>
                        <div class="col-md-6 mb-0 mt-0 pt-0">
                            <small>Ongoing Revenue</small>
                            <input type="number" class="form-control form-control-sm ongoing_revenue w-100 d-block">
                        </div>
                    </div>
                    <div class="row col-md-12 pb-0 mt-0">
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Due Date</small>
                            <input type="date" class="form-control form-control-sm due_date w-100 d-block">
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Status</small>
                            <select class="form-control form-control-sm status w-100"></select>
                        </div>
                        <div class="col-md-4 mb-0 mt-0 pt-0">
                            <small>Priority</small>
                            <select class="form-control form-control-sm priority w-100"></select>
                        </div>
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>Feedback Notes From Client</small>
                        <textarea rows="7" class="description w-100 d-block"></textarea>
                    </div>
                    <div class="col-md-12 pb-0">
                        <small>File Prep</small>
                        <textarea rows="7" class="description2 w-100 d-block"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>&nbsp;
                    <button class="btn btn-success" onclick="saveMyWorkDayItem()">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showCommentsModal" tabindex="-1" role="dialog" aria-labelledby="showCommentsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="commentsModalTitle">All Comments</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="border:solid; background-color:rgb(190 201 218 / 33%); margin-left:1%; margin-right:1%; border-radius:5%;">
              <input type="text" hidden id="discussionID" name="discussionID">
              <div id="comments" name="comments">
                {{-- <strong style="float: left; background-colour"></strong> --}}
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="newComment()">Add New</button>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="addCommentsModal" tabindex="-1" role="dialog" aria-labelledby="addCommentsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addCommentsModalTitle">All Comments</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="border:solid; background-color:rgb(190 201 218 / 33%); margin-left:1%; margin-right:1%; border-radius:5%;">
              <input type="text" hidden id="discussionID" name="discussionID">
              <div id="comments" name="comments">
                
              </div>
            </div>
            <div class="modal-footer">
                <div class="input-group">
                    {{-- <form action="{{ route('discusson.store') }}" method="post"> --}}
                        <textarea id="comment_text" name="comment_text" class="form-control type_msg" placeholder="Type your message..."></textarea>
                        <div class="input-group-append">
                            {{-- <span class="input-group-text send_btn"><i class="fas fa-arrow-alt-circle-right"></i></span> --}}
                            <button onclick="saveComment()" type="button" class="btn btn-primary" style="margin: 1% !important"><i class="fas fa-arrow-alt-circle-right"></i></button>
                        </div>
                    {{-- </form> --}}
                </div>
              {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Submit</button> --}}
            </div>
          </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .text-default{
            color: #777777 !important;
        }
        #myworkday tr{
            cursor: pointer;
        }

        #myworkday .my-card.overdue{
            /* background: #e99b9a; */
            background: #ff9999b3;
        }

        .completed{
            background: #a0e99a;
        }

        #myworkday .my-task tr.overdue{
            background: rgba(233,155,154,0.7);
        }

        #myworkday .my-subtask tr.overdue{
            background: rgba(233,155,154,0.4);
        }

        table thead th{
            position: sticky;
            top: -1px;
            background: #FFF;
            outline: 1px solid #e9ecef;
            border:none;
            outline-offset: -1px;
            vertical-align: middle !important;
        }

        #modalShowMyWorkDayItem span{
            color:#06496f;
        }

        #modalEditMyWorkDayItem .modal-body input,#modalEditMyWorkDayItem .modal-body select{
            margin: 0px !important;
        }

        #modalEditMyWorkDayItem .modal-body .chosen-choices{
            padding: 0px 5px !important;
        }
        #modalEditMyWorkDayItem .modal-body input[type=date]{
            padding:.25rem .5rem !important;
        }

        tr.my-task:hover,tr.my-subtask:hover{
            background-color: transparent !important;
        }

        .logged-in-comment{
            /* color: #777777 !important; */
            /* border: solid; */
            /* border-radius: 40%; */
            width: 60%;
            float: left;
            display:block;
            text-align: center;
            margin: 2%;
            /* margin-top: auto; */
            /* margin-bottom: auto; */
            /* margin-left: 10px; */
            border-radius: 25px;
            background-color: #82ccdd;
            padding: 10px;
            position: relative;
        }
        .other-comment{
            /* color: #218608 !important; */
            /* border: solid; */
            /* border-radius: 40%; */
            width: 60%;
            float: right;
            display: block;
            text-align: center;
            margin: 2%;
            border-radius: 25px;
            background-color: #78e08f;
            padding: 10px;
            position: relative;
        }

        .type_msg{
			background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color:white !important;
			height: 60px !important;
			overflow-y: auto;
            width: 70% !important;
            margin-right: 1%;
		}
			.type_msg:focus{
		     box-shadow:none !important;
           outline:0px !important;
		}

        .send_btn{
	        border-radius: 0  !important;
	        background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color: white !important;
			cursor: pointer;
		}

        .arrow-logged{
	        color: #82ccdd;
            left: -17% !important;
            height: 55px !important;
            width: 60px !important;
		}

        .arrow-other{
	        color: #78e08f;
            left: 7% !important;
            height: 55px !important;
            width: 60px !important;
		}

    </style>
@endsection
@section('extra-js')
    <script>

            $('#myworkday').on('click','.my-card td',function (event) {

                if($(this).is(":nth-child(1)") || $(this).is(":nth-child(2)") || $(this).is(":nth-child(14)")){
                    console.log('td');
                } else {
                    let id = $(this).closest('tr').attr('data-id');
                    showWorkDayItem(id);
                }
            })

        function showWorkDayItem(id){
            var id = id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/reports/my-work-day/' + id + '/details',
                type:"GET",
                dataType:"json",
                success:function(data){
                    $("#modalShowMyWorkDayItem").modal('show');
                    $('#modalShowMyWorkDayItem').find('.card_id').val(data.card.id);
                    $('#modalShowMyWorkDayItem').find('.section_name').html(data.section_name);
                    $('#modalShowMyWorkDayItem').find('.card_name').html(data.card.name);
                    $('#modalShowMyWorkDayItem').find('.case_summary').html(data.card.summary_description);
                    if(data.card.client_name === null) {
                        $('#modalShowMyWorkDayItem').find('.client').html('None');
                    } else {
                        $('#modalShowMyWorkDayItem').find('.client').html(data.card.client_name);
                    }
                    $('#modalShowMyWorkDayItem').find('.insurer').html(data.card.insurer);
                    $('#modalShowMyWorkDayItem').find('.policy').html(data.card.policy);
                    $('#modalShowMyWorkDayItem').find('.dependency').html(data.dependency);
                    $('#modalShowMyWorkDayItem').find('.advisor').html(data.card.assignee_name);
                    $('#modalShowMyWorkDayItem').find('.team').html(data.card.team_names);
                    $('#modalShowMyWorkDayItem').find('.upfront_revenue').html(data.card.upfront_revenue);
                    $('#modalShowMyWorkDayItem').find('.ongoing_revenue').html(data.card.ongoing_revenue);
                    $('#modalShowMyWorkDayItem').find('.due_date').html(data.card.due_date);
                    $('#modalShowMyWorkDayItem').find('.status').html(data.statuss);
                    $('#modalShowMyWorkDayItem').find('.priority').html(data.priority);
                    $('#modalShowMyWorkDayItem').find('.description').html(data.card.description);
                    $('#modalShowMyWorkDayItem').find('.description2').html(data.card.description2);
                }
            });
        }
        /*function addRowHandlers() {
            var table = document.getElementById("myworkday");
            var rows = $("table > tbody > tr:first > td");

            for (i = 0; i <= rows.length; i++) {
                var currentRow = table.rows[i];
                var createClickHandler =
                    function (row) {
                        return function () {
                            var cell = row.getElementsByTagName("td")[0];
                            var id = row.getAttribute('data-id');

                            if(row.classList.contains("my-card")) {
                                if(cell) {
                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    $.ajax({
                                        url: '/reports/my-work-day/' + id + '/details',
                                        type: "GET",
                                        dataType: "json",
                                        success: function (data) {
                                            $("#modalShowMyWorkDayItem").modal('show');
                                            $('#modalShowMyWorkDayItem').find('.card_id').val(data.card.id);
                                            $('#modalShowMyWorkDayItem').find('.section_name').html(data.section_name);
                                            $('#modalShowMyWorkDayItem').find('.card_name').html(data.card.name);
                                            $('#modalShowMyWorkDayItem').find('.case_summary').html(data.card.summary_description);
                                            if (data.card.client_name === null) {
                                                $('#modalShowMyWorkDayItem').find('.client').html('None');
                                            } else {
                                                $('#modalShowMyWorkDayItem').find('.client').html(data.card.client_name);
                                            }
                                            $('#modalShowMyWorkDayItem').find('.insurer').html(data.card.insurer);
                                            $('#modalShowMyWorkDayItem').find('.policy').html(data.card.policy);
                                            $('#modalShowMyWorkDayItem').find('.dependency').html(data.dependency);
                                            $('#modalShowMyWorkDayItem').find('.advisor').html(data.card.assignee_name);
                                            $('#modalShowMyWorkDayItem').find('.team').html(data.card.team_names);
                                            $('#modalShowMyWorkDayItem').find('.upfront_revenue').html(data.card.upfront_revenue);
                                            $('#modalShowMyWorkDayItem').find('.ongoing_revenue').html(data.card.ongoing_revenue);
                                            $('#modalShowMyWorkDayItem').find('.due_date').html(data.card.due_date);
                                            $('#modalShowMyWorkDayItem').find('.status').html(data.statuss);
                                            $('#modalShowMyWorkDayItem').find('.priority').html(data.priority);
                                            $('#modalShowMyWorkDayItem').find('.description').html(data.card.description);
                                        }
                                    });
                                }
                            }
                        };
                    };
                currentRow.onclick = createClickHandler(currentRow);
            }
        }

        window.onload = addRowHandlers();*/

        $(".chosen-select").chosen();

        $('#modalShowMyWorkDayItem').on('hidden.bs.modal', function () {
            $('#modalShowMyWorkDayItem').find('.card_id').val('');
            $('#modalShowMyWorkDayItem').find('.section_name').html('');
            $('#modalShowMyWorkDayItem').find('.card_name').html('');
            $('#modalShowMyWorkDayItem').find('.case_summary').html('');
                $('#modalShowMyWorkDayItem').find('.client').html('');
            $('#modalShowMyWorkDayItem').find('.insurer').html('');
            $('#modalShowMyWorkDayItem').find('.policy').html('');
            $('#modalShowMyWorkDayItem').find('.dependency').html('');
            $('#modalShowMyWorkDayItem').find('.advisor').html('');
            $('#modalShowMyWorkDayItem').find('.team').html('');
            $('#modalShowMyWorkDayItem').find('.upfront_revenue').html('');
            $('#modalShowMyWorkDayItem').find('.ongoing_revenue').html('');
            $('#modalShowMyWorkDayItem').find('.due_date').html('');
            $('#modalShowMyWorkDayItem').find('.status').html('');
            $('#modalShowMyWorkDayItem').find('.priority').html('');
            $('#modalShowMyWorkDayItem').find('.description').html('');
        })

        $('#modalEditMyWorkDayItem').on('hidden.bs.modal', function () {
            $('#modalEditMyWorkDayItem').find('.card_id').val('');
            $('#modalEditMyWorkDayItem').find('.section_name').html('');
            $('#modalEditMyWorkDayItem').find('.card_name').val('');
            $('#modalEditMyWorkDayItem').find('.case_summary').val('');
            $("#modalEditMyWorkDayItem").find('.status').empty();
            $("#modalEditMyWorkDayItem").find('.priority').empty();
            $("#modalEditMyWorkDayItem").find('.dependency').empty();
            $("#modalEditMyWorkDayItem").find('.advisor').empty();
            $('#modalEditMyWorkDayItem').find('.client').empty();
            $('#modalEditMyWorkDayItem').find(".team").empty();
            $('#modalEditMyWorkDayItem').find('.insurer').val('');
            $('#modalEditMyWorkDayItem').find('.policy').val('');
            $('#modalEditMyWorkDayItem').find('.dependency').val('');
            $('#modalEditMyWorkDayItem').find('.advisor').val('');
            $('#modalEditMyWorkDayItem').find('.upfront_revenue').val('');
            $('#modalEditMyWorkDayItem').find('.ongoing_revenue').val('');
            $('#modalEditMyWorkDayItem').find('.due_date').val('');
            $('#modalEditMyWorkDayItem').find('.status').val('');
            $('#modalEditMyWorkDayItem').find('.priority').val('');
            $('#modalEditMyWorkDayItem').find('.description').val('');
        });

        function saveMyWorkDayItem() {
            let id = $('#modalEditMyWorkDayItem').find('.card_id').val();
            let card_name = $('#modalEditMyWorkDayItem').find('.card_name').val();
            let summary_description = $('#modalEditMyWorkDayItem').find('.case_summary').val();
            let status = $('#modalEditMyWorkDayItem').find('.status').val();
            let priority = $('#modalEditMyWorkDayItem').find('.priority').val();
            let policy = $('#modalEditMyWorkDayItem').find('.policy').val();
            let insurer = $('#modalEditMyWorkDayItem').find('.insurer').val();
            let dependency = $('#modalEditMyWorkDayItem').find('.dependency').val();
            let upfront = $('#modalEditMyWorkDayItem').find('.upfront_revenue').val();
            let ongoing = $('#modalEditMyWorkDayItem').find('.ongoing_revenue').val();
            let due_date = $('#modalEditMyWorkDayItem').find('.due_date').val();
            let description = $('#modalEditMyWorkDayItem').find('.description').val();
            let description2 = $('#modalEditMyWorkDayItem').find('.description2').val();
            let client = $('#modalEditMyWorkDayItem').find('.client').val();
            let advisor = $('#modalEditMyWorkDayItem').find('.advisor').val();
            let team = $('#modalEditMyWorkDayItem').find('.team').val();


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/reports/my-work-day/' + id + '/save',
                data: {id:id,client:client,advisor:advisor,team:team,description:description,description2:description2,due_date:due_date,ongoing:ongoing,upfront:upfront,dependency:dependency,insurer:insurer,policy:policy,priority:priority,status:status,card_name:card_name,summary_description:summary_description},
                success: function( data ) {
                    let row = '';
                    $("#modalEditMyWorkDayItem").modal('hide');

                    let task_length = '';
                    console.log(data.tasks.length);
                    if(data.tasks.length > 0){
                        task_length = '<a href="javascript:void(0)" onclick="showTasks(' + data.id + ')" style="padding:0 7px;"><i class="fas fa-plus text-success"></i></a>';
                    } else {
                        task_length = '<a href="javascript:void(0)" style="padding:0 7px;"><i class="fas fa-plus text-default"></i></a>';
                    }

                    let complete = '';
                    if(data.complete === "1"){
                        complete = '<a href="javascript:void(0)" onclick="toggleCardStatus(data.id)" style="padding:0 7px;" class="text-success" id="c' + data.id + 't"><i class="fas fa-check"></i></a>';
                    } else {
                        complete = '<a href="javascript:void(0)" onclick="toggleCardStatus(' + data.id + ')" style="padding:0 7px;" class="text-default" id="c' + data.id + 't"><i class="fas fa-check"></i></a>';
                    }

                    let overdue = '';
                    var date1= new Date(data.due_date);
                    var date2= new Date();

                    if(date1 < date2 && data.complete === "0"){
                        overdue = 'overdue';
                    }

                    row = '<tr id="' + data.id + '" data-id="' + data.id + '" class="my-card ' + overdue + ' ' + (data.complete === '1' ? 'completed' : '') + '">' +
                        '<td nowrap>' + task_length + '</td>' + '' +
                        '<td nowrap>' + complete + '</td>' +
                    '<td>' + data.id + '</td>' +
                    '<td>' + data.name + '</td>' +
                    '<td>' + data.created + '</td>' +
                    '<td>' + data.client + '</td>' +
                    '<td>' + data.client_id +  '</td>' +
                    '<td>' + data.summary + '</td>' +
                    '<td>' + data.description + '</td>' +
                    '<td>' + data.description2 + '</td>' +
                    '<td>' + data.advisor + '</td>' +
                    '<td>' + data.team_names + '</td>' +
                    '<td>' + data.due_date + '</td>' +
                    '<td>' + data.completed_date + '</td>' +
                    '<td></td>' +
                    '<td>' + data.board + '</td>' +
                    '<td>' + data.section + '</td></tr>';

                    $(document).find('#' + id).replaceWith(row);

                        toastr.success('<strong>Success!</strong> Card was updated successfully.');

                        toastr.options.timeOut = 1000;

                }
            });


        }

        function editMyWorkDayItem() {
            let id = $('#modalShowMyWorkDayItem').find('.card_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/reports/my-work-day/' + id + '/details',
                type:"GET",
                dataType:"json",
                success:function(data){
                    $("#modalShowMyWorkDayItem").modal('hide');
                    $("#modalEditMyWorkDayItem").modal('show');
                    $('#modalEditMyWorkDayItem').find('.card_id').val(data.card.id);
                    $('#modalEditMyWorkDayItem').find('.section_name').html(data.section_name);
                    $('#modalEditMyWorkDayItem').find('.card_name').val(data.card.name);
                    $('#modalEditMyWorkDayItem').find('.case_summary').val(data.card.summary_description);
                    $.each(data.progress_status, function(key, value) {
                        $("#modalEditMyWorkDayItem").find('.status').append($("<option></option>").attr("value",key).text(value));
                    });
                    $.each(data.priority_status, function(key, value) {
                        $("#modalEditMyWorkDayItem").find('.priority').append($("<option></option>").attr("value",key).text(value));
                    });
                    $.each(data.office_clients, function(key, value) {
                        $("#modalEditMyWorkDayItem").find('.client').append($("<option></option>").attr("value",value).text(value));
                    });
                    $.each(data.cards, function(key, value) {
                        $("#modalEditMyWorkDayItem").find('.dependency').append($("<option></option>").attr("value",key).text(value));
                    });
                    $.each(data.office_users, function(key, value) {
                        $("#modalEditMyWorkDayItem").find('.team').append($("<option></option>").attr("value",value.full_name).text(value.full_name));
                        $("#modalEditMyWorkDayItem").find('.advisor').append($("<option></option>").attr("value",value.full_name).text(value.full_name));
                    });
                    if(data.card.client_name === null) {
                        $('#modalEditMyWorkDayItem').find('.client').val('None');
                    } else {
                        $('#modalEditMyWorkDayItem').find('.client').val(data.card.client_name);
                    }
                    if(data.card.team_names === null) {

                    } else {
                        $.each(data.card.team_names.replace(', ', ',').split(","), function (i, e) {
                            $('#modalEditMyWorkDayItem').find(".team option[value='" + e + "']").prop("selected", true);
                            $('#modalEditMyWorkDayItem').find(".chosen-select").trigger("chosen:updated");
                        });
                    }
                    $('#modalEditMyWorkDayItem').find('.insurer').val(data.card.insurer);
                    $('#modalEditMyWorkDayItem').find('.policy').val(data.card.policy);
                    $('#modalEditMyWorkDayItem').find('.dependency').val(data.card.dependency_id);
                    $('#modalEditMyWorkDayItem').find('.advisor').val(data.card.assignee_name);
                    /*$('#modalEditMyWorkDayItem').find('.team').val(data.card.team_names);*/
                    $('#modalEditMyWorkDayItem').find('.upfront_revenue').val(data.card.upfront_revenue);
                    $('#modalEditMyWorkDayItem').find('.ongoing_revenue').val(data.card.ongoing_revenue);
                    $('#modalEditMyWorkDayItem').find('.due_date').val(data.card.due_date);
                    $('#modalEditMyWorkDayItem').find('.status').val(data.card.status_id);
                    $('#modalEditMyWorkDayItem').find('.priority').val(data.card.priority_id);
                    $('#modalEditMyWorkDayItem').find('.description').val(data.card.description);
                    $('#modalEditMyWorkDayItem').find('.description2').val(data.card.description2);
                }
            });
        }

        function showTasks(id){
            if($('.tasks-' + id).is(':visible')) {
                $('.tasks-' + id).hide();
            } else {
                $('.tasks-' + id).show();
            }
        }

        function showComments(id){
            $('#showCommentsModal').modal('hide');
            $('#addCommentsModal').modal('hide');
            $('#showCommentsModal').modal('show');

            $.ajax({
                url: '/reports/my-work-day/' + id + '/comments',
                type:"GET",
                dataType:"json",
                success:function(data){
                    $("#showCommentsModal").modal('hide');
                    $("#showCommentsModal").modal('show');
                    $("#showCommentsModal").find('#comments').empty();
                    $('#showCommentsModal').find('#commentsModalTitle').text('All comments for '+data.card.name);
                    
                    data.discussions.forEach(element => {
                        $('#showCommentsModal').find('#discussionID').val(element.card_id);
                        // console.log(element.message);
                        data.users.forEach(user => {
                            if (user.id == element.user_id) {
                                if (element.user_id == data.logged_in_id) {
                                    // console.log('test');
                                    $("#showCommentsModal").find('#comments').append("<div class='logged-in-comment'><i class='fas fa-location-arrow arrow-logged'></i><strong>"+user.full_name+"</strong><p>"+element.message+"</p><small>"+element.created_at+"</small></div>");
                                    // $("#showCommentsModal").find('#comments').append("<span>"+element.message+"</span><br><br>");
                                } else {
                                    $("#showCommentsModal").find('#comments').append("<div class='other-comment'><i class='fas fa-location-arrow arrow-other'></i><strong>"+user.full_name+"</strong><p>"+element.message+"</p><small>"+element.created_at+"</small></div>");
                                    // $("#showCommentsModal").find('#comments').append("<span>"+element.message+"</span><br><br>");
                                }
                                // $("#showCommentsModal").find('#comments').append("<strong>"+user.full_name+"</strong><br>");
                            }
                        });
                        // $("#showCommentsModal").find('#comments').append("<span>"+element.message+"</span><br><br>");
                    });
                }
            });
        }

        function newComment(){
            var cardID =  $("#showCommentsModal").find('#discussionID').val();
            $('#showCommentsModal').modal('hide');
            $('#addCommentsModal').modal('hide');
            $('#addCommentsModal').modal('show');

            $.ajax({
                url: '/reports/my-work-day/' + cardID + '/comments',
                type:"GET",
                dataType:"json",
                success:function(data){
                    $("#addCommentsModal").modal('hide');
                    $("#addCommentsModal").modal('show');
                    $("#addCommentsModal").find('#comments').empty();
                    $('#addCommentsModal').find('#addCommentsModalTitle').text('All comments for '+data.card.name);
                    
                    data.discussions.forEach(element => {
                        $('#addCommentsModal').find('#discussionID').val(element.card_id);
                        data.users.forEach(user => {
                            if (user.id == element.user_id) {
                                if (element.user_id == data.logged_in_id) {
                                    $("#addCommentsModal").find('#comments').append("<div class='logged-in-comment'><strong>"+user.full_name+"</strong><br><p>"+element.message+"</p><small>"+element.created_at+"</small></div>");
                                } else {
                                    $("#addCommentsModal").find('#comments').append("<div class='other-comment'><strong>"+user.full_name+"</strong><br><p>"+element.message+"</p><small>"+element.created_at+"</small></div>");
                                }
                            }
                        });
                    });
                }
            });
        }

        function saveComment(){

            var card_id = $('#addCommentsModal').find('#discussionID').val();
            var message = $('#addCommentsModal').find('#comment_text').val();

            // console.log(message);

            // axios.post('/discussion', {
            // card_id: card_id,
            // message: this.discussion_message
            // }).then(response => {
            // this.discussion_message = "";
            // this.discussions.push(response.data.discussion);
            // toastr.success('<strong>Success!</strong> Discussion successfully saved.');

            // $("#addCommentsModal").modal('hide');
            // newComment();

            // toastr.options.timeOut = 1000;
            // }).catch(error => console.log(error.response));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/discussion',
                data: {
                    card_id: card_id,
                    message: message
                },
                success: function( data ) {
                    // $("#addCommentsModal").modal('hide');
                    showComments(card_id);
                }
            });
        }

        function showSubtasks(id){
            if($('.subtasks-' + id).is(':visible')) {
                $('.subtasks-' + id).hide();
            } else {
                $('.subtasks-' + id).show();
            }
        }

        function toggleCardStatus(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/reports/my-work-day/card/update_status/' + id,
                data: {currentstatus:status},
                success: function( data ) {
                    //$('#'+data.card.id).remove();

                    var today = new Date();
                    var dueDate = new Date(data.card.due_date);
                    console.log(today);
                    console.log(dueDate);

                    if(today > dueDate && data.card.complete === 0){
                        $('#'+data.card.id).addClass('overdue');
                        $('#'+data.card.id).removeClass('completed');
                    }

                    if(data.card.complete === 1){
                        $('#'+data.card.id).addClass('completed');
                        $('#'+data.card.id).removeClass('overdue');
                    }

                    if($('#c' + data.card.id + 't').is(':visible')){
                        $('#c' + data.card.id + 't').toggleClass('text-success');
                        $('#c' + data.card.id + 't').toggleClass('text-default');
                        $('#c' + data.card.id + '-completed_date').html(data.completed_date);
                    }
                    toastr.success('<strong>Success!</strong> Task was updated successfully.');

                    toastr.options.timeOut = 1000;

                }
            });
        }

        function toggleTaskStatus(id,status) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/reports/my-work-day/task/update_status/' + id,
                data: {currentstatus:status},
                success: function( data ) {
                    let cd = data.completed_date;

                    var today = new Date();
                    var dueDate = new Date(data.task.due_date);
                    console.log(today);
                    console.log(dueDate);

                    if(today > dueDate && data.task.status_id === 0){
                        if(data.task.parent_id > 0) {
                            $('#st' + data.task.id).addClass('overdue');
                            $('#st' + data.task.id).removeClass('completed');
                        } else {
                            $('#t' + data.task.id).addClass('overdue');
                            $('#t' + data.task.id).removeClass('completed');
                        }
                    }

                    if(data.task.status_id === 1){
                        if(data.task.parent_id > 0) {
                            $('#st' + data.task.id).addClass('completed');
                            $('#st' + data.task.id).removeClass('overdue');
                        } else {
                            $('#t' + data.task.id).addClass('completed');
                            $('#t' + data.task.id).removeClass('overdue');
                        }
                    }

                    if($('#t' + data.task.id + 't').is(':visible')){
                        $('#t' + data.task.id + 't').toggleClass('text-success');
                        $('#t' + data.task.id + 't').toggleClass('text-default');
                        $('#t' + data.task.id + '-completed_date').html(cd);
                    }

                    if($('#st' + data.task.id + 't').is(':visible')){
                        $('#st' + data.task.id + 't').toggleClass('text-success');
                        $('#st' + data.task.id + 't').toggleClass('text-default');
                        $('#st' + data.task.id + '-completed_date').html(data.completed_date);
                    }
                    toastr.success('<strong>Success!</strong> Task was updated successfully.');

                    toastr.options.timeOut = 1000;

                }
            });
        }
    </script>
@endsection