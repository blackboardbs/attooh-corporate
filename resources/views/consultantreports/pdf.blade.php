<table class="table table-bordered table-sm table-hover overflow: scroll;">
    <thead>
        <tr>
            <th>Client Name</th>
            @foreach ($fields as $field)
                <th>{{isset($field['name']) ? $field['name'] : $field}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($clients as $client)
            @foreach ($fund_consultants as $consultant)
                @if (strtolower($client['consultant']) == strtolower($consultant))
                    @php
                        $max = count($client['data']);
                    @endphp
                    <tr>
                        <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["client_name"] ) && $client["client_name"] != ' ' && $client["client_name"] != null ? $client["client_name"]  : $client["company"])}}</a></td>
                        @for ($i = 0; $i < $max; $i++)
                            <td>{{$client['data'][$i]}}</td>
                        @endfor
                    </tr>
                @endif
            @endforeach
        @endforeach
    </tbody>
</table>