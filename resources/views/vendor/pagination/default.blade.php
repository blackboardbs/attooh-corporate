@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->url(1) }}" rel="prev"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></a></li>
            <li class="disabled"><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fas fa-chevron-left"></i></a></li>
        @else
            <li><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->url(1) }}" rel="prev"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></a></li>
            <li><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fas fa-chevron-left"></i></a></li>
        @endif

        {{-- Pagination Elements --}}
        {{-- @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach --}}

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fas fa-chevron-right"></i></a></li>
            <li><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->url($paginator->lastPage()) }}" rel="next"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a></li>
        @else
            <li class="disabled"><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fas fa-chevron-right"></i></a></li>
            <li class="disabled"><a class="btn btn-secondary btn-sm" style="margin: 5px;" href="{{ $paginator->url($paginator->lastPage()) }}" rel="next"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a></li>
        @endif
    </ul>
@endif
