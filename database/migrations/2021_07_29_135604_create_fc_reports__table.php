<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFcReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fc_reports_', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('process_id');
            $table->integer('branch_id');
            $table->integer('fc_id');
            $table->integer('user_id')->unsigned();
            $table->date('month')->nullable();
            /*$table->integer('activity_id')->unsigned();*/
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fc_reports_');
    }
}
