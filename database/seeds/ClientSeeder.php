<?php

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\Client;
use App\ClientComment;
use App\FormInputDate;
use App\FormInputDateData;
use App\FormInputDropdownData;
use App\FormInputTextData;
use App\FormInputBooleanData;
use App\Step;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;
use Carbon\Carbon;

class ClientSeeder extends Seeder
{
    public function __construct() {
        //
    }

    public function run()
    {
        /*$this->getClientFiles('Gaukes_Moster_Data_New.csv', 45);
        $this->getClientFiles('Philip_Roesch_Data_New.csv', 65);
        $this->getClientFiles('Rayno_van_Vuuren_Data_New.csv', 47);
        $this->getClientFiles('Stian_de_Witt_Data_New.csv', 43);*/
        // $this->getClientFiles('Duane_van_Niekerk_Data_New.csv', 60);
        // $this->getClientFiles('attooh_data_new.csv', 526);
        // $this->getClientFiles('data_new_new.csv', 458);
        $this->getClientFiles('individual_new_new.csv', 310);
        // $this->getClientFiles('attooh_test.csv', 2);
    }

    public function getClientFiles($fileName, $office_id)
    {
        $csv = Reader::createFromPath(database_path('/data/'.$fileName, 'r'));
        $csv->setDelimiter(',');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record_key => $record) {
            $client = new Client;
            $client->first_name = isset($record['first_name']) && $record['first_name'] != '' && $record['first_name'] != 'NULL' ? ($record['first_name']) : NULL;
            $client->last_name = isset($record['last_name']) && $record['last_name'] != '' && $record['last_name'] != 'NULL' ? ($record['last_name']) : NULL;
            // $client->company = isset($record['client_name']) && $record['client_name'] != '' && $record['client_name'] != 'NULL' ? ($record['client_name']) : NULL;
            $client->email = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? ($record['email']) : NULL;
            // $client->contact = isset($record['contact_number']) && $record['contact_number'] != '' && $record['contact_number'] != 'NULL' ? ($record['contact_number']) : NULL;
            // $client->contact_office = isset($record['contact_number']) && $record['contact_number'] != '' && $record['contact_number'] != 'NULL' ? ($record['contact_number']) : NULL;
            $client->introducer_id = 1;
            $client->office_id = 1;
            $client->process_id = 35;
            $client->step_id = 188;
            $client->is_progressing = 1;
            $client->needs_approval = 0;
            $client->created_at = now();
            $client->updated_at = now();
            $client->consultant_id = 1;
            $client->crm_id = 2;
            // $client->crm_id = 4;
            // $client->consultant_name = isset($record['fund_consultant']) && $record['fund_consultant'] != '' && $record['fund_consultant'] != 'NULL' ? ($record['fund_consultant']) : NULL;
            $client->save();

            // $client = Client::where('company', 'LIKE',"%{$record['client_name']}%")->first();

            $cp = new \App\ClientProcess;
            $cp->client_id = $client->id;
            $cp->process_id = 35;
            $cp->step_id = 188;
            $cp->active = 0;
            $cp->save();

            // // $clientData = isset($record['province']) && $record['province'] != '' && $record['province'] != 'NULL' ? ($record['province'] == 'Gauteng' ? 144 : 0)($record['Province'] == 'Mpumalanga' ? 145 : 0)($record['Province'] == 'North West' ? 146 : 0)($record['Province'] == 'Limpopo' ? 147 : 0)($record['Province'] == 'Kwazulu Natal' ? 148 : 0)($record['Province'] == 'Eastern Cape' ? 149 : 0)($record['Province'] == 'Western Cape' ? 150 : 0)($record['Province'] == 'Northern Cape' ? 151 : 0)($record['Province'] == 'Free Sate' ? 152 : 0) : 0;
            // $clientData = isset($record['province']) && $record['province'] != '' && $record['province'] != 'NULL' ? ($record['province'] == 'Gauteng' ? 144 : ($record['province'] == 'Mpumalanga' ? 145 : ($record['province'] == 'North West' ? 146 : ($record['province'] == 'Limpopo' ? 147 : ($record['province'] == 'Kwazulu Natal' ? 148 : ($record['province'] == 'Eastern Cape' ? 149 : ($record['province'] == 'Western Cape' ? 150 : ($record['province'] == 'Northern Cape' ? 151 : ($record['province'] == 'Freestate' ? 1735 : ($record['province'] == 'Howick' ? 1736 : 0)))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 48;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // // $clientData = isset($record['Parent_or_Branch']) && $record['Parent_or_Branch'] != '' && $record['Parent_or_Branch'] != 'NULL' ? ($record['Parent_or_Branch'] == 'Parent' ? 138 : 0)($record['Parent_or_Branch'] == 'Branch' ? 139 : 0) : 0;
            // $clientData = isset($record['parent_branch']) && $record['parent_branch'] != '' && $record['parent_branch'] != 'NULL' ? ($record['parent_branch'] == 'Parent' ? 138 : ($record['parent_branch'] == 'Branch' ? 139 : 0)) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 36;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // // $clientData = isset($record['Fund_Consultant']) && $record['Fund_Consultant'] != '' && $record['Fund_Consultant'] != 'NULL' ? ($record['Fund_Consultant'] == 'Douw Smit' ? 493 : 0)($record['Fund_Consultant'] == 'George Herman' ? 494 : 0)($record['Fund_Consultant'] == 'Liesl Nienaber' ? 495 : 0)($record['Fund_Consultant'] == 'Madeleen van den Berg' ? 496 : 0)($record['Fund_Consultant'] == 'Nadine Lewis' ? 497 : 0)($record['Fund_Consultant'] == 'Natasha le Roux' ? 498 : 0)($record['Fund_Consultant'] == 'Sam van Zyl' ? 499 : 0)($record['Fund_Consultant'] == 'Amanda White' ? 500 : 0)($record['Fund_Consultant'] == 'Direct Client' ? 501 : 0) : 0;
            // $clientData = isset($record['fund_consultant']) && $record['fund_consultant'] != '' && $record['fund_consultant'] != 'NULL' ? ($record['fund_consultant'] == 'Douw Smit' ? 493 : ($record['fund_consultant'] == 'George Herman' ? 494 : ($record['fund_consultant'] == 'Liesl Nienaber' ? 495 : ($record['fund_consultant'] == 'Madeleen van den Berg' ? 496 : ($record['fund_consultant'] == 'Nadine Lewis' ? 497 : ($record['fund_consultant'] == 'Natasha le Roux' ? 498 : ($record['fund_consultant'] == 'Sam van Zyl' ? 499 : ($record['fund_consultant'] == 'Amanda White' ? 500 : ($record['fund_consultant'] == 'Direct Client' ? 501 : ($record['fund_consultant'] == 'SJ Du Preez' ? 1734 : ($record['fund_consultant'] == 'Jenny du Preez' ? 1745 : 0))))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 56;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['eb_assistant']) && $record['eb_assistant'] != '' && $record['eb_assistant'] != 'NULL' ? ($record['eb_assistant'] == 'Adri Jansen van Vuuren' ? 157 : ($record['eb_assistant'] == 'Chantal Brooke' ? 158 : ($record['eb_assistant'] == 'Tanya van Zyl' ? 159 : ($record['eb_assistant'] == 'Tanya Smit' ? 160 : ($record['eb_assistant'] == 'Rene Koekemoer' ? 161 : ($record['eb_assistant'] == 'Shaneley Alberts' ? 162 : ($record['eb_assistant'] == 'Marc Broeke' ? 1739 : ($record['eb_assistant'] == 'Nadine Lewis' ? 1740 : ($record['eb_assistant'] == 'Dominique / Marc Broeke' ? 1737 : ($record['eb_assistant'] == 'Dominique Vilares' ? 1738 : 0)))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 52;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['healthcare_assistant']) && $record['healthcare_assistant'] != '' && $record['healthcare_assistant'] != 'NULL' ? ($record['healthcare_assistant'] == 'Bernice Fourie' ? 153 : ($record['healthcare_assistant'] == 'Clarissa Lottering' ? 154 : ($record['healthcare_assistant'] == 'Pricilla Abrahams' ? 155 : ($record['healthcare_assistant'] == 'Tracey Ellery' ? 156 : ($record['healthcare_assistant'] == 'Cassie Nel' ? 1741 : ($record['healthcare_assistant'] == 'Lizaan Jacobs' ? 1742 : ($record['healthcare_assistant'] == 'Yolandi Veira' ? 1743 : 0))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 53;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['medical_insurance_assistant']) && $record['medical_insurance_assistant'] != '' && $record['medical_insurance_assistant'] != 'NULL' ? ($record['medical_insurance_assistant'] == 'Bernice Fourie' ? 238 : ($record['medical_insurance_assistant'] == 'Pricilla Abrahams' ? 239 : ($record['medical_insurance_assistant'] == 'Tracey Ellery' ? 240 : ($record['medical_insurance_assistant'] == 'N/A' ? 241 : ($record['medical_insurance_assistant'] == 'Lizaan Jacobs' ? 1364 : ($record['medical_insurance_assistant'] == 'Tanya Steyn' ? 1744 : 0)))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 54;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['type']) && $record['type'] != '' && $record['type'] != 'N/A' ? ($record['type'] == 'Approved' ? 523 : ($record['type'] == 'Unapproved' ? 524 : 0)) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 61;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // // $clientData = isset($record['Risk_Fund_Provider']) && $record['Risk_Fund_Provider'] != '' && $record['Risk_Fund_Provider'] != 'NULL' ? ($record['Risk_Fund_Provider'] == 'Brefco' ? 511 : 0)($record['Risk_Fund_Provider'] == 'Capital Alliance' ? 512 : 0)($record['Risk_Fund_Provider'] == 'Discovery' ? 513 : 0)($record['Risk_Fund_Provider'] == 'Hollard' ? 514 : 0)($record['Risk_Fund_Provider'] == 'Keyrisk' ? 515 : 0)($record['Risk_Fund_Provider'] == 'Liberty' ? 516 : 0)($record['Risk_Fund_Provider'] == 'Momentum' ? 517 : 0)($record['Risk_Fund_Provider'] == 'Old Mutual' ? 518 : 0)($record['Risk_Fund_Provider'] == 'Regent' ? 519 : 0)($record['Risk_Fund_Provider'] == 'Sanlam' ? 520 : 0)($record['Risk_Fund_Provider'] == 'Other' ? 521 : 0)($record['Risk_Fund_Provider'] == 'N/A' ? 522 : 0) : 0;
            // $clientData = isset($record['risk_provider']) && $record['risk_provider'] != '' && $record['risk_provider'] != 'NULL' ? ($record['risk_provider'] == 'Brefco' ? 511 : ($record['risk_provider'] == 'Capital Alliance' ? 512 : ($record['risk_provider'] == 'Discovery' ? 513 : ($record['risk_provider'] == 'Hollard' ? 514 : ($record['risk_provider'] == 'Keyrisk' ? 515 : ($record['risk_provider'] == 'Liberty' ? 516 : ($record['risk_provider'] == 'Momentum' ? 517 : ($record['risk_provider'] == 'Old Mutual' ? 518 : ($record['risk_provider'] == 'Regent' ? 519 : ($record['risk_provider'] == 'Sanlam' ? 520 : ($record['risk_provider'] == 'Other' ? 521 : ($record['risk_provider'] == 'N/A' ? 522 : ($record['risk_provider'] == 'AF Life' ? 1746 : ($record['risk_provider'] == 'Alex Forbes' ? 1747 : ($record['risk_provider'] == 'Alexander Forbes' ? 1748 : ($record['risk_provider'] == 'Ambledown' ? 1749 : ($record['risk_provider'] == 'Keyrisk Capital Alliance' ? 1750 : ($record['risk_provider'] == 'Safrican' ? 1751 : ($record['risk_provider'] == 'Stangen' ? 1752 : 0))))))))))))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 60;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // // // $clientData = isset($record['Retirement_Fund_Provider']) && $record['Retirement_Fund_Provider'] != '' && $record['Retirement_Fund_Provider'] != 'NULL' ? ($record['Retirement_Fund_Provider'] == 'Alexander Forbes' ? 603 : 0)($record['Retirement_Fund_Provider'] == 'Brefco' ? 604 : 0)($record['Retirement_Fund_Provider'] == 'Discovery' ? 605 : 0)($record['Retirement_Fund_Provider'] == 'Liberty' ? 606 : 0)($record['Retirement_Fund_Provider'] == 'Momentum' ? 607 : 0)($record['Retirement_Fund_Provider'] == 'NMG' ? 608 : 0)($record['Retirement_Fund_Provider'] == 'Old Mutual' ? 609 : 0)($record['Retirement_Fund_Provider'] == 'RFS' ? 610 : 0)($record['Retirement_Fund_Provider'] == 'Sanlam' ? 611 : 0)($record['Retirement_Fund_Provider'] == 'Other' ? 612 : 0)($record['Retirement_Fund_Provider'] == 'N/A' ? 613 : 0) : 0;
            // $clientData = isset($record['fund_provider']) && $record['fund_provider'] != '' && $record['fund_provider'] != 'NULL' ? ($record['fund_provider'] == 'Alexander Forbes' ? 1753 : ($record['fund_provider'] == 'Brefco' ? 604 : ($record['fund_provider'] == 'Discovery' ? 605 : ($record['fund_provider'] == 'Liberty' ? 606 : ($record['fund_provider'] == 'Momentum' ? 607 : ($record['fund_provider'] == 'NMG' ? 608 : ($record['fund_provider'] == 'Old Mutual' ? 609 : ($record['fund_provider'] == 'RFS' ? 610 : ($record['fund_provider'] == 'Sanlam' ? 611 : ($record['fund_provider'] == 'Other' ? 612 : ($record['fund_provider'] == 'N/A' ? 613 : ($record['fund_provider'] == 'Alex Forbes' ? 1753 : ($record['fund_provider'] == 'GQM' ? 1754 : ($record['fund_provider'] == 'GTC' ? 1755 : ($record['fund_provider'] == 'Tennant' ? 1756 : 0))))))))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 74;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['medical_aid_provider']) && $record['medical_aid_provider'] != '' && $record['medical_aid_provider'] != 'NULL' ? ($record['medical_aid_provider'] == 'Bonitas' ? 627 : ($record['medical_aid_provider'] == 'Discovery' ? 628 : ($record['medical_aid_provider'] == 'Fedhealth' ? 629 : ($record['medical_aid_provider'] == 'Medihelp' ? 630 : ($record['medical_aid_provider'] == 'Momentum' ? 631 : ($record['medical_aid_provider'] == 'Resolution Health' ? 632 : ($record['medical_aid_provider'] == 'Other' ? 633 : ($record['medical_aid_provider'] == 'N/A' ? 634 : ($record['medical_aid_provider'] == 'Bonitas / Medihelp' ? 327 : ($record['medical_aid_provider'] == 'Discovery Health' ? 1758 : ($record['medical_aid_provider'] == 'Discovery Health / Bonitas' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health / Medihelp' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health / Medihelp / Momentum' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health / Momentum Health' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health / Momentum Health and Health4me' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health / Agility Health' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health and Health4me' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health/BOnitas/Medihelp/Momentum' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health/Medihelp/Bonitas' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health/Medihelp/Momentum' ? 628 : ($record['medical_aid_provider'] == 'Discovery Health/Momentum' ? 628 : 0))))))))))))))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 78;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['medical_insurance_provider']) && $record['medical_insurance_provider'] != '' && $record['medical_insurance_provider'] != 'NULL' ? ($record['medical_insurance_provider'] == 'Ambledown (Casuals)' ? 642 : ($record['medical_insurance_provider'] == 'Health 4 Me' ? 643 : ($record['medical_insurance_provider'] == 'OCSA' ? 644 : ($record['medical_insurance_provider'] == 'Primary Care' ? 645 : ($record['medical_insurance_provider'] == 'Hollard' ? 646 : ($record['medical_insurance_provider'] == 'Other' ? 647 : ($record['medical_insurance_provider'] == 'N/A' ? 648 : ($record['medical_insurance_provider'] == 'Ambledown/ H4H D2D FU' ? 1770 : ($record['medical_insurance_provider'] == 'Classic Saver ONLY' ? 1771 : ($record['medical_insurance_provider'] == 'Flexicare' ? 1772 : ($record['medical_insurance_provider'] == 'Health4Me' ? 1773 : ($record['medical_insurance_provider'] == 'Momentum H4M' ? 1774 : ($record['medical_insurance_provider'] == 'Momentum Health4Me' ? 1775 : ($record['medical_insurance_provider'] == 'Oneplan and Health4Me' ? 1776 : ($record['medical_insurance_provider'] == 'Priscilla Abrahams' ? 1777 : ($record['medical_insurance_provider'] == 'Stratum Elite' ? 1778 : 0)))))))))))))))) : 0;
            // $formInputData = new FormInputDropdownData();
            // $formInputData->form_input_dropdown_id = 80;
            // $formInputData->form_input_dropdown_item_id = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['client_name']) && $record['client_name'] != '' && $record['client_name'] != 'NULL' ? $record['client_name'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 465;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['commission_split']) && $record['commission_split'] != '' && $record['commission_split'] != 'NULL' ? $record['commission_split'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 611;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['street_number']) && $record['street_number'] != '' && $record['street_number'] != 'NULL' ? $record['street_number'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 467;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['city']) && $record['city'] != '' && $record['city'] != 'NULL' ? $record['city'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 469;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['category']) && $record['category'] != '' && $record['category'] != 'NULL' ? $record['category'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 595;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['contact_number']) && $record['contact_number'] != '' && $record['contact_number'] != 'NULL' ? $record['contact_number'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 471;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['contact_number']) && $record['contact_number'] != '' && $record['contact_number'] != 'NULL' ? $record['contact_number'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 475;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['ceo_contact_details']) && $record['ceo_contact_details'] != '' && $record['ceo_contact_details'] != 'NULL' ? $record['ceo_contact_details'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 473;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['ceo_email']) && $record['ceo_email'] != '' && $record['ceo_email'] != 'NULL' ? $record['ceo_email'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 476;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['finance_contact']) && $record['finance_contact'] != '' && $record['finance_contact'] != 'NULL' ? $record['finance_contact'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 479;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['finance_email']) && $record['finance_email'] != '' && $record['finance_email'] != 'NULL' ? $record['finance_email'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 482;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['hr_contact']) && $record['hr_contact'] != '' && $record['hr_contact'] != 'NULL' ? $record['hr_contact'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 486;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['email_hr']) && $record['email_hr'] != '' && $record['email_hr'] != 'NULL' ? $record['email_hr'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 489;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['billing_contact']) && $record['billing_contact'] != '' && $record['billing_contact'] != 'NULL' ? $record['billing_contact'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 492;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['billing_email']) && $record['billing_email'] != '' && $record['billing_email'] != 'NULL' ? $record['billing_email'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 495;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['first_contact_email']) && $record['first_contact_email'] != '' && $record['first_contact_email'] != 'NULL' ? $record['first_contact_email'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 501;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['contact_email']) && $record['contact_email'] != '' && $record['contact_email'] != 'NULL' ? $record['contact_email'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 507;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['financial_advisor']) && $record['financial_advisor'] != '' && $record['financial_advisor'] != 'NULL' ? $record['financial_advisor'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 612;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['referring_broker']) && $record['referring_broker'] != '' && $record['referring_broker'] != 'NULL' ? $record['referring_broker'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 421;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['risk_scheme']) && $record['risk_scheme'] != '' && $record['risk_scheme'] != 'NULL' ? $record['risk_scheme'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 613;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['ra_fund_provider']) && $record['ra_fund_provider'] != '' && $record['ra_fund_provider'] != 'NULL' ? $record['ra_fund_provider'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 615;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['ra_scheme']) && $record['ra_scheme'] != '' && $record['ra_scheme'] != 'NULL' ? $record['ra_scheme'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 616;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['who_billing']) && $record['who_billing'] != '' && $record['who_billing'] != 'NULL' ? $record['who_billing'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 617;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['medical_aid_number']) && $record['medical_aid_number'] != '' && $record['medical_aid_number'] != 'NULL' ? $record['medical_aid_number'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 618;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['group_number']) && $record['group_number'] != '' && $record['group_number'] != 'NULL' ? $record['group_number'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 619;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['h4m_benefits']) && $record['h4m_benefits'] != '' && $record['h4m_benefits'] != 'NULL' ? $record['h4m_benefits'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 620;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // $clientData = isset($record['h4m_login']) && $record['h4m_login'] != '' && $record['h4m_login'] != 'NULL' ? $record['h4m_login'] : NULL;
            // // $formInputData = new FormInputTextData();
            // // $formInputData->form_input_text_id = 621;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // $clientData = isset($record['arrears']) && $record['arrears'] != '' && $record['arrears'] != 'NULL' ? $record['arrears'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 622;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // $clientData = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : NULL;
            // $formInputData = new FormInputTextData();
            // $formInputData->form_input_text_id = 623;
            // $formInputData->data = trim($clientData);
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();

            // // $clientData = isset($record['broker_appointment_received']) && $record['broker_appointment_received'] != '' && $record['broker_appointment_received'] != 'NULL' ? $record['broker_appointment_received'] : NULL;
            // // $formInputData = new FormInputDateData();
            // // $formInputData->form_input_date_id = 495;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // // $clientData = isset($record['risk_renewal']) && $record['risk_renewal'] != '' && $record['risk_renewal'] != 'NULL' ? $record['risk_renewal'] : NULL;
            // // $formInputData = new FormInputDateData();
            // // $formInputData->form_input_date_id = 96;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // // $clientData = isset($record['ra_fund_renewal']) && $record['ra_fund_renewal'] != '' && $record['ra_fund_renewal'] != 'NULL' ? $record['ra_fund_renewal'] : NULL;
            // // $formInputData = new FormInputDateData();
            // // $formInputData->form_input_date_id = 647;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // // $clientData = isset($record['signed_sla']) && $record['signed_sla'] != '' && $record['signed_sla'] != 'NULL' ? $record['signed_sla'] : NULL;
            // // $formInputData = new FormInputDateData();
            // // $formInputData->form_input_date_id = 648;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // // $clientData = isset($record['signed_investment']) && $record['signed_investment'] != '' && $record['signed_investment'] != 'NULL' ? $record['signed_investment'] : NULL;
            // // $formInputData = new FormInputDateData();
            // // $formInputData->form_input_date_id = 649;
            // // $formInputData->data = trim($clientData);
            // // $formInputData->client_id = $client->id;
            // // $formInputData->user_id = 1;
            // // $formInputData->duration = 120;
            // // $formInputData->save();

            // $clientData = isset($record['combo_fund']) && $record['combo_fund'] != '' && $record['combo_fund'] != 'NULL' ? ($record['combo_fund'] == 'N' ? 0 : ($record['combo_fund'] == 'Y' ? 1 : 0 )) : 0;
            // $formInputData = new FormInputBooleanData();
            // $formInputData->form_input_boolean_id = 60;
            // $formInputData->data = $clientData;
            // $formInputData->client_id = $client->id;
            // $formInputData->user_id = 1;
            // $formInputData->duration = 120;
            // $formInputData->save();
        }
    }
}
